<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.imarketing.client.MenuGeneratorClient"%>
<%@page import="com.imarketing.common.Common"%>
<%@include file="includes/userauthentication.inc"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@include file="includes/google_analytics.inc"%>

<title>KhaosatOnline - trang admin</title>

<script type='text/javascript' src='dwr/engine.js'></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script type='text/javascript' src='dwr/interface/UserManagement.js'></script>
<script type='text/javascript' src='dwr/interface/SurveyManagement.js'></script>
<script type='text/javascript' src='dwr/interface/PendingSurveyUpgradeManagement.js'></script>

<script type='text/javascript' src='js/utils/richtext/wysiwyg.js'></script>
<script type='text/javascript' src='js/utils/jquery.js'></script>
<script type='text/javascript' src='js/utils/msgbox/jquery.dragndrop.min.js'></script>
<script type='text/javascript' src='js/utils/msgbox/jquery.msgbox.js'></script>

<script type='text/javascript' src='js/common/common.js'></script>
<script type='text/javascript' src='js/admin.js'></script>

<link rel="stylesheet" href="css/user.css" type="text/css" />
<link rel=stylesheet href="css/jquery.msgbox.css" type="text/css" />


<script type="javascript">
<% 
String header = "";
MenuGeneratorClient menuGeneratorClient = new MenuGeneratorClient(request, response);
session.setAttribute(Common.HTTPSESSION_CURRENT_MENU_ID, new Long(4));
String menuUserHtml = menuGeneratorClient.getMenuTabHTML();
%>
</script>

</head>
<body>
<table width="100%" height="1487" border="0">
  <tr>
    <td width="15%" rowspan="6" bgcolor="#333333"><div class="leftads" id="leftads"></div></td>
    <td height="143" rowspan="2" align="center" bgcolor="#D93911"><div class="toplogo" id="toplogo"> <h1 class="style6">KhaosatOnline</h1></div></td>
    <td height="135" bgcolor="#EAEAEA"><div class="topbanner" id="topbanner"></div></td>
    <td width="15%" rowspan="6"><div class="rightads" id="rightads"></div></td>
  </tr>
  <tr>
    <td height="30" bgcolor="#D93911"><div class="menutab" id="menutab"><ul><%=menuUserHtml%></ul></div></td>
  </tr>
  <tr>
     <td height="160" valign="top" bgcolor="#D93911"><h3 class="style2"><strong>Ba bước tạo 1 khảo sát</strong>:</h3>
	    <p class="style2">1. Điền thông tin cơ bản về khảo sát</p>
	    <p class="style2">2. Tạo câu hỏi</p>
	    <p class="style2">3. Gửi khảo sát đến cho bạn bè</p>
     </td>
    
     <td width="51%" rowspan="3" valign="top" bgcolor="#EAEAEA">
        <div class="menubody" id="menubody">
            <div id="surveyupgraderequestlist" style="display:none"></div>
            <div id="userupgraderequestlist" style="display:none"></div>
            <div id="userlist" style="display:none"></div>
            <div id="moderatorlist" style="display:none"></div>
            <div id="surveylist" style="display:none"></div>
            <div id="polllist" style="display:none"></div>
            <div id='spinningwheel'></div>
        </div>
     </td>    
  </tr>
  <tr>
    <td height="244" valign="top" align="center" bgcolor="#D93911"><div class="signup" id="signup">
      <h2 class="style4"><a href='#' onclick='return logout();'>Thoát</a></h2>
      <p class="style3">&nbsp;</p>
      <p class="style3">&nbsp;</p>
      <p class="style3">&nbsp;</p>
    </div></td>
  </tr>
  
  <tr>
    <td height="228" colspan="2" bgcolor="#000000"><div class="footer" id="footer"><%@ include file="includes/footer.inc" %></div></td>
  </tr>
</table>
</body>
</html>