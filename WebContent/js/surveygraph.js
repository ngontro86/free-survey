

$(document).ready(function() {
    if(questionStatsId != null && chart != null) {
        var respBack = function(data) {
            $("#spinningwheel").html('');
            $('#questionstats').html("<img src='"+data+"'/>");
            $('#questionstats').show();
        }
        $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
        SurveyManagement.displayOneQuestionStats(questionStatsId, chart, chart == 'PieChart'? 700 : 350, chart == 'PieChart'? 700 : 400, respBack);
    }
});