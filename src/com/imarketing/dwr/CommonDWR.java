package com.imarketing.dwr;

import com.imarketing.conf.ConfigStore;

public class CommonDWR {
	
	public static final int MAX_IDLE_DURATION = 30 * 60;
	
	public static final String FREEMIUMGROUP = "Free forever";
	
	public static final String WAITING_ACTIVATION = "waiting_activation";
	
	public static final String ACTIVATED = "activated";
	
	public final static String LAUNCHING_SURVEY_EMAIL_SUBJECT = String.format("Từ %s: Khởi chạy khảo sát", ConfigStore.getConfig("Application.Website.Name"));
	
	public final static String SURVEY_UPGRADE_EMAIL_SUBJECT = String.format("Từ %s: Chứng nhận nâng cấp khảo sát", ConfigStore.getConfig("Application.Website.Name"));
	
	public final static String LAUNCHING_SURVEY_EMAILBODY = "Chào %s, <br><br><br>" +
			"Bạn đã thành công khởi chạy khảo sát mới với đường link: " + ConfigStore.getConfig("Application.Host.URL") + "<br>" +
			"Bạn có thể bắt đầu gửi đường link sau " + ConfigStore.getConfig("Application.Host.URL") + "/publisher.jsp?uid=%s đến mọi người để thu thập câu trả lời<br><br>" +
			"<br><br>" +
			"Cám ơn bạn đã sử dụng dịch vụ của chúng tôi." +
			"<br><br>" +
			"Nhóm KhaosatOnline.";
	
	public final static String ACCOUNT_ACTIVATION_EMAILBODY = "Chào %s,<br><br><br>" +
			"Cám ơn sự tham gia của bạn tại "+ConfigStore.getConfig("Application.Host.URL")+"<br>" +
			"Vui lòng truy cập link sau để kích hoạt tài khoản của bạn: "+ConfigStore.getConfig("Application.Host.URL") + "/utils.jsp?uid=%s<br><br>" +
			"Hy vọng bạn sẽ hài lòng với KhaosatOnline" +
			"<br><br>" +
			"Best Regards,<br>" +
			"Nhóm KhaosatOnline.";
	
	public final static String ACCOUNT_PASSWORD_RECOVERY = "Chào %s,<br><br>"+
			"Dưới đây là mật khẩu: <b>%s</b> được lưu tại cơ sở dữ liệu của chúng tôi, bạn có thể tiếp tục dùng chúng để truy cập vào: " + ConfigStore.getConfig("Application.Host.URL")+
			"<br><br>Cám ơn đã sử dụng dịch vụ của chúng tôi."+
			"<br>Hy vọng bạn sẽ hài lòng với KhaosatOnline" +
			"<br><br>" +
			"Best Regards,<br>" +
			"Nhóm KhaosatOnline.";
	
	public final static String SURVEY_UPGRADE_EMAILBODY = "Chào %s, <br><br>"+
		"Cám ơn bạn đã sử dụng dịch vụ có trả phí của " + ConfigStore.getConfig("Application.Host.URL") + "<br>" +
		"Khảo sát của bạn sẽ được nâng cấp trở thành khảo sát có trả phí trong vòng 24h tới, bạn sẽ nhận được 1 email chứng thực khi đó, xin vui lòng quay lại để kiểm tra<br>"+  
		"<br>Hy vọng bạn sẽ hài lòng với KhaosatOnline" +
		"<br><br>" +
		"Best Regards,<br>" +
		"Nhóm KhaosatOnline.";
}
