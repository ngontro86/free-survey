﻿
INSERT INTO `accessiblemenu` (`id`, `label`, `link`, `imgsrc`) VALUES (1, 'Khảo sát mới nhất', 'index.jsp', 'img/menus/home.gif');
INSERT INTO `accessiblemenu` (`id`, `label`, `link`, `imgsrc`) VALUES (2, 'Khảo sát đã tạo', 'user.jsp', 'img/menus/chart.jpg');
INSERT INTO `accessiblemenu` (`id`, `label`, `link`, `imgsrc`) VALUES (3, 'Moderator', 'moderator.jsp', '');
INSERT INTO `accessiblemenu` (`id`, `label`, `link`, `imgsrc`) VALUES (4, 'Survey upgrade request', 'admin.jsp', 'img/menus/user.jpg');
INSERT INTO `accessiblemenu` (`id`, `label`, `link`, `imgsrc`) VALUES (101, 'Câu hỏi thường gặp', 'faq.jsp', 'img/menus/faq.jpg');
INSERT INTO `accessiblemenu` (`id`, `label`, `link`, `imgsrc`) VALUES (102, 'Đăng ký', 'index.jsp?action=registration', 'img/menus/signup.jpg');
INSERT INTO `accessiblemenu` (`id`, `label`, `link`, `imgsrc`) VALUES (103, 'Đăng nhập', 'index.jsp?action=signin', 'img/menus/login.jpg');
INSERT INTO `accessiblemenu` (`id`, `label`, `link`, `imgsrc`) VALUES (104, 'Tài khoản cá nhân', 'user.jsp', 'img/menus/homeuser.jpg');
INSERT INTO `accessiblemenu` (`id`, `label`, `link`, `imgsrc`) VALUES (201, 'Tạo khảo sát mới', 'user.jsp?action=createsurvey', 'img/menus/createsurvey.jpg');
INSERT INTO `accessiblemenu` (`id`, `label`, `link`, `imgsrc`) VALUES (202, 'Tài khoản', 'user.jsp?action=account', 'img/menus/homeuser.jpg');
INSERT INTO `accessiblemenu` (`id`, `label`, `link`, `imgsrc`) VALUES (203, 'Trang chủ', 'index.jsp', 'img/menus/home.gif');
INSERT INTO `accessiblemenu` (`id`, `label`, `link`, `imgsrc`) VALUES (301, 'User list', 'moderator.jsp?action=userlist', '');
INSERT INTO `accessiblemenu` (`id`, `label`, `link`, `imgsrc`) VALUES (302, 'User', 'moderator.jsp?action=user', '');
INSERT INTO `accessiblemenu` (`id`, `label`, `link`, `imgsrc`) VALUES (303, 'Survey/poll list', 'moderator.jsp?action=servicelist', '');
INSERT INTO `accessiblemenu` (`id`, `label`, `link`, `imgsrc`) VALUES (401, 'User list', 'admin.jsp?action=userlist', 'img/menus/homeuser.jpg');
INSERT INTO `accessiblemenu` (`id`, `label`, `link`, `imgsrc`) VALUES (402, 'Moderator list', 'admin.jsp?action=user', 'img/menus/homeuser.jpg');
INSERT INTO `accessiblemenu` (`id`, `label`, `link`, `imgsrc`) VALUES (403, 'Survey list', 'admin.jsp?action=servicelist', 'img/menus/chart.jpg');
INSERT INTO `accessiblemenu` (`id`, `label`, `link`, `imgsrc`) VALUES (404, 'Poll list', 'admin.jsp?action=moderatorlist', 'img/menus/chart.jpg');


INSERT INTO `accessibleservices` (`id`, `label`) VALUES (1, 'Khảo sát miễn phí');
INSERT INTO `accessibleservices` (`id`, `label`) VALUES (2, 'Khảo sát có trả phí');
INSERT INTO `accessibleservices` (`id`, `label`) VALUES (3, 'Bình chọn');


INSERT INTO `groups` (`id`, `name`, `description`) VALUES (1, 'admin', 'for IT head');
INSERT INTO `groups` (`id`, `name`, `description`) VALUES (2, 'IT staff', 'for IT staff');
INSERT INTO `groups` (`id`, `name`, `description`) VALUES (3, 'moderator', 'for company staff');
INSERT INTO `groups` (`id`, `name`, `description`) VALUES (4, 'freemiumuser', 'for user with free registration');
INSERT INTO `groups` (`id`, `name`, `description`) VALUES (5, 'premiumuser', 'for users with pair registration');
INSERT INTO `groups` (`id`, `name`, `description`) VALUES (6, 'other', 'for visitors');

INSERT INTO `group_accessiblemenu` (`groupid`, `menuid`) VALUES (1, 1);
INSERT INTO `group_accessiblemenu` (`groupid`, `menuid`) VALUES (2, 1);
INSERT INTO `group_accessiblemenu` (`groupid`, `menuid`) VALUES (3, 1);
INSERT INTO `group_accessiblemenu` (`groupid`, `menuid`) VALUES (4, 1);
INSERT INTO `group_accessiblemenu` (`groupid`, `menuid`) VALUES (5, 1);
INSERT INTO `group_accessiblemenu` (`groupid`, `menuid`) VALUES (6, 1);
INSERT INTO `group_accessiblemenu` (`groupid`, `menuid`) VALUES (1, 2);
INSERT INTO `group_accessiblemenu` (`groupid`, `menuid`) VALUES (2, 2);
INSERT INTO `group_accessiblemenu` (`groupid`, `menuid`) VALUES (3, 2);
INSERT INTO `group_accessiblemenu` (`groupid`, `menuid`) VALUES (4, 2);
INSERT INTO `group_accessiblemenu` (`groupid`, `menuid`) VALUES (5, 2);
INSERT INTO `group_accessiblemenu` (`groupid`, `menuid`) VALUES (1, 3);
INSERT INTO `group_accessiblemenu` (`groupid`, `menuid`) VALUES (2, 3);
INSERT INTO `group_accessiblemenu` (`groupid`, `menuid`) VALUES (3, 3);
INSERT INTO `group_accessiblemenu` (`groupid`, `menuid`) VALUES (1, 4);
INSERT INTO `group_accessiblemenu` (`groupid`, `menuid`) VALUES (2, 4);

INSERT INTO `group_accessibleservice` (`groupid`, `accessibleserviceid`) VALUES (1, 1);
INSERT INTO `group_accessibleservice` (`groupid`, `accessibleserviceid`) VALUES (2, 1);
INSERT INTO `group_accessibleservice` (`groupid`, `accessibleserviceid`) VALUES (3, 1);
INSERT INTO `group_accessibleservice` (`groupid`, `accessibleserviceid`) VALUES (4, 1);
INSERT INTO `group_accessibleservice` (`groupid`, `accessibleserviceid`) VALUES (1, 2);
INSERT INTO `group_accessibleservice` (`groupid`, `accessibleserviceid`) VALUES (2, 2);
INSERT INTO `group_accessibleservice` (`groupid`, `accessibleserviceid`) VALUES (3, 2);
INSERT INTO `group_accessibleservice` (`groupid`, `accessibleserviceid`) VALUES (4, 2);
INSERT INTO `group_accessibleservice` (`groupid`, `accessibleserviceid`) VALUES (1, 3);
INSERT INTO `group_accessibleservice` (`groupid`, `accessibleserviceid`) VALUES (2, 3);
INSERT INTO `group_accessibleservice` (`groupid`, `accessibleserviceid`) VALUES (3, 3);
INSERT INTO `group_accessibleservice` (`groupid`, `accessibleserviceid`) VALUES (4, 3);


INSERT INTO `menu_submenu` (`menuid`, `submenuid`) VALUES (1, 101);
INSERT INTO `menu_submenu` (`menuid`, `submenuid`) VALUES (1, 102);
INSERT INTO `menu_submenu` (`menuid`, `submenuid`) VALUES (1, 103);
INSERT INTO `menu_submenu` (`menuid`, `submenuid`) VALUES (1, 104);
INSERT INTO `menu_submenu` (`menuid`, `submenuid`) VALUES (2, 201);
INSERT INTO `menu_submenu` (`menuid`, `submenuid`) VALUES (2, 202);
INSERT INTO `menu_submenu` (`menuid`, `submenuid`) VALUES (2, 203);
INSERT INTO `menu_submenu` (`menuid`, `submenuid`) VALUES (3, 301);
INSERT INTO `menu_submenu` (`menuid`, `submenuid`) VALUES (3, 302);
INSERT INTO `menu_submenu` (`menuid`, `submenuid`) VALUES (3, 303);
INSERT INTO `menu_submenu` (`menuid`, `submenuid`) VALUES (4, 401);
INSERT INTO `menu_submenu` (`menuid`, `submenuid`) VALUES (4, 402);
INSERT INTO `menu_submenu` (`menuid`, `submenuid`) VALUES (4, 403);
INSERT INTO `menu_submenu` (`menuid`, `submenuid`) VALUES (4, 404);


INSERT INTO `faqcategories` (`id`, `name`) VALUES (1, 'Câu hỏi chung');
INSERT INTO `faqcategories` (`id`, `name`) VALUES (2, 'Sản phẩm và dịch vụ');
INSERT INTO `faqcategories` (`id`, `name`) VALUES (3, 'Hỗ trợ kỹ thuật');

INSERT INTO `users` (`id`, `status`, `uid`, `username_emailaddress`, `password`, `sex`, `fullname`, `dob`, `address`, `postalcode`, `mobile_contact`, `fax_number`, `companyname`, `companyindustry`, `companysize`, `current_role`, `office_contact`, `lastlogin_timestamp`, `lastupdated_timestamp`, `expired_timestamp`, `created_timestamp`, `rolegroup`, `credit`) VALUES (1, 'activated', 'adfsdasdasdasopqkd', 'admin', 'abc', 'Male', 'Vu Truong Vinh', 1986, 'Singapore', '089394', '+6597985086', 'N.A', 'XYZ Pte. Ltd', 'Banking/Finance', 10, 'Analyst', 'N.A', 20110701, 20110701, 20200701, 20110701, 1, NULL);

INSERT INTO `servicetypedefs` (`id`, `name`, `maxpage`, `maxquestion`, `resnum_openquestion`, `displaycustomizedlogo`, `displaycustomizethankyoupage`, `expirationduration`, `resultdisplayduration`) VALUES (1, 'Khảo sát miễn phí', 1, 10, 15, 0, 0, 45, 5);
INSERT INTO `servicetypedefs` (`id`, `name`, `maxpage`, `maxquestion`, `resnum_openquestion`, `displaycustomizedlogo`, `displaycustomizethankyoupage`, `expirationduration`, `resultdisplayduration`) VALUES (2, 'Khảo sát có trả phí', 1, 100, 150, 1, 1, 45, 45);
INSERT INTO `servicetypedefs` (`id`, `name`, `maxpage`, `maxquestion`, `resnum_openquestion`, `displaycustomizedlogo`, `displaycustomizethankyoupage`, `expirationduration`, `resultdisplayduration`) VALUES (3, 'Bình chọn', 1, 1, 10000, 0, 0, 7, 7);


INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (1, 'OTHER', 468, 60, 'Affililiate', 'MochaHost', '<a href="http://www.mochahost.com/3222-26-1-181.html" target="_blank"><img border="0" src="http://www.mochasupport.com/aff/banners/468x60_reseller.gif" width="468" height="60"></a>', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (2, 'BANNER', 728, 90, 'Affililiate', 'MochaHost', '<a href="http://www.mochahost.com/3222-26-1-184.html" target="_blank"><img border="0" src="http://www.mochasupport.com/aff/banners/728x90_reseller.gif" width="728" height="90"></a>', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (3, 'OTHER', 234, 60, 'Affililiate', 'MochaHost', '<a href="http://www.mochahost.com/3222-25-1-137.html" target="_blank"><img border="0" src="http://www.mochasupport.com/aff/banners/234x60_vps_1.gif" width="234" height="60"></a>', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (4, 'ADS', 140, 240, 'Affililiate', 'MochaHost', '<a href="http://www.mochahost.com/3222-0-1-153.html" target="_blank"><img border="0" src="http://www.mochasupport.com/aff/banners/jspin1_01.gif" width="140" height="240"></a>', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (5, 'ADS', 120, 60, 'Affililiate', 'MochaHost', '<a href="http://www.mochahost.com/3222-0-1-156.html" target="_blank"><img border="0" src="http://www.mochasupport.com/aff/banners/111_120-21.gif" width="120" height="60"></a>', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (6, 'OTHER', 0, 100, 'Affililiate', 'EventCliqk', '<a href=\'index.jsp\'><img src=\'img/logo.PNG\'/></a>', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (7, 'OTHER', 468, 60, 'Affililiate', 'MochaHost', '<a href="http://www.mochahost.com/3222-0-1-122.html" target="_blank"><img border="0" src="http://www.mochasupport.com/aff/banners/111_468-1_1.gif" width="468" height="60"></a>', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (8, 'OTHER', 468, 60, 'Affililiate', 'MochaHost', '<a href="http://www.mochahost.com/3222-0-1-123.html" target="_blank"><img border="0" src="http://www.mochasupport.com/aff/banners/468_60-1_1.gif" width="468" height="60"></a>', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (9, 'OTHER', 468, 60, 'Affililiate', 'MochatHost', '<a href="http://www.mochahost.com/3222-0-1-125.html" target="_blank"><img border="0" src="http://www.mochasupport.com/aff/banners/340_1.gif" width="468" height="60"></a>', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (10, 'BANNER', 728, 90, 'Affililiate', 'MochaHost', '<a href="http://www.mochahost.com/3222-0-1-114.html" target="_blank"><img border="0" src="http://www.mochasupport.com/aff/banners/112_728-4_1.gif" width="728" height="90"></a>', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (15, 'ADS', 0, 100, 'Affililiate', 'GoDaddy', '<a href="http://affiliate.godaddy.com/redirect/768BE5475C1F1E19809E19097105F62FA792F6E528835E08D7668645D91E4C1EE2DD8F1D260CE0B791FAFF86D25835ED" title="$1.99 Web Hosting">$1.99 Web Hosting</a><img src="http://affiliate.godaddy.com/content/spacer.png?q=768BE5475C1F1E19809E19097105F62FA792F6E528835E08D7668645D91E4C1EE2DD8F1D260CE0B791FAFF86D25835ED" width="0" height="0" alt=""/>', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (16, 'ADS', 0, 100, 'Affililiate', 'GoDaddy', '<a href="http://affiliate.godaddy.com/redirect/8CACE2EDD0B9B243C9206FF0ABC612CEDC0F3D4A1EE655C70FB05C43D272671BCC896B1B754E0D5F06AC8CC568C0ADB1"><img src="http://affiliate.godaddy.com/ads/8CACE2EDD0B9B243C9206FF0ABC612CEDC0F3D4A1EE655C70FB05C43D272671BCC896B1B754E0D5F06AC8CC568C0ADB1" border="0" width="120"  height="240" alt="$1.99/mo Web Hosting"/></a>', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (17, 'ADS', 0, 100, 'Affililiate', 'GoDaddy', '<a href="http://affiliate.godaddy.com/redirect/78325E7B630224F2DFD648AE582F34BD6989CC3FDD4DF424CC01828F54C2888063EB6D9FFBB34723E8009C3814D7EAFA8FF1EC88B3DFFFAF3CB65C01D9D4F4BB"><img src="http://affiliate.godaddy.com/ads/78325E7B630224F2DFD648AE582F34BD6989CC3FDD4DF424CC01828F54C2888063EB6D9FFBB34723E8009C3814D7EAFA8FF1EC88B3DFFFAF3CB65C01D9D4F4BB" border="0" width="120"  height="240" alt="Domains $7.69 - Why Pay More?"/></a>', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (18, 'ADS', 0, 100, 'Affililiate', 'GoDaddy', '<a href="http://affiliate.godaddy.com/redirect/0886725CEE5260047117BF6CD26AD763BA2B42D05C99D4BEEEDFECDF96A84A338811081D790FDC708EA8512A55F1FC3161386AF4126C7C08E0957B8680CB0925"><img src="http://affiliate.godaddy.com/ads/0886725CEE5260047117BF6CD26AD763BA2B42D05C99D4BEEEDFECDF96A84A338811081D790FDC708EA8512A55F1FC3161386AF4126C7C08E0957B8680CB0925" border="0" width="120"  height="240" alt="Buy a .COM, get a FREE .US!"/></a>', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (19, 'ADS', 0, 100, 'Affililiate', 'GoDaddy', '<a href="http://affiliate.godaddy.com/redirect/78BA2E315BAC999C1E6EB51CF1FB87F6675BF77E81A27FED98CB0A0CFA52E188037FAD37DEEF24E5E9326FC895306ECA" title="Unlimited Hosting at GoDaddy.com!">Unlimited Hosting at GoDaddy.com!</a><img src="http://affiliate.godaddy.com/content/spacer.png?q=78BA2E315BAC999C1E6EB51CF1FB87F6675BF77E81A27FED98CB0A0CFA52E188037FAD37DEEF24E5E9326FC895306ECA" width="0" height="0" alt=""/>', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (20, 'ADS', 0, 100, 'Affililiate', 'GoDaddy', '<a href="http://affiliate.godaddy.com/redirect/F5EEFEC89A36066FCC7AE46355AC678F30E2353D5865698171FACFFFBDC8A65852D4BDB432ADBDE277B943BA11E25BB3" title="Create your own Web store in minutes with GoDaddy.com Quick Shopping Cart">Create your own Web store in minutes with GoDaddy.com Quick Shopping Cart</a><img src="http://affiliate.godaddy.com/content/spacer.png?q=F5EEFEC89A36066FCC7AE46355AC678F30E2353D5865698171FACFFFBDC8A65852D4BDB432ADBDE277B943BA11E25BB3" width="0" height="0" alt=""/>', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (21, 'ADS', 0, 100, 'Affililiate', 'GoDaddy', '<a href="http://affiliate.godaddy.com/redirect/5BF122719E558BA247E9F96017C87EC13AF6C053739367082178F2A9139FAD550EDC94FE3C3F1EF79888351125DDD4F674CEFB4E25B78FE3A6A0BC70ABC8EE44"><img src="http://affiliate.godaddy.com/ads/5BF122719E558BA247E9F96017C87EC13AF6C053739367082178F2A9139FAD550EDC94FE3C3F1EF79888351125DDD4F674CEFB4E25B78FE3A6A0BC70ABC8EE44" border="0" width="120"  height="240" alt="Stay organized and on track with Group Email from GoDaddy.com"/></a>', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (22, 'ADS', 0, 100, 'Affililiate', 'GoDaddy', '<a href="http://affiliate.godaddy.com/redirect/60F517EE01616C2EE7B57326E802FACAD82809A56CB116DA083D99140B50DE039B16646BC9BAF87EC71A2C3A7852AE30"><img src="http://affiliate.godaddy.com/ads/60F517EE01616C2EE7B57326E802FACAD82809A56CB116DA083D99140B50DE039B16646BC9BAF87EC71A2C3A7852AE30" border="0" width="120"  height="240" alt="Dedicated Servers from just $69.99/mo"/></a>', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (23, 'ADS', 0, 100, 'Affililiate', 'GoDaddy', '<a href="http://affiliate.godaddy.com/redirect/50DDEB6F3885EC16FA06899ADA09B99EB58B24671340AEAF4A76099FC21C6ED3164DEAFAAF8EA202D675D34350B1BB36"><img src="http://affiliate.godaddy.com/ads/50DDEB6F3885EC16FA06899ADA09B99EB58B24671340AEAF4A76099FC21C6ED3164DEAFAAF8EA202D675D34350B1BB36" border="0" width="120"  height="240" alt="Virtual Dedicated Servers from just $29.99/mo"/></a>', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (24, 'ADS', 0, 100, 'Affililiate', 'GoDaddy', '<a href="http://affiliate.godaddy.com/redirect/E051622E6A9FA9CA090D9B28457B162516CB1CEFA5312333F801F05803F24E7CEDFDD052038D74B92F8D91503D6AA011614FF209D3B250C2219AA6D01923F926"><img src="http://affiliate.godaddy.com/ads/E051622E6A9FA9CA090D9B28457B162516CB1CEFA5312333F801F05803F24E7CEDFDD052038D74B92F8D91503D6AA011614FF209D3B250C2219AA6D01923F926" border="0" width="120"  height="240" alt="Selling Made Easy with Quick Shopping Cart from GoDaddy.com"/></a>', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (26, 'SURVEYICON', 0, 100, 'Affililiate', 'KhaosatOnline', 'img/graph-icon/surveyicon1.jpg', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (27, 'SURVEYICON', 0, 100, 'Affililiate', 'KhaosatOnline', 'img/graph-icon/surveyicon2.jpg', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (28, 'SURVEYICON', 0, 100, 'Affililiate', 'KhaosatOnline', 'img/graph-icon/surveyicon3.jpg', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (29, 'SURVEYICON', 0, 100, 'Affililiate', 'KhaosatOnline', 'img/graph-icon/surveyicon4.jpg', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (30, 'SURVEYICON', 0, 100, 'Affililiate', 'KhaosatOnline', 'img/graph-icon/surveyicon5.jpg', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (31, 'SURVEYICON', 0, 100, 'Affililiate', 'KhaosatOnline', 'img/graph-icon/surveyicon6.jpg', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (32, 'SURVEYICON', 0, 100, 'Affililiate', 'KhaosatOnline', 'img/graph-icon/surveyicon7.jpg', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (33, 'SURVEYICON', 0, 100, 'Affililiate', 'KhaosatOnline', 'img/graph-icon/surveyicon8.jpg', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (34, 'SURVEYICON', 0, 100, 'Affililiate', 'KhaosatOnline', 'img/graph-icon/surveyicon9.jpg', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (35, 'SURVEYICON', 0, 100, 'Affililiate', 'KhaosatOnline', 'img/graph-icon/surveyicon10.jpg', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (36, 'SURVEYICON', 0, 100, 'Affililiate', 'KhaosatOnline', 'img/graph-icon/surveyicon11.jpg', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (37, 'SURVEYICON', 0, 100, 'Affililiate', 'KhaosatOnline', 'img/graph-icon/surveyicon12.jpg', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (38, 'SURVEYICON', 0, 100, 'Affililiate', 'KhaosatOnline', 'img/graph-icon/surveyicon13.jpg', 0, 1);
INSERT INTO `ads` (`id`, `type`, `width`, `height`, `description`, `provider`, `htmlcode`, `timestamp`, `expirytimestamp`) VALUES (39, 'SURVEYICON', 0, 100, 'Affililiate', 'KhaosatOnline', 'img/graph-icon/surveyicon14.jpg', 0, 1);



INSERT INTO `faqs` (`id`, `question`, `answer`) VALUES (1, 'Số lần tạo khảo sát miễn phí có bị giới hạn không?', '<br>Không. Chỉ cần đăng ký tài khoản, bạn có thể tạo khảo sát với số lượng nhiều tùy ý.');
INSERT INTO `faqs` (`id`, `question`, `answer`) VALUES (2, 'Có thể biết thông tin về người điền khảo sát không?', '<br>Tất cả các khảo sát đều được mặc định ẩn danh, vì thế EventCliqk không cung cấp thông tin về địa chỉ email hoặc IP của người điền khảo sát.');
INSERT INTO `faqs` (`id`, `question`, `answer`) VALUES (3, 'Các kết quả khảo sát có được bảo mật không? Ai sở hữu các dữ liệu này?', '<br>Bạn có thể hoàn toàn yên tâm rằng các kết quả khảo sát của bạn được bảo mật tuyệt đối và thuộc quyền sở hữu của riêng bạn. Chúng tôi luôn đảm bảo quyền bảo mật thông tin khách hàng. Bạn có thể xem chính sách bảo mật của website <a href=\'index.jsp\'>tại đây</a>.');
INSERT INTO `faqs` (`id`, `question`, `answer`) VALUES (4, 'Làm thế nào để thay logo của EventCliqk bằng logo của bạn trong link khảo sát?', '<br>Bạn có thể upload hình ảnh lên các website miễn phí như photobucket.com, flickr.com hoặc freeimagehosting.net, copy link chứa hình ảnh của bạn và paste vào mục logo link (Bước điền thông tin cơ bản trong mục tạo khảo sát). Bạn vẫn có thể thay logo khi khảo sát đã khởi chạy bằng cách sử dụng công cụ “Chỉnh sửa” và tiếp tục chọn “Chỉnh sửa thông tin cơ bản”. Chú ý rằng dung lượng của file bạn muốn đăng tải không vượt quá 250KB và  kích thước file dưới 200 pixels với độ rộng dưới 100 pixel nhằm giữ cho kết cấu của trang không bị thay đổi. Đây là tính năng dành cho các khảo sát đã được nâng cấp.\r\n\r\n<br><b>Lưu ý</b>: Không sử dụng logo của các tổ chức/công ty đã được đăng ký bản quyền thương hiệu trừ khi bạn được cho phép bởi các bộ phận hoặc cá nhân có thẩm quyền. Hình ảnh của các ngân hàng và các tổ chức tài chính chỉ được sử dụng sau khi văn bản cấp phép hợp pháp được gửi tới ban quản trị website qua fax hoặc email.');
INSERT INTO `faqs` (`id`, `question`, `answer`) VALUES (5, 'Có thể chèn hình ảnh và video clips vào khảo sát không?', '<br>Tính năng này đang trong giai đoạn thử nghiệm');
INSERT INTO `faqs` (`id`, `question`, `answer`) VALUES (6, 'Làm thế nào để chia sẻ kết quả khảo sát với những người khác mà không cần chia sẻ thông tin tài khoản của mình?', '<br>Bạn có thể sử dụng tính năng “Hiển thị kết quả khảo sát công khai” để chia sẻ kết quả khảo sát với tất cả mọi người, bạn có thể sử dụng công cụ “Chỉnh sửa” để bỏ chọn tính năng này bất cứ lúc nào.');
INSERT INTO `faqs` (`id`, `question`, `answer`) VALUES (7, 'Hướng dẫn thực hiện khởi chạy khảo sát', '<br>Sau khi tạo khảo sát thành công, một đường truyền chứa khảo sát của bạn sẽ được hiển thị rõ ràng (URL Link), bạn cũng có thể nhấp chuột vào tiêu đề khảo sát để hiển thị link này. Thực hiện thao tác Copy và Paste Link trên (copy toàn bộ mã) để gửi khảo sát tới người nhận. Người nhận chỉ cần nhấp vào Link do bạn gửi đến để điền khảo sát. Kết quả phản hồi sẽ được hệ thống xử lý ngay sau đó.');
INSERT INTO `faqs` (`id`, `question`, `answer`) VALUES (8, 'Có thể xem được từng bản kết quả khảo sát riêng lẻ thay vì kết quả chung đã được tổng hợp không?', '<br>Tính năng này đang trong giai đoạn thử nghiệm');
INSERT INTO `faqs` (`id`, `question`, `answer`) VALUES (9, 'Tại sao không xem được kết quả khảo sát?', '<br>Khảo sát được tạo bởi một thành viên khác: Người tạo khảo sát lựa chọn không đăng kết quả khảo sát của mình, đây là một tính năng trong bước điền thông tin cơ bản của quá trình tạo khảo sát và có thể được người tạo chỉnh sửa tùy chọn sau đó.\r\n\r\n<br><b>Khảo sát của bạn</b>: Mọi khảo sát đều được lưu trữ trong 45 ngày, tuy nhiên kết quả khảo sát sẽ chỉ được hiển thị trong 5 ngày kể từ ngày khởi tạo đối với khảo sát miễn phí và 45 ngày kể từ ngày khởi chạy đối với khảo sát có phí.');
INSERT INTO `faqs` (`id`, `question`, `answer`) VALUES (10, 'Những lợi ích của việc nâng cấp khảo sát?', '<br>- Không giới hạn số lượng câu hỏi mỗi khảo sát\r\n\r\n<br>- Hiện thị số lượng kết quả trong câu hỏi mở lên tới 150 câu trả lời\r\n\r\n<br>- Có thể xem kết quả khảo sát trong 45 ngày thay vì 5 ngày đối với khảo sát miễn phí\r\n\r\n<br>- Tùy ý lựa chọn thay đổi logo tại khảo sát\r\n\r\n<br>- Tùy chỉnh lời cảm ơn sau mỗi khảo sát theo ý bạn\r\n\r\n<br><b>Hãy nâng cấp khảo sát của bạn để tận dụng những tính năng tuyệt vời trên sớm nhất có thể</b>');
INSERT INTO `faqs` (`id`, `question`, `answer`) VALUES (11, 'Phí nâng cấp khảo sát là bao nhiêu? Thanh toán theo phương thức nào?', '<br>Phí nâng cấp là 50,000 VND/ 01 khảo sát\r\n\r\n<br>Hiện tại, chúng tôi chỉ chấp nhận thanh toán qua chuyển khoản, với thông tin tài khoản như sau:\r\n\r\n<br>+ Chủ khoản (Account holder): Vũ Thị Oanh\r\n\r\n<br>+ Số tài khoản (Account number): 0102844474\r\n\r\n<br>+ Ngân hàng (Bank): Ngân hàng Đông Á\r\n\r\n<br>+ Chi nhánh (Branch): Chi nhánh Nam Định\r\n\r\n<br>Sau khi thực hiện giao dịch gửi tiền, bạn nhập mã số chứng nhận giao dịch gửi tiền đó (Ref num) theo yêu cầu tại mục nâng cấp tài khoản và vui lòng chờ đợi hệ thống kiểm tra và hồi đáp. Sau khi xác nhận thanh toán thành công, khảo sát của bạn sẽ được nâng cấp trong vòng 24h.');
INSERT INTO `faqs` (`id`, `question`, `answer`) VALUES (12, 'Hướng dẫn phân biệt các loại câu hỏi khảo sát', '<br>1. Câu hỏi lựa chọn một phương án: chỉ được chọn duy nhất một phương án trả lời, các phương án trả lời loại trừ nhau\r\n\r\n<br>VD: Bạn bao nhiêu tuổi?\r\n\r\n<br>Dưới 18 tuổi\r\n\r\n<br>Từ 18 đến 30 tuổi\r\n\r\n<br>Trên 30 tuổi\r\n\r\n<br>2. Câu hỏi lựa chọn nhiều phương án: được chọn nhiều phương án trả lời, các phương án trả lời không loại trừ nhau\r\n\r\n<br>VD: Bạn đã sử dụng các sản phẩm nào dưới đây?\r\n\r\n<br>Sản phẩm A\r\n\r\n<br>Sản phẩm B\r\n\r\n<br>Sản phẩm C\r\n\r\n<br>3. Câu hỏi ĐÚNG/SAI : Câu hỏi chỉ có 02 phương án trả lời Đúng hoặc Sai. Đây là một trường hợp đặc biệt của loại câu hỏi lựa chọn một phương án, do thường xuyên được sử dụng nên được tách riêng.\r\n\r\n<br>VD: Bạn hài lòng với sản phẩm đã sử dụng?\r\n\r\n<br>Đúng\r\n\r\n<br>Sai\r\n\r\n<br>4. Câu hỏi đánh giá theo tiêu chí: Đánh giá các tiêu chí (chủ thể) theo các mức độ khác nhau.\r\n\r\n<br>VD: Đánh giá chất lượng các sản phẩm sau:\r\n\r\n<br>            Tốt    Khá    Trung bình    Kém\r\n\r\n<br>Sản phẩm A\r\n\r\n<br>Sản phẩm B\r\n\r\n<br>Sản phẩm C\r\n\r\n<br>Trong câu hỏi trên: Sản phẩm A, sản phẩm B, sản phẩm C là các tiêu chí đánh giá; Tốt, khá, trung bình, kém là các mức độ đánh giá.\r\n\r\n<br>5. Câu hỏi xếp hạng: Xếp hạng các lựa chọn (chủ thể) theo thứ tự tăng hoặc giảm dần tùy theo bạn quy định.\r\n\r\n<br>VD: Xếp hạng mức độ hài lòng của bạn về sản phẩm của công ty X theo quy ước: 1 là hài lòng nhất\r\n\r\n<br>Chất lượng sản phẩm        1\r\n\r\n<br>Giá thành            3\r\n\r\n<br>Dịch vụ khách hàng        2\r\n\r\n<br>Trong câu hỏi trên: Chất lượng sản phẩm, giá thành, dịch vụ khách hàng là các lựa chọn để xếp hạng; các số 1, 3, 2 là các thứ tự xếp hạng tương ứng. Theo ví dụ trên đây: người dùng này hài lòng nhất về chất lượng sản phẩm, tiếp đó là dịch vụ khách hàng và chưa hài lòng về giá thành sản phẩm.\r\n\r\n<br>6. Câu hỏi mở: Thường là câu hỏi nêu ý kiến đánh giá, góp ý cá nhân\r\n\r\n<br>VD: Nêu ý kiến của bạn nhằm nâng cao mức độ hài lòng về sản phẩm của công ty X');
INSERT INTO `faqs` (`id`, `question`, `answer`) VALUES (13, 'Một số lưu ý khi tạo khảo sát', '<br>1. Lựa chọn mặc định: Tự động thêm phương án trả lời “Khác” vào phần câu trả lời\r\n\r\n<br>VD: Bạn sống ở tỉnh thành nào?\r\n\r\n<br>    Hà nội\r\n\r\n<br>    Nam định\r\n\r\n<br>    Hải dương\r\n\r\n<br>    Khác\r\n\r\n<br>Trong câu hỏi trên, khi bạn kích chọn “Lựa chọn mặc định”, phần câu trả lời sẽ tự động hiển thị phương án trả lời “Khác”.\r\n\r\n<br>2. Câu hỏi bắt buộc: Khi bạn kích chọn “Câu hỏi bắt buộc”, câu hỏi bạn đang tạo sẽ hiển thị dấu sao đỏ phía bên phải, người điền khảo sát sẽ bắt buộc phải trả lời câu hỏi này, nếu bỏ qua, hệ thống sẽ tự động nhắc nhở người điền thực hiện.\r\n\r\n<br>3. Khi tạo các phương án trả lời, mỗi dòng sẽ chứa một phương án, sử dụng phím Enter để xuống dòng.\r\n\r\n<br>4. Bạn nên sử dụng các mặc định được gợi ý chọn trước bởi hệ thống nếu không cần thiết phải thay đổi. Ví dụ các mục: “Hiển thị lựa chọn”; “Hiển thị lựa chọn theo cột” đã được hệ thống gợi ý chọn trước.');


INSERT INTO `faq_category` (`categoryid`, `faqid`) VALUES (1, 1);
INSERT INTO `faq_category` (`categoryid`, `faqid`) VALUES (1, 2);
INSERT INTO `faq_category` (`categoryid`, `faqid`) VALUES (1, 3);
INSERT INTO `faq_category` (`categoryid`, `faqid`) VALUES (1, 4);
INSERT INTO `faq_category` (`categoryid`, `faqid`) VALUES (1, 5);
INSERT INTO `faq_category` (`categoryid`, `faqid`) VALUES (1, 6);
INSERT INTO `faq_category` (`categoryid`, `faqid`) VALUES (1, 7);
INSERT INTO `faq_category` (`categoryid`, `faqid`) VALUES (1, 8);
INSERT INTO `faq_category` (`categoryid`, `faqid`) VALUES (1, 9);
INSERT INTO `faq_category` (`categoryid`, `faqid`) VALUES (1, 10);
INSERT INTO `faq_category` (`categoryid`, `faqid`) VALUES (1, 11);
INSERT INTO `faq_category` (`categoryid`, `faqid`) VALUES (1, 12);
INSERT INTO `faq_category` (`categoryid`, `faqid`) VALUES (1, 13);
INSERT INTO `faq_category` (`categoryid`, `faqid`) VALUES (2, 10);
INSERT INTO `faq_category` (`categoryid`, `faqid`) VALUES (2, 11);
INSERT INTO `faq_category` (`categoryid`, `faqid`) VALUES (3, 12);
INSERT INTO `faq_category` (`categoryid`, `faqid`) VALUES (3, 13);


INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (1, 'ResourceSync.EsperAddress', 'localhost:8433', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (2, 'Facebook.Like.Code', '<div id="fb-root"></div>\r\n                   <script>(function(d, s, id) {\r\n                     var js, fjs = d.getElementsByTagName(s)[0];\r\n                     if (d.getElementById(id)) {return;}\r\n                     js = d.createElement(s); js.id = id;\r\n                    js.src = "//connect.facebook.net/en_US/all.js#appId=248355421874169&xfbml=1";\r\n                   fjs.parentNode.insertBefore(js, fjs);\r\n                 }(document, \'script\', \'facebook-jssdk\'));</script>\r\n              <div class="fb-like" data-href="%s" data-send="true" data-width="450" data-show-faces="true"></div>', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (3, 'Email.Host', 'mail.eventcliqk.com', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (4, 'Email.Port', '2525', 'java.lang.Integer');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (5, 'Email.Username', 'mailer@eventcliqk.com', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (6, 'Email.Password', 'mailer123', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (7, 'Free forever', 'freemiumuser', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (8, 'Monthly subscription', 'premiumuser', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (9, 'Quarterly subscription', 'premiumuser', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (10, 'Platinum subscription', 'premiumuser', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (11, 'Titanium subscription', 'premiumuser', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (12, 'Câu hỏi lựa chọn một phương án', 'service_multiple_choice_answer_inputs.xml', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (13, 'Câu hỏi mở', 'service_open_ended_single_textbox_answer_inputs.xml', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (14, 'Câu hỏi xếp hạng', 'service_order_list_answer_inputs.xml', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (15, 'Câu hỏi đánh giá theo tiêu chí', 'service_rating_matrix_answer_inputs.xml', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (16, 'Câu hỏi ĐÚNG/SAI', 'service_true_false_answer_inputs.xml', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (17, 'Khảo sát miễn phí', 'service_question_inputs.xml', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (18, 'Khảo sát có trả phí', 'service_question_inputs.xml', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (19, 'Bình chọn', 'service_question_inputs.xml', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (20, 'Câu hỏi lựa chọn nhiều phương án', 'service_multiple_selection_answer_inputs.xml', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (21, 'Question_AnswerOption.Câu hỏi mở', 'textarea', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (22, 'Question_AnswerOption.Câu hỏi xếp hạng', 'checkbox', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (23, 'Question_AnswerOption.Câu hỏi đánh giá theo tiêu chí', 'radio', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (24, 'Question_AnswerOption.Câu hỏi ĐÚNG/SAI', 'checkbox', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (25, 'Question_AnswerOption.Câu hỏi lựa chọn nhiều phương án', 'checkbox', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (26, 'Question_AnswerOption.Câu hỏi lựa chọn một phương án', 'radio', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (27, 'Application.WarFile.Name', 'iMarketing', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (28, 'Application.Website.Name', 'EventCliqk', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (29, 'Application.URL.Prefix', 'secure', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (30, 'Application.Host.URL', 'http://eventcliqk.com', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (31, 'Application.Admin.Email', 'freeman.vu@gmail.com', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (32, 'Application.Logger.Email', 'freeman.vu@gmail.com', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (33, 'Message.ExceedingQuestion', 'Khảo sát đã vượt quá số lượng câu hỏi cho phép, muốn tiếp tục xin vui lòng nâng cấp khảo sát', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (34, 'Message.DeleteNotAllow', 'Bạn không thể xóa câu hỏi khi khảo sát đã được đăng', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (35, 'Message.EditNotAllow', 'Bạn không thể chỉnh sửa câu hỏi khi khảo sát đã được đăng', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (36, 'Message.SurveyAlreadyUpgraded', 'Khảo sát này đã được nâng cấp lên thành khảo sát có trả phí', 'java.lang.String');
INSERT INTO `configs` (`id`, `param`, `value`, `type`) VALUES (37, 'Message.DefaultThankyou', 'Xin cảm ơn thời gian của quý vị, khảo sát trên được tạo hoàn toàn MIỄN PHÍ bởi dịch vụ của EventCliqk. ', 'java.lang.String');



