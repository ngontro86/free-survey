package com.imarketing.conf;


import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.imarketing.dbwriter.DBWriterListener;

public class HibernateUtil {
	private static Logger logger = Logger.getLogger(HibernateUtil.class);

	private volatile static SessionFactory factory = null;

	public static SessionFactory getSessionFactoryInstance(){
		if(factory == null){ // TODO need to implement it smarter as it doesnt care abt synchronization at the moment
			try {
				Configuration cfg = new Configuration().configure();
				logger.info("cfg url: " + cfg.getProperty("connection.url"));	
				factory = cfg.buildSessionFactory();
			} catch (Exception e) {
				logger.error("getSessionFactoryInstance() got exception: ", e);
				DBWriterListener.writeLogs("getSessionFactoryInstance() got exception: " + e.getMessage());
			}
		}
		return factory;
	}
	
	public static void initialize(Object obj){
		Hibernate.initialize(obj);
	}
}
