package com.imarketing.cep.io;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.imarketing.cep.util.SocketData;


public class SocketListener<RECV, SEND> implements Runnable {
	
	private Logger logger = Logger.getLogger(SocketListener.class);
	
	private String authRef;
	
	private int port;
	
	private ServerSocket socket;
	
	private IHandle<SocketData<RECV>> receivedDataHandler = null;
	
	private ConcurrentHashMap<String, SocketComm<RECV, SEND>> readers = new ConcurrentHashMap<String, SocketComm<RECV,SEND>>();
	
	private boolean isInitialized = false;
	private boolean quit = false;
	private volatile int idCount = 0;
	
	public SocketListener(String authRef, int port, IHandle<SocketData<RECV>> handler) {
		this.authRef = authRef;
		this.port = port; this.receivedDataHandler = handler;
		this.isInitialized = (this.receivedDataHandler != null);
		new Timer().schedule(new CleanClosedSocketTask(), 1000, 60000);
	}

	@Override
	public void run() {
		if(!this.isInitialized) return;
		try {
			this.socket = new ServerSocket(port);
			while(!this.quit) {
				try {
					Socket incomingSocket = this.socket.accept();
					logger.info("Socket count: " + idCount + " .New connection at: "+ incomingSocket.getRemoteSocketAddress());
					String id = generateESocketCommID();
					SocketComm<RECV, SEND> reader = new SocketComm<RECV, SEND>(id, this.authRef, incomingSocket, this.receivedDataHandler, true);
					readers.put(id, reader);
					(new Thread((SocketComm<RECV, SEND>)reader, id)).start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			quit();
		} catch (Exception e){ logger.error("run() got exception: ", e); }
	}
	
	public boolean publish(String commId, SEND send){
		try {
			SocketComm<RECV, SEND> reader = readers.get(commId);
			if(reader == null) {
				logger.info("no socketcommon with commid: " + commId);
				return false;
			}
			logger.info(String.format("commId: %s, send: %s", commId, send.toString()));
			reader.write(send);
			return true;
		} catch (Exception e){
			logger.error("publish() got exception: ", e);
			return false;
		}
	}
	
	public void quit(){
		try {
			this.quit = true;
			for (SocketComm<RECV, SEND> reader : readers.values()) reader.quit();
			if(this.socket != null) this.socket.close();
		} catch (Exception e){
			logger.error("quit() got exception: ", e);
		}
	}
	
	private String generateESocketCommID(){
		return String.format("EListener_ESocketComm number: %d", this.idCount++);
	}
	
	class CleanClosedSocketTask extends TimerTask{
		@Override
		public void run() {
			for(Iterator<Entry<String, SocketComm<RECV, SEND>>> it = readers.entrySet().iterator(); it.hasNext();){
				SocketComm<RECV, SEND> sc = it.next().getValue();
				if(sc.isStop()) sc.quit();
			}
		}
	}

}
