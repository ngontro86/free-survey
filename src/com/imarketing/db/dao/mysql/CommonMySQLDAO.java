package com.imarketing.db.dao.mysql;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.imarketing.conf.HibernateUtil;
import com.imarketing.db.dao.ICommonDAO;
import com.imarketing.dbwriter.DBWriterListener;

public abstract class CommonMySQLDAO implements ICommonDAO {
	
	private Logger logger = Logger.getLogger(CommonMySQLDAO.class);
	protected SessionFactory factory;
	protected Session session;
	
	public CommonMySQLDAO() {
		try {
			factory = HibernateUtil.getSessionFactoryInstance();
			if(factory != null) {
				session = factory.openSession();
				if(!session.isOpen()) session = factory.openSession();
			}
		} catch (Exception e){ 
			logger.error("Constructor got exception: ", e); 
			DBWriterListener.writeLogs(CommonMySQLDAO.class + ":Constructor got exception: " + e.getMessage());
		}
	}
	
	@Override
	public void beginTransaction() {
		if(session != null && !session.isOpen()) {
			session = factory.openSession();
			if(!session.isOpen()) session = factory.openSession();
		}
		if(session != null && !session.getTransaction().isActive()) session.beginTransaction();
	}

	@Override
	public void close() {
		try {
			if(session != null && session.isOpen()) {
				if(session.getTransaction().isActive()){
					session.getTransaction().commit();
				}
				if(session.isOpen()) {
					session.disconnect();
					session.close();
				}
			}
		} catch (Exception e){
			logger.error("close() got exception: ", e);
			DBWriterListener.writeLogs(CommonMySQLDAO.class + ":close() got exception: " + e.getMessage());
		}
	}

	@Override
	public void flush() {
		if(session != null) {
			if(session.getTransaction() != null) session.getTransaction().commit();
			session.flush();
		}
	}

	@Override
	public Session getSession() {
		return session;
	}

	@Override
	public void rollBackTransaction() {
		if(session != null && session.getTransaction().isActive()) session.getTransaction().rollback();
	}

}
