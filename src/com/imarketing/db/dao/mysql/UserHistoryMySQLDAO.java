package com.imarketing.db.dao.mysql;

import com.imarketing.db.UserHistory;
import com.imarketing.db.dao.IUserHistoryDAO;

public class UserHistoryMySQLDAO extends CommonMySQLDAO implements IUserHistoryDAO {

	@Override
	public UserHistory insertOrUpdateUserHistory(UserHistory uh) throws Exception {
		beginTransaction();
		UserHistory ret;
		if(uh.getId() > 0) {
			session.saveOrUpdate(uh);
			flush();
			ret = (UserHistory)session.get(UserHistory.class, new Long(uh.getId()));
		} else {
			Long retID = (Long)session.save(uh);
			flush();
			ret = (UserHistory)session.get(UserHistory.class, retID);
		}
		return ret;
	}

}
