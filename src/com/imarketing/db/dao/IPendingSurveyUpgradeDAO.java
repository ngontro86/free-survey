package com.imarketing.db.dao;

import java.util.List;

import com.imarketing.db.PendingSurveyUpgrade;

public interface IPendingSurveyUpgradeDAO extends ICommonDAO {
	
	public boolean insertPendingSurveyUpgrade(PendingSurveyUpgrade psu) throws Exception;
	
	public boolean updatePendingSurveyUpgrade(PendingSurveyUpgrade psu) throws Exception;
	
	public PendingSurveyUpgrade getPendingSurveyUpgradeById(long id) throws Exception;
	
	public List<PendingSurveyUpgrade> getAllPendingRequest(int limit, long startingId) throws Exception; 
	
	public boolean updateSurvey(PendingSurveyUpgrade psu) throws Exception;
	
}
