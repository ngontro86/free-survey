package com.imarketing.db;

import java.io.Serializable;

public class Ads implements Serializable, Comparable<Ads>{
	private static final long serialVersionUID = 1L;

	private long id;
	
	private String type;
	
	private long width;
	
	private long height;
	
	private String provider;
	
	private String description;
	
	private String htmlcode;
	
	private long timestamp;
	
	private long expirytimestamp;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getWidth() {
		return width;
	}

	public void setWidth(long width) {
		this.width = width;
	}

	public long getHeight() {
		return height;
	}

	public void setHeight(long height) {
		this.height = height;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHtmlcode() {
		return htmlcode;
	}

	public void setHtmlcode(String htmlcode) {
		this.htmlcode = htmlcode;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public long getExpirytimestamp() {
		return expirytimestamp;
	}

	public void setExpirytimestamp(long expirytimestamp) {
		this.expirytimestamp = expirytimestamp;
	}

	@Override
	public int compareTo(Ads o) {
		if(o == null) return 1;
		if(getId() > o.getId()) return 1;
		if(getId() < o.getId()) return -1;
		return 0;
	}
	
}
