
var questionobj = null;
var answerobj = null;
var subMenuId = "";

var surveyUID = null;
var surveyType = "";
var questionIdx = 0;
var questionUID = null;
var insertQuestion = false;
var surveyPageId = 0;

function resetParams() {
    surveyUID = null;
    surveyType = "";
    questionIdx = 0;
    questionUID = null;
    insertQuestion = false;
    surveyPageId = 0;
}

$(document).ready(function() {
    displayMenu("Khảo sát đã tạo");
    displayAds();
    jQuery('#dock').jqDock();
});

function displayMenu(name) {
    resetParams();
    $('#surveylist').hide(); $('#createsurvey').hide(); $('#account').hide();
    $('#surveylaunch').hide(); $('#surveyquestion').hide(); $('#svq_answeroption').hide();
    $('#surveyconstruction').hide(); $('#survey_response').hide(); $('#surveyupgrade').hide();
    
    if(name == "Trang chủ") { 
        stopUpdateAds();
        window.location = "index.jsp";
    }
    
    if(name == "Tạo bình chọn mới") {
        alert("Chức năng trên đang trong giai đoạn beta thử nghiệm, xin vui lòng quay lại lần sau");
        displayMenu("Khảo sát đã tạo");
    }
    
    if(name == "Tạo khảo sát mới") {
        surveyUID = null; questionUID = null;
        var respBack = function(data) {
            $("#spinningwheel").html('');
            $('#createsurvey').html(data);
            $('#createsurvey').show();
        }
        $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
        SurveyManagement.getHTMLSurveyBasicInputForm(respBack);
    }
    
    if(name == "Tài khoản") { 
        updatingUserAccount = true;
        var respBack = function(data) {
            $("#spinningwheel").html('');
            $('#account').html(data);
            $('#account').show(); 
        }
        $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
        UserManagement.displayUserAccount(respBack);
    }   
    
    if(name == "Khảo sát đã tạo" || name == "Bình chọn đã tạo") { 
        var respBack = function(data) {
            $("#spinningwheel").html('');
            $('#surveylist').html(data);
            $('#surveylist').show();
        }
        $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
        SurveyManagement.getAllSurveys(respBack); 
    }
}

function golaunch(surveyUID) {
    if(confirm("Bạn chắc chắn muốn đăng khảo sát này?")) {
	    var respBack = function(data) {
	        $("#spinningwheel").html('');
	        if(data != null) {
	            $('#surveylaunch').html(data); $('#surveylaunch').show();
	            $('#surveyconstruction').toggle();
	        }
	    }
	    $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
	    SurveyManagement.launchOneSurvey(surveyUID, respBack);
    }
}

function edittitle(surveyUid) {
    surveyUID = surveyUid;
    $('#surveylist').hide(); $('#createsurvey').hide(); $('#account').hide();
    $('#surveylaunch').hide(); $('#surveyquestion').hide(); $('#svq_answeroption').hide();
    $('#surveyconstruction').hide(); $('#survey_response').hide(); $('#surveyupgrade').hide();
    var respBack = function(data) {
        $("#spinningwheel").html('');
        $('#createsurvey').html(data);
        $('#createsurvey').show();
    }
    $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
    SurveyManagement.editSurveyBasicInformation(surveyUid, respBack);
}

function insertquestion(questionidx,surveyPageid) {
	questionIdx = questionidx;
	surveyPageId = surveyPageid;
	insertQuestion=true;
    var respBack = function(data) {
        $("#spinningwheel").html('');
        $('#surveyconstruction').toggle();
        $('#svq_answeroption').html(data); $('#svq_answeroption').show();
    }
    $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
    SurveyManagement.getHTMLFormFromMapping(surveyType, respBack);
}

function editquestion(surveyUid, questionidx, questionUid) {
    if(confirm("Bạn chắc chắn muốn chỉnh sửa câu hỏi này? Tất cả các đáp án lựa chọn cũ sẽ bị xóa")) {
	    surveyUID = surveyUid; questionUID = questionUid; 
	    var respBack = function(data) {
	        $("#spinningwheel").html('');
	        if(data.success == 'true') {
	            $('#surveyconstruction').toggle();
	            $('#svq_answeroption').html(data.msg);
	            $('#svq_answeroption').show();
	        } else {
	           alert(data.msg);
	        }
	    }
	    $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
	    SurveyManagement.startEdittingQuestion(surveyUID,questionUID,respBack);
    }
}

function deletequestion(questionId,surveyPageId) {
    if(confirm("Bạn chắc chắn muốn xóa câu hỏi này?")) {
	    var respBack = function(data) {
	        $("#spinningwheel").html('');
	        if(data.success == 'true') {
	           $('#surveyconstruction').html(data.msg); $('#surveyconstruction').show();
	        } else {
	           alert(data.msg);
	        }
	    }
	    // delete question
	    $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
	    SurveyManagement.deleteQuestion(surveyUID, surveyPageId, questionId, respBack);
    }
}

function displaysurveyresult(surveyUid) {
    var respBack = function(data) {
        $("#spinningwheel").html('');
        if(data != null && data.length > 0) {
            $('#survey_response').html(data); $('#survey_response').show();  
            $('#surveylist').toggle();
        }
    }
    $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
    SurveyManagement.getOneSurveyResult(surveyUid, respBack);
}

function editsurvey(surveyUid, type) {
    surveyUID = surveyUid; surveyType = type; 
    var displaySurvey = function(data) {
        if(data != null) {
            $("#spinningwheel").html('');
            $('#surveylist').toggle();
            $('#surveyconstruction').html(data);
            $('#surveyconstruction').show();
        }
    }
    $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
    SurveyManagement.displayOneSurvey(surveyUid, displaySurvey);
}

function launchsurvey(surveyUid) {
    if(confirm("Bạn chắc chắn muốn đăng khảo sát này?")) {
	    var respBack = function(data) {
	        if(data != null) {
	            $("#spinningwheel").html('');
	            $('#surveylaunch').html(data); $('#surveylaunch').show();
	            $('#surveylist').toggle();
	        }
	    }
	    $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
	    SurveyManagement.launchOneSurvey(surveyUid, respBack);
    }
}

function deletesurvey(surveyUid) {
    if(confirm("Bạn chắc chắn muốn xóa khảo sát này?")) {
        var generateListSurveyFunc=function(data){
            $("#spinningwheel").html('');
	        $('#surveylist').html(data); $('#surveylist').show();  
        }
        $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
        SurveyManagement.deleteSurvey(surveyUid, generateListSurveyFunc);
    }
}

function upgradesurvey(surveyUid) {
     $.get('docs/survey_upgrade.txt', function(data){
            new $.msgbox({
                onClose: resp,
                title: 'Nâng cấp khảo sát',
                width: 800,
                height: 500,
                type: 'alert',
                content: data
            }).show();}
            );
            
    var resp = function() {
	    if(confirm("Bạn chắc chắn muốn nâng cấp khảo sát trên")){
	        $('#surveylist').hide();
	        surveyUID = surveyUid;
	        var respBack = function(data) {
	            if(data.success == 'true') {   
	                $('#surveyupgrade').html(data.msg);
	                $('#surveyupgrade').show();
	            } else { alert(data.msg); displayMenu("Khảo sát đã tạo"); }
	        }
	        SurveyManagement.getSurveyUpgradeInputForm(surveyUID, respBack);
	    }
    }
}

function upgradeOnesurvey(name) {
    var obj = getFormValueObject(name);
    obj['surveyUID'] = surveyUID;
    var respBack = function(data) {
        alert("Hệ thống đang kiểm tra, khảo sát của bạn sẽ được nâng cấp trong vòng 24 tiếng. Xin cảm ơn");
        displayMenu("Khảo sát đã tạo");
    }
    surveyUID = null;
    SurveyManagement.upgradeOneSurvey(obj, respBack);
}

function processServiceCreateData(obj, name) {
	var divobj = document.getElementById(name);
	divobj.style.display="none";
	if(name.match('Bước 2')) { questionobj = obj; answerobj = null; }
	else if(name.match('Bước 3')) { answerobj = obj; }
}

function createSurveyBasicInputs(obj) {
    surveyType = obj.type;
    var createSurveyResponseBack = function(data) {
        $("#spinningwheel").html('');
        if(data == '' || data.length < 1) {
             new $.msgbox({
                            title: 'Chỉnh sửa khảo sát',
                            type: 'alert',
                            content: 'Bạn chưa đăng nhập'
                        }).show();
            window.location = "index.jsp";
        } else {
		    if(surveyUID == null) {
		        surveyUID = data;
		        var respBack = function(data) {
	                $("#spinningwheel").html('');
	                $('#svq_answeroption').html(data); $('#svq_answeroption').show();
	            }
		        $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
		        SurveyManagement.getHTMLFormFromMapping(surveyType, respBack);
		    } else {
		       alert("Thay đổi đã được lưu");
	           $('#surveylist').hide(); 
	           $('#createsurvey').hide();
	           $('#surveyconstruction').html(data);
	           $('#surveyconstruction').show();   
		    }
	    }
    }
    $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
    if(surveyUID == null) SurveyManagement.createSurvey(obj, true, createSurveyResponseBack);
    else SurveyManagement.saveSurveyBasicInput(obj, surveyUID, createSurveyResponseBack);
}

function addNewSurveyPage() {
    alert('Chức năng trên dành cho người dùng có trả phí');
}

function saveExit() { displayMenu("Khảo sát đã tạo"); }

function addNewQuestion() {
    insertQuestion = false;
    questionIdx = 0; surveyPageId = 0;
    questionUID = null;
    var respBack = function(data) {
        $("#spinningwheel").html('');
        $('#surveyconstruction').toggle();
        $('#svq_answeroption').html(data); $('#svq_answeroption').show();
    }
    $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
    SurveyManagement.getHTMLFormFromMapping(surveyType, "step2", respBack);
}

function addQuestionIntoSurvey() {
    var respBack = function(data) {
        insertQuestion = false;
        $("#spinningwheel").html('');
        if(data.success == 'true') {
		    $('#surveyconstruction').html(data.msg); 
		    $('#surveyconstruction').show();
		    alert("Thay đổi đã được lưu");
	    } else {alert(data.msg); displayMenu("Khảo sát đã tạo"); }
    }
   
    $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
    if(insertQuestion && surveyUID != null && questionIdx != null) {
        SurveyManagement.insertQuestionIntoSurvey(surveyUID, questionIdx, questionobj, answerobj, respBack);
    } else if(surveyUID != null && questionUID != null) {
        SurveyManagement.saveEdittingQuestion(surveyUID, questionUID, questionobj, answerobj, respBack); 
    } else if(surveyUID != null) {
        SurveyManagement.addQuestionIntoSurvey(surveyUID, questionobj, answerobj, respBack);
    }
}

function displayQuestionDiv(obj) {
    $('#surveyquestion').html('<h3><b>Câu hỏi: '+obj.question+'</b></h3>');
    $('#surveyquestion').show();
}