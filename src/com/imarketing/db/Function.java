package com.imarketing.db;

import java.io.Serializable;

@SuppressWarnings({ "unchecked", "serial" })
public class Function implements Serializable, Comparable {
	private long id;
	private String name;
	@Override
	public int compareTo(Object o) {
		if(o == null) return 1;
		if(o instanceof Function){
			Function f = (Function)o;
			if(getId() == f.getId()) return 0;
			return getName().compareTo(f.getName());
		}
		return 0;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
