package com.imarketing.email;

public class EmailTask {
	
	private String subject;
	private String[] toAddresses;
	private String[] ccAddresses = null;
	private String[] bccAddresses = null;
	private String body;
	
	private int retriesCnt = 0;
	
	public EmailTask() {}
	
	public EmailTask(String subject, String[] toAddresses, String body){
		this.subject = subject;
		this.toAddresses = toAddresses;
		this.body = body;
	}
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String[] getToAddresses() {
		return toAddresses;
	}
	public void setToAddresses(String[] toAddresses) {
		this.toAddresses = toAddresses;
	}
	public String[] getCcAddresses() {
		return ccAddresses;
	}
	public void setCcAddresses(String[] ccAddresses) {
		this.ccAddresses = ccAddresses;
	}
	public String[] getBccAddresses() {
		return bccAddresses;
	}
	public void setBccAddresses(String[] bccAddresses) {
		this.bccAddresses = bccAddresses;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}

	public int getRetriesCnt() {
		return retriesCnt;
	}

	public void setRetriesCnt(int retriesCnt) {
		this.retriesCnt = retriesCnt;
	}
}
