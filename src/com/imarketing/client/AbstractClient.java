package com.imarketing.client;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class AbstractClient {
	
	private HttpServletRequest request;
	private HttpServletResponse response;
	
	public AbstractClient(HttpServletRequest req, HttpServletResponse res){
		this.request = req; this.response = res;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}
	
	
}
