package com.imarketing.obj;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class CustomizedInputTable {
	private String name;
	private int width;
	private Set<CustomizedInputField> allFields = new HashSet<CustomizedInputField>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public Set<CustomizedInputField> getAllFields() {
		return allFields;
	}
	public void setAllFields(Set<CustomizedInputField> allFields) {
		this.allFields = allFields;
	}
	
	public void addInputField(CustomizedInputField f){
		allFields.add(f);
	}
	
	public String toString() {
		return "name: " + name + ", allFields: " + allFields.toString();
	}

	public String toHTMLWithDiv(String addedString) {
		String ret = "";
		ret += String.format("<div id='%s'>", name);
		
		ret += addedString;
		
		ret += String.format("<table align='center'> <tr> <td> <h2> %s </h2> </td> </tr> </table>", name);
		ret += String.format("<table width='%dpx' align='left' cellpadding=3 cellspacing=3 style='border:1px solid #000000;background-color:#efefef;'>", width);
		ret += "<tr><td colspan=2></td></tr>";
		ret += "<tr><td colspan=2> </td></tr>";  
		
		String compulsoryFields = "";
		for(CustomizedInputField field : new TreeSet<CustomizedInputField>(this.getAllFields())){
			if(field.getFieldDetails().getMandatory() == 1) compulsoryFields += String.format("%s,", field.getColumn());
		}
		
		for(CustomizedInputField field : new TreeSet<CustomizedInputField>(this.getAllFields())) ret += field.toHTMLString(name, compulsoryFields);
		ret += "<tr><td colspan=2></td></tr> </table></div>"; 
		return ret;
	}
	
	public String toHtmlWithDiv(Map<String, Object> prefixedValues, String addedString) {
		String ret = "";
		ret += String.format("<div id='%s'>", name);
		
		ret += addedString;
		
		ret += String.format("<table align='center'> <tr> <td> <h2> %s </h2> </td> </tr> </table>", name);
		ret += String.format("<table width='%dpx' align='left' cellpadding=3 cellspacing=3 style='border:1px solid #000000;background-color:#efefef;'>", width);
		ret += "<tr><td colspan=2></td></tr>";
		ret += "<tr><td colspan=2> </td></tr>";  
		
		String compulsoryFields = "";
		for(CustomizedInputField field : new TreeSet<CustomizedInputField>(this.getAllFields())){
			if(field.getFieldDetails().getMandatory() == 1) compulsoryFields += String.format("%s,", field.getColumn());
		}
		
		for(CustomizedInputField field : new TreeSet<CustomizedInputField>(this.getAllFields())) 
			ret += field.toHTMLWithFixedValueString(name, compulsoryFields, prefixedValues.containsKey(field.getColumn())? prefixedValues.get(field.getColumn()) : "");
		ret += "<tr><td colspan=2></td></tr> </table></div>"; 
		return ret;	
	}
	
	public String toHTMLString(Map<String, Object> prefixedValues) {
		String ret = "";
		ret += String.format("<div id='%s'>", name);

		ret += String.format("<table align='center'> <tr> <td> <h2> %s </h2> </td> </tr> </table>", name);
		ret += String.format("<table width='%dpx' align='left' cellpadding=3 cellspacing=3 style='border:1px solid #000000;background-color:#efefef;'>", width);
		ret += "<tr><td colspan=2></td></tr>";
		ret += "<tr><td colspan=2> </td></tr>";  
		
		String compulsoryFields = "";
		for(CustomizedInputField field : new TreeSet<CustomizedInputField>(this.getAllFields())){
			if(field.getFieldDetails().getMandatory() == 1) compulsoryFields += String.format("%s,", field.getColumn());
		}
		
		for(CustomizedInputField field : new TreeSet<CustomizedInputField>(this.getAllFields())) 
			ret += field.toHTMLWithFixedValueString(name, compulsoryFields, prefixedValues.containsKey(field.getColumn())? prefixedValues.get(field.getColumn()) : "");
		ret += "<tr><td colspan=2></td></tr> </table></div>"; 
		return ret;	
	}
	
	
	public String toHTMLString() {
		String ret = "";
		ret += String.format("<div id='%s'>", name);

		ret += String.format("<table align='center'> <tr> <td> <h2> %s </h2> </td> </tr> </table>", name);
		ret += String.format("<table width='%dpx' align='center' cellpadding=3 cellspacing=3 style='border:1px solid #000000;background-color:#efefef;'>", width);
		ret += "<tr><td colspan=2></td></tr>";
		ret += "<tr><td colspan=2> </td></tr>";  
		
		String compulsoryFields = "";
		for(CustomizedInputField field : new TreeSet<CustomizedInputField>(this.getAllFields())){
			if(field.getFieldDetails().getMandatory() == 1) compulsoryFields += String.format("%s,", field.getColumn());
		}
		
		for(CustomizedInputField field : new TreeSet<CustomizedInputField>(this.getAllFields())) ret += field.toHTMLString(name, compulsoryFields);
		ret += "<tr><td colspan=2></td></tr> </table></div>"; 
		return ret;
	}
	
}
