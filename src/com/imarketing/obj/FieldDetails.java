package com.imarketing.obj;

public class FieldDetails {
	
	private String name;
	
	private String type;
	
	private String fieldvalues;
	
	private int length;
	
	private int row;
	
	private int column;
	
	private String imgsource;
	
	private int mandatory;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFieldvalues() {
		return fieldvalues;
	}

	public void setFieldvalues(String fieldvalues) {
		this.fieldvalues = fieldvalues;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getMandatory() {
		return mandatory;
	}

	public void setMandatory(int mandatory) {
		this.mandatory = mandatory;
	}
	
	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public String getImgsource() {
		return imgsource;
	}

	public void setImgsource(String imgsource) {
		this.imgsource = imgsource;
	}

	@Override
	public String toString() {
		return String.format("name: %s, type: %s, fieldvalues: %s, length: %d, mandatory: %d", name, type, fieldvalues, length, mandatory);
	}

}
