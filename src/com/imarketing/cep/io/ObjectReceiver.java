package com.imarketing.cep.io;

import java.util.HashMap;
import java.util.Map;


import com.imarketing.cep.util.ObjectEvent;
import com.imarketing.cep.util.SocketData;

public class ObjectReceiver implements IHandle<SocketData<Object>> {
	
	private IHandle<SocketData<ObjectEvent>> outgoingHandler;
	
	private AbstractProcessor<Object, ObjectEvent> defaultProcessor;
	@SuppressWarnings("unchecked")
	private Map<Class, AbstractProcessor<Object, ObjectEvent>> processors = new HashMap<Class, AbstractProcessor<Object,ObjectEvent>>();
	
	public void setOutgoingHandler(IHandle<SocketData<ObjectEvent>> handler){ this.outgoingHandler = handler; }
	
	public void setDefaultProcessor(AbstractProcessor<Object, ObjectEvent> defaultProc) {defaultProcessor = defaultProc;}
	
	@SuppressWarnings("unchecked")
	public void registerProcessor(Class cl, AbstractProcessor<Object, ObjectEvent> processor){
		if(!processors.containsKey(cl)) processors.put(cl, processor);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void handle(SocketData<Object> data) {
		Class cl = data.getData().getClass();
		AbstractProcessor<Object, ObjectEvent> processor = processors.get(cl);
		if(processor != null) processor.process(data, outgoingHandler);
		else defaultProcessor.process(data, outgoingHandler);
	}
}
