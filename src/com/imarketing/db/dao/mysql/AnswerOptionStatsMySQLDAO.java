package com.imarketing.db.dao.mysql;

import java.util.List;

import org.hibernate.Query;

import com.imarketing.db.AnswerOption;
import com.imarketing.db.AnswerOptionStats;
import com.imarketing.db.dao.IAnswerOptionStatsDAO;

public class AnswerOptionStatsMySQLDAO extends CommonMySQLDAO implements IAnswerOptionStatsDAO {

	@Override
	public boolean insertOrUpdateAnswerOptionStats(AnswerOptionStats answerOptionStats) throws Exception {
		beginTransaction();
		session.saveOrUpdate(answerOptionStats);
		flush();
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public AnswerOptionStats getStatsByAnswerOption(AnswerOption option)
			throws Exception {
		beginTransaction();
		Query q = session.createQuery("from AnswerOptionStats where answeroption=:ans");
		q.setEntity("ans", option);
		
		List<AnswerOptionStats> asot = q.list();
		return (asot == null || asot.isEmpty())? null : asot.get(0);
	}

}
