package com.imarketing.db.dao.mysql;


import java.util.List;
import org.hibernate.Query;
import com.imarketing.db.Question;
import com.imarketing.db.QuestionStats;
import com.imarketing.db.dao.IQuestionStatsDAO;

public class QuestionStatsMySQLDAO extends CommonMySQLDAO implements IQuestionStatsDAO {

	@Override
	public QuestionStats getStatsByQuestion(Question question)
			throws Exception {
		beginTransaction();
		Query q = session.createQuery("from QuestionStats where question=:ques");
		q.setEntity("ques", question);
		@SuppressWarnings("unchecked")
		List<QuestionStats> questionStats = q.list();
		if (questionStats.isEmpty()) {
			return null;
		} else {
			return questionStats.get(0);
		}
	}

	@Override
	public boolean insertOrUpdateQuestionStats(QuestionStats questionStats)
			throws Exception {
		beginTransaction();
		session.saveOrUpdate(questionStats);
		flush();
		return true;
	}

	@Override
	public QuestionStats getStatsById(long id) throws Exception {
		beginTransaction();
		return (QuestionStats)session.get(QuestionStats.class, new Long(id));
	}
}
