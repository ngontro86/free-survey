package com.imarketing.common;

import java.io.File;

import org.apache.log4j.Logger;

import com.imarketing.conf.ConfigStore;

public class Common {
	private static Logger logger = Logger.getLogger(Common.class);
	public static Double toDouble(Object obj, Double defaultVal) {
		try {
			if(obj instanceof Double) return (Double)obj;
			if(obj != null) return Double.parseDouble(obj.toString());
			return defaultVal;
		} catch (Exception e){
			logger.debug("toDouble: ",e);
			return defaultVal;
		}
	}
	
	public static String toString(Object obj, String defaultVal){
		try {
			if(obj instanceof String) return (String)obj;
			if(obj != null) return obj.toString();
			return defaultVal;
		} catch (Exception e){
			logger.debug("toString: ",e);
			return defaultVal;
		}
	}
	
	public static Long toLong(Object obj, Long defaultVal){
		try {
			if(obj instanceof Long) return (Long)obj;
			if(obj != null) return Long.parseLong(obj.toString());
			return defaultVal;
		} catch (Exception e){
			logger.debug("toLong: ",e);
			return defaultVal;
		}
	}
	
	public static Integer toInt(Object obj, Integer defaultVal){
		try {
			if(obj instanceof Integer) return (Integer)obj;
			if(obj != null) return Integer.parseInt(obj.toString());
			return defaultVal;
		} catch (Exception e) {
			logger.debug("toInt: ",e);
			return defaultVal;
		}
	}
	
	// Different type of surveys
	public final static String FREE_INTERNET_SURVEY = "Khảo sát miễn phí";
	public final static String PREMIUM_INTERNET_SURVEY = "Khảo sát có trả phí";
	
	// Status of survey
	public final static String SURVEYPOLL_STATUS_EDITING = "Đang khởi tạo";
	public final static String SURVEYPOLL_STATUS_LAUNCHED = "Đã khởi chạy";
	public final static String SURVEYPOLL_STATUS_EXPIRED = "Đã quá thời gian";
	public final static String SURVEY_RESULT_VALID_STATUS = "VALID";
	public final static String SURVEY_RESULT_EXPIRED_STATUS = "EXPIRED";
	
	
	// status of the pending user upgrade
	public final static String UPGRADE_STATUS_UNDERPROCESSING = "UNDER PROCESSING";
	public final static String UPGRADE_STATUS_PROCESSED = "PROCESSED";
	public final static String UPGRADE_STATUS_INVALIDATE = "INVALIDATE";
	
	// Default other answer option
	public static String ANSWEROPTION_DEFAULT = "Khác";
	
	// XML file name
	public final static String XML_CONF_PATH = "WEB-INF" + File.separator+"classes" +File.separator+"com"+File.separator+"imarketing"+File.separator+"conf"+File.separator+"xml" + File.separator;
	
	public final static String ESPER_FILE_PATH = "WEB-INF" + File.separator+"classes" +File.separator+"com"+File.separator+"imarketing"+File.separator+"cep"+File.separator + "resources" + File.separator;
	
	public final static String USER_REGISTRATION_INPUT_FILE = "user_registration_inputs.xml";
	public final static String USER_REGISTRATION_FULL_INPUT_FILE = "user_registration_inputs_full.xml";
	public final static String USER_LOGIN_INPUT_FILE = "user_login_inputs.xml";
	
	public final static String SERVICE_MULTIPLE_CHOICE_ANSWER_INPUT_FILE = "service_multiple_choice_answer_inputs.xml";
	public final static String SERVICE_OPEN_ENDED_SINGLE_TEXTBOX_ANSWER_INPUT_FILE = "service_open_ended_single_textbox_answer_inputs.xml";
	public final static String SERVICE_ORDER_LIST_ANSWER_INPUT_FILE = "service_order_list_answer_inputs.xml";
	public final static String SERVICE_RATING_MATRIX_ANSWER_INPUT_FILE = "service_rating_matrix_answer_inputs.xml";
	public final static String SERVICE_TRUE_FALSE_ANSWER_INPUT_FILE = "service_true_false_answer_inputs.xml";
	
	public final static String SURVEY_BASIC_INPUT_FILE = "survey_basic_inputs.xml";
	public final static String POLL_BASIC_INPUT_FILE = "poll_basic_inputs.xml";
	public final static String SERVICE_QUESTION_INPUT_FILE = "service_question_inputs.xml";
	
	public final static String SURVEY_UPGRADE_INPUT_FILE = "survey_upgrade_inputs.xml";
	
	 // Question type
    public final static String QUESTION_TYPE_MULTIPLE_CHOICES = "Câu hỏi lựa chọn một phương án";
    public final static String QUESTION_TYPE_MORE_THAN_ONE = "Câu hỏi lựa chọn nhiều phương án";
    public final static String QUESTION_TYPE_TRUE_FALSE = "Câu hỏi ĐÚNG/SAI";
    public final static String QUESTION_TYPE_OPEN_ENDED = "Câu hỏi mở";
    public final static String QUESTION_TYPE_RATING_SCALE = "Câu hỏi đánh giá theo tiêu chí";
    public final static String QUESTION_TYPE_ORDER_ITEM = "Câu hỏi xếp hạng";
	
	// Request attributes
	public final static String HTTPSESSION_USER_ATTRIBUTION = String.format("%s_user_attribution", ConfigStore.getConfig("Application.Website.Name"));
	public final static String HTTPSESSION_USERHISTORY = "_userhistory_";
	public final static String HTTPSESSION_CURRENT_MENU_ID = "__currentMenuId__";
	
	// The default page id
	public final static long DEFAULT_PAGE_WITHOUT_AUTHENTICATION = 1;
	public final static long USER_PAGE = 2;
	public final static long MODERATOR_PAGE = 3;
	public final static long ADMIN_PAGE = 4;
	
	// Input field types
	public final static String TYPE_INPUTBOX = "inputbox";
	public final static String TYPE_INPUTBOX_PASSWORD = "inputbox-password";
	public final static String TYPE_PICKLIST_SINGLE = "picklist-single";
	public final static String TYPE_PICKLIST_MULTIPLE = "picklist-multiple";
	public final static String TYPE_INPUTBOX_DATE = "inputbox-date";
	public final static String TYPE_CHECKBOX = "checkbox";
	public final static String TYPE_TEXTBOX = "textbox";
    public final static String TYPE_TEXTAREA = "textarea";
    public final static String TYPE_RADIO = "radio";
    public final static String TYPE_BUTTON = "button";
    public final static String TYPE_LIST_PICKLIST_SINGLE = "list-picklist-single";
	
    // Component for the different key value
    public final static String COMPONENT_SERVICE_MAPPING = "";
    public final static String COMPONENT_QTYPE_AO_MAPPING = "Question_AnswerOption.";
    
    // Credit for survey
    public final static long DEFAULT_ADDED_CREDIT = 1000;
    
    // Display answer option style
    public final static String DISPLAY_ANSWER_OPTION_VERTICAL = "Hiển thị các lựa chọn theo chiều dọc";
	
	public final static long EXPIRATION_TIME = 2*365*24*3600;
	
	public final static String DIVIDER_CONTAINER = 
		String.format("<div class='dividerContainer'>" +
						"<img width='100%%' height='22' alt='Divider Bar' src='img/preset/default.gif'>" +
						"</div>");

}
