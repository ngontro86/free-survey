package com.imarketing.cep.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import org.apache.log4j.Logger;

import com.imarketing.cep.util.SocketData;


public class SocketComm<RECV, SEND> implements Runnable {
	private Logger logger = Logger.getLogger(SocketComm.class);
	private String commID, authRef;
	private Socket sock;
	private ObjectInputStream input;
	private ObjectOutputStream output;
	private volatile boolean stop = false;
	
	private volatile boolean authenticated = false;
	private boolean needAuthenticated;
	private IHandle<SocketData<RECV>> handler;
	
	public SocketComm(String commId, String authRef, Socket sock, IHandle<SocketData<RECV>> h){ this(commId, authRef, sock, h, false); }
	
	public SocketComm(String commId, String authRef, Socket sock, IHandle<SocketData<RECV>> h, boolean needAuth){
		this.commID = commId; this.authRef = authRef;
		this.needAuthenticated = needAuth; this.sock = sock;
		this.handler = h;
		connect();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		try {
			while(!stop) {
				RECV data = (RECV)input.readUnshared();
				if(needAuthenticated && !authenticated) { 
					if(!authRef.equals(data)) { 
						stop = true; quit(); 
						logger.error("Unauthorized violation!"); break;
					} 
					authenticated = true;
				} else {
					logger.error("Got an input: " + data);
					handler.handle(new SocketData<RECV>(commID, data));
				}
			}
		} catch (Exception e){ logger.error("run() got exception: ", e); }
	}

	public void write(SEND obj) {
		try {
			output.writeUnshared(obj);
			output.flush();
		} catch (Exception e) {
			logger.error("write() got exception: ", e);
		}
	}
	
	public boolean isStop() {return stop;}
	
	private void connect() {
		try {
			output = new ObjectOutputStream(new BufferedOutputStream(sock.getOutputStream()));
			output.flush();
			input = new ObjectInputStream(new BufferedInputStream(sock.getInputStream()));
		} catch (Exception e) {
			logger.error("connect() got exception: ", e);
			stop = true;
		}
	}
	
	public void quit() { stop = true; try { if(!sock.isClosed()) sock.close(); } catch (Exception e){ logger.error("quit() got exception: ", e); } }
}
