package com.imarketing.db;

import java.util.HashSet;
import java.util.Set;

public class PollStats {
	private long id;
	
	private Poll poll;
	
	private long total_response;
	
	@SuppressWarnings("unchecked")
	private Set questionstats = new HashSet();
	
	private long lastupdated_timestamp;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Poll getPoll() {
		return poll;
	}

	public void setPoll(Poll poll) {
		this.poll = poll;
	}

	public long getTotal_response() {
		return total_response;
	}

	public void setTotal_response(long total_response) {
		this.total_response = total_response;
	}

	@SuppressWarnings("unchecked")
	public Set getQuestionstats() {
		return questionstats;
	}

	@SuppressWarnings("unchecked")
	public void setQuestionstats(Set questionstats) {
		this.questionstats = questionstats;
	}

	public long getLastupdated_timestamp() {
		return lastupdated_timestamp;
	}

	public void setLastupdated_timestamp(long lastupdated_timestamp) {
		this.lastupdated_timestamp = lastupdated_timestamp;
	}
	
	
}
