package com.imarketing.conf.xml;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.digester.Digester;
import org.apache.log4j.Logger;

import com.imarketing.common.Common;
import com.imarketing.conf.ConfigStore;
import com.imarketing.obj.CustomizedInputField;
import com.imarketing.obj.CustomizedInputTable;
import com.imarketing.obj.FieldDetails;

public class SimpleXMLFileParserFactory {
	
	private static Logger logger = Logger.getLogger(SimpleXMLFileParserFactory.class);
	
	private static volatile Map<String, CustomizedInputTable> tableInputCache = new HashMap<String, CustomizedInputTable>(); 
	
	public static CustomizedInputTable getCustomizedInputTable(String tableName) throws Exception {
		if(!ConfigStore.containConfig(ConfigStore.BASEPATH)) { logger.error("Common base_path hasnt been set yet!"); return null; }
		if(!tableInputCache.containsKey(tableName)) parseFormFile(tableName);
		return tableInputCache.get(tableName);
	}
	
	private static void parseFormFile(String tableName) throws Exception {
		Digester digester = new Digester();
		digester.setValidating(false);
		digester.addObjectCreate("table", CustomizedInputTable.class);
		digester.addSetProperties("table", "name", "name");
		digester.addSetProperties("table", "width", "width");
		digester.addObjectCreate("table/input", CustomizedInputField.class);
		digester.addSetProperties("table/input", "col", "col");
		digester.addSetProperties("table/input", "row", "row");
		digester.addSetProperties("table/input", "column", "column");
		
		digester.addSetNext("table/input", "addInputField");
		
		digester.addObjectCreate("table/input/field", FieldDetails.class);
		digester.addSetProperties("table/input/field", "name", "name");
		digester.addSetProperties("table/input/field", "type", "type");
		digester.addSetProperties("table/input/field", "fieldvalues", "fieldvalues");
		//digester.addSetProperties("table/input/field", "defaultvalue", "defaultvalue");
		digester.addSetProperties("table/input/field", "length", "length");
		digester.addSetProperties("table/input/field", "mandatory", "mandatory");
		digester.addSetProperties("table/input/field", "row", "row");
		digester.addSetProperties("table/input/field", "column", "column");
		digester.addSetProperties("table/input/field", "imgsource", "imgsource");
		
		digester.addSetNext("table/input/field", "setFieldDetails");
		
		File folder = new File(ConfigStore.getConfig(ConfigStore.BASEPATH) + File.separator + Common.XML_CONF_PATH + "forms");
		String fullPath = null;
		for(File f : folder.listFiles()){
			if(f.getName().equals(tableName)) { fullPath = f.getAbsolutePath(); break; } 
		}
		if(fullPath == null) {
			folder = new File(ConfigStore.getConfig(ConfigStore.BASEPATH) + File.separator + Common.XML_CONF_PATH + "forms" + File.separator + "service");
			for(File f : folder.listFiles()) {
				if(f.getName().equals(tableName)) { fullPath = f.getAbsolutePath(); break; }
			}
		}
		if(fullPath == null) { logger.error(String.format("parseFile() couldnt find an xml file named: %s", tableName)); return; }
		CustomizedInputTable cit = (CustomizedInputTable)digester.parse(new File(fullPath));
		tableInputCache.put(tableName, cit);
	}
}
