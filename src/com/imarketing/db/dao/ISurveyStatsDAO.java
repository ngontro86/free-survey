package com.imarketing.db.dao;

import com.imarketing.db.Survey;
import com.imarketing.db.SurveyStats;

public interface ISurveyStatsDAO extends ICommonDAO {
	
	public SurveyStats getStatsBySurvey(Survey survey) throws Exception;
	
	public boolean insertOrUpdateSurveyStats(SurveyStats stats) throws Exception;
	
	public boolean deleteSurveyStats(SurveyStats stats) throws Exception;
}
