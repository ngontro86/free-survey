package com.imarketing.dwr;

import java.util.Arrays;
import java.util.List;

import com.imarketing.ads.AdsListenerFactory;
import com.imarketing.db.Ads;

public class AdsManagement extends AbstractManagement {
	
	public AdsManagement() throws Exception {
		super();
	}	
	
	public String getRandomAds(String type, int max, boolean v) {
		Ads[] list = AdsListenerFactory.getRandomAds(type, max);
		String ret = String.format("<table border='0'>");
		ret += v? "" : "<tr>";
		for(Ads a : list) {
			ret += v? "<tr>" : "<td>";
			ret += a.getHtmlcode();
			ret += v? "</tr>" : "</td>";
		}
		ret += v? "</table>" : "</tr></table>";
		return ret;
	}
	
	@SuppressWarnings("unchecked")
	public List getRandomAds(String type, int max) { 
		Ads[] ret = AdsListenerFactory.getRandomAds(type, max);
		return Arrays.asList(ret); 
	}
	
}
