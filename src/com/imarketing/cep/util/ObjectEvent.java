package com.imarketing.cep.util;

import java.io.Serializable;

public class ObjectEvent implements Serializable {
	private static final long serialVersionUID = -828278581841581545L;
	public String eventID;
	public Object event;
	public ObjectEvent(String id, Object e) { this.eventID = id; this.event = e; }
}
