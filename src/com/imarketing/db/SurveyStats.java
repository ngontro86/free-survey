package com.imarketing.db;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SurveyStats {
	private long id;
	
	private Survey survey;
	
	private long total_response;
	
	@SuppressWarnings("unchecked")
	private Set questionstats = new HashSet();
	
	private long lastupdated_timestamp;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Survey getSurvey() {
		return survey;
	}

	public void setSurvey(Survey survey) {
		this.survey = survey;
	}

	public long getTotal_response() {
		return total_response;
	}

	public void setTotal_response(long total_response) {
		this.total_response = total_response;
	}

	@SuppressWarnings("unchecked")
	public Set getQuestionstats() {
		return questionstats;
	}

	@SuppressWarnings("unchecked")
	public void setQuestionstats(Set questionstats) {
		this.questionstats = questionstats;
	}

	public long getLastupdated_timestamp() {
		return lastupdated_timestamp;
	}

	public void setLastupdated_timestamp(long lastupdated_timestamp) {
		this.lastupdated_timestamp = lastupdated_timestamp;
	}
	
	@SuppressWarnings("unchecked")
	public String toHTMLString() {
		String ret = "";
		
		ret += String.format("<div id='%d'><p class='surveytitle'><b>%s</b></p>", getId(), "Result: " + survey.getTitle());
		
		ret += String.format("<table width='10%%' cellspacing='0' cellpadding='0' border='0'>" +
				"<tbody>" +
				"<tr>" +
				"<td>");
		ret += String.format("<table width='10%%' cellspacing='0' cellpadding='0' border='0'>" +
				"<tbody>");
		
		List<QuestionStats> l = new ArrayList<QuestionStats>(getQuestionstats());
		Collections.sort(l, new Comparator<QuestionStats>(){
			@Override
			public int compare(QuestionStats o1, QuestionStats o2) {
				if(o1.getQuestion().getIdx() > o2.getQuestion().getIdx()) return 1;
				if(o1.getQuestion().getIdx() == o2.getQuestion().getIdx()) return 0;
				return -1;
			}
		});
		for(QuestionStats qstats : l) ret += qstats.toHTMLString(survey.getType());
		ret += "</tbody></table>";
		ret += String.format("</td></tr></tbody></table></div>");
		return ret;
	}
	
}
