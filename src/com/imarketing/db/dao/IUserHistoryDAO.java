package com.imarketing.db.dao;

import com.imarketing.db.UserHistory;

public interface IUserHistoryDAO extends ICommonDAO {
	
	public UserHistory insertOrUpdateUserHistory(UserHistory uh) throws Exception;
	
}
