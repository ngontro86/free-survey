package com.imarketing.cep.io;

import java.net.Socket;

import org.apache.log4j.Logger;

import com.imarketing.cep.util.SocketData;


public class SocketPublisher<RECV, SEND> implements IHandle<SEND> {

	private Logger logger = Logger.getLogger(SocketPublisher.class); 
	
	private SocketComm<RECV, SEND> socketComm;
	private Thread socketReader;
	private IHandle<SocketData<RECV>> incomingHandler;
	
	private Socket socket;
	private String host, authRef;
	
	private int port;
	
	public SocketPublisher(String host, int port, String authRef, IHandle<SocketData<RECV>> handler){
		this.incomingHandler = handler;
		this.host = host; this.port = port; 
		this.authRef = authRef;
		if(!init()) logger.error(String.format("SocketPublisher: cannot init the connection, host =%s, port =%d", this.host, this.port));
	}
	
	private boolean init() {
		try {
			this.socket = new Socket(this.host, this.port);
			this.socketComm = new SocketComm<RECV, SEND>("" , authRef, this.socket, this.incomingHandler);
			this.socketReader = new Thread(this.socketReader);
			this.socketReader.start();
			return true;
		} catch (Exception e) {
			logger.error("init() got exception",e);
			return false;
		}
	}
	
	@Override
	public void handle(SEND event) {
		if(this.socketComm == null) return;
		this.socketComm.write(event);
	}
	

}
