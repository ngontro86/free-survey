package com.imarketing.client;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.imarketing.db.FAQ;
import com.imarketing.db.FAQCategory;
import com.imarketing.db.dao.DAOFactory;
import com.imarketing.db.dao.IFAQCategoryDAO;
import com.imarketing.db.dao.IFAQDAO;

public class FAQClient extends AbstractClient {

	public FAQClient(HttpServletRequest req, HttpServletResponse res) {
		super(req, res);
	}
	
	public String getAllCategories() throws Exception {
		if(ClientStoreFactory.getInstance().cached(String.format("%s:getAllCategories", FAQClient.class.toString()))) {
			return ClientStoreFactory.getInstance().getCachedString(String.format("%s:getAllCategories", FAQClient.class.toString()));
		}
		String ret = "";
		IFAQCategoryDAO faqCategoryDAO = DAOFactory.getDefaultFactory().getFAQCategoryDAO();
		List<FAQCategory> faqCategories = faqCategoryDAO.getAllFAQCategories();
		int i = 0;
		for(FAQCategory fc : faqCategories) {
			ret += String.format("<a href='%s'>%d. %s</a><br />", "javascript:showFAQs("+fc.getId()+")", ++i, fc.getName());
			
		}
		faqCategoryDAO.close();
		ClientStoreFactory.getInstance().cacheObject(String.format("%s:getAllCategories", FAQClient.class.toString()), ret);
		return ret;
	}
	
	public static String getAllFAQs(long categoryID) throws Exception {
		if(ClientStoreFactory.getInstance().cached(String.format("%s:getAllFAQs", FAQClient.class.toString()))) {
			return ClientStoreFactory.getInstance().getCachedString(String.format("%s:getAllFAQs", FAQClient.class.toString()));
		}
		IFAQDAO faqDAO = DAOFactory.getDefaultFactory().getFAQDAO();
		List<FAQ> listFAQs = faqDAO.getAllFAQs(categoryID);
		String ret = "";
		for(FAQ faq : listFAQs) {
			ret += String.format("<li>%d. <br> <b>Câu hỏi: %s</b><br> Trả lời: %s</li>", faq.getId(), faq.getQuestion(), faq.getAnswer());
		}
		faqDAO.close();
		ClientStoreFactory.getInstance().cacheObject(String.format("%s:getAllFAQs", FAQClient.class.toString()), ret);
		return ret;
	}

}
