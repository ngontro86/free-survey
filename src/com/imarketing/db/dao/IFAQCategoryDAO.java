package com.imarketing.db.dao;

import java.util.List;

import com.imarketing.db.FAQCategory;

public interface IFAQCategoryDAO extends ICommonDAO {
	
	public List<FAQCategory> getAllFAQCategories() throws Exception;
	
	public FAQCategory getFAQCategory(long id) throws Exception;
}
