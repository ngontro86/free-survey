package com.imarketing.ads;

import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.imarketing.db.Ads;
import com.imarketing.db.dao.DAOFactory;
import com.imarketing.db.dao.IAdsDAO;
import com.imarketing.dbwriter.DBWriterListener;

public class AdsListenerFactory implements ServletContextListener {
	private Logger logger = Logger.getLogger(AdsListenerFactory.class);

	private static List<Ads> store = new CopyOnWriteArrayList<Ads>();
	
	private static Ads ourAds = null;
	
	private static final int UPDATE_FREQ = 30 * 60 * 1000;
	
	private static final double SHUFFLING_PROBABILITY = 0.05;
	
	public static Ads getRandomAds() {return store.get(0); }
	
	public static Ads getOneAds() {return ourAds;}
	
	public static Ads[] getRandomAds(String type, int max) {
		return getRandomAds(type, max, false);
	}
	
	public static Ads[] getRandomAds(String type, int max, boolean shuffle) {
		Ads[] ret = new Ads[max];
		int idx = 0;
		if(shuffle || Math.random() < SHUFFLING_PROBABILITY) Collections.shuffle(store);
		for(int i = 0 ; i < store.size(); i++) {
			Ads tmp = store.get(i);
			if(type.equals(tmp.getType())) { 
				if(idx < max) ret[idx++] = tmp; 
				else break; 
			}
		}
		return ret; 
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		logger.error("contextDestroyed, stop the ad loading timer task now!");
		reloadAdsTask.cancel();
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		try {
			logger.error("contextInitilized, need to reload the ads!");
			new Timer().scheduleAtFixedRate(reloadAdsTask, 1000, UPDATE_FREQ); 
		} catch (Exception e){
			logger.error("contextInitialized() got exception; ", e);
			DBWriterListener.writeLogs("AdsListenerFactory: contextInitialized() got exception: " +e.getMessage());
		}
	}
	
	private TimerTask reloadAdsTask = new TimerTask() {
		@Override
		public void run() {
			store.clear();
			try {
				IAdsDAO adsDAO = DAOFactory.getDefaultFactory().getAdsDAO();
				List<Ads> tmp = adsDAO.getAllAds();
				ourAds = adsDAO.getAdsByProvider("EventCliqk");
				store.addAll(tmp);
				adsDAO.close();
			} catch (Exception e){ 
				logger.error("ReloadAdsTask got exception: ", e); 
				DBWriterListener.writeLogs("ReloadAdsTask: got exception: " +e.getMessage());
			}
		}
	};
}
