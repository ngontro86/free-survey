package com.imarketing.db.listener;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.imarketing.common.Common;
import com.imarketing.db.Poll;
import com.imarketing.db.Survey;
import com.imarketing.db.dao.DAOFactory;
import com.imarketing.db.dao.IPollDAO;
import com.imarketing.db.dao.ISurveyDAO;
import com.imarketing.dbwriter.DBWriterListener;
import com.imarketing.servicedef.ServiceDefStore;
import com.imarketing.time.GlobalTime;

public class DBCleaner implements ServletContextListener {

	private Logger logger = Logger.getLogger(DBCleaner.class);
	
	private static final int UPDATE_FREQ = 30 * 60 * 1000; // 30 mins once
	private static final int LIMIT = 20;
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		logger.info("contextDestroyed() got called, stop the task to update DB!");
		updateDBTask.cancel();
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		logger.info("contextInitialized() got called, initilizing the task to update DB!");
		new Timer().scheduleAtFixedRate(updateDBTask, 10000, UPDATE_FREQ);
	}
	
	private void setSurveyStatus(Survey s){
		if(s.getCreated_timestamp() + ServiceDefStore.getServiceTypeDef(s.getType()).getExpirationduration() * 24 * 60 * 60 <= GlobalTime.currentTimeSeconds())
			s.setDisplaysurveystatus(Common.SURVEYPOLL_STATUS_EXPIRED);
		
		if(s.getCreated_timestamp() + ServiceDefStore.getServiceTypeDef(s.getType()).getResultdisplayduration() * 24 * 60 * 60 <= GlobalTime.currentTimeSeconds())
				s.setDisplayresultstatus(Common.SURVEY_RESULT_EXPIRED_STATUS);
	}
	
	private void setPollStatus(Poll p) {
		if(p.getCreated_timestamp() + ServiceDefStore.getServiceTypeDef("Bình chọn").getExpirationduration() * 24 * 60 * 60 <= GlobalTime.currentTimeSeconds())
			p.setStatus(Common.SURVEY_RESULT_EXPIRED_STATUS);
	}
	
	private void workWithSurvey() throws Exception {
		ISurveyDAO surveyDAO = DAOFactory.getDefaultFactory().getSurveyDAO();
		long startingId = 0;
		List<Survey> list = null;
		while(list == null || list.size() == LIMIT) {
			list = surveyDAO.getValidSurveys(LIMIT, startingId);
			if(list != null && !list.isEmpty()) {
				for(Survey s : list) {
					setSurveyStatus(s);
					surveyDAO.updateSurvey(s);
				}
				startingId = list.get(0).getId();
			} else { break; }
		}
		if(list != null && !list.isEmpty()) {
			for(Survey s : list) {
				setSurveyStatus(s);
				surveyDAO.updateSurvey(s);
			}
		}
		surveyDAO.close();
	}
	
	private void workWithPoll() throws Exception {
		IPollDAO pollDAO = DAOFactory.getDefaultFactory().getPollDAO();
		long startingId = 0;
		List<Poll> list = null;
		while(list == null || list.size() == LIMIT) {
			list = pollDAO.getValidPolls(LIMIT, startingId);
			if(list != null && !list.isEmpty()) {
				for(Poll p : list) {
					setPollStatus(p);
					pollDAO.updatePoll(p);
				}
				startingId = list.get(0).getId();
			} else { break; }
		}
		if(list != null && !list.isEmpty()) {
			for(Poll p : list) {
				setPollStatus(p);
				pollDAO.updatePoll(p);
			}
		}
		pollDAO.close();
	}
	
	private TimerTask updateDBTask = new TimerTask() {
		@Override
		public void run() {
			try {
				workWithSurvey();
				workWithPoll();
			} catch (Exception e){
				logger.error("updateDBTask() got exception: ", e);
				DBWriterListener.writeLogs("updateDBTask() got exception: " +e.getMessage());
			}
		}
	};
}
