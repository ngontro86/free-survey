package com.imarketing.db.dao.mysql;

import com.imarketing.db.Logs;
import com.imarketing.db.dao.ILogsDAO;

public class LogsMySQLDAO extends CommonMySQLDAO implements ILogsDAO {

	@Override
	public boolean insertLog(Logs log) throws Exception {
		beginTransaction();
		session.saveOrUpdate(log);
		flush();
		return true;
	}
}
