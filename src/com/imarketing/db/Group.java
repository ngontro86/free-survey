package com.imarketing.db;

import java.io.Serializable;
import java.util.Set;

@SuppressWarnings({ "unchecked", "serial" })
public class Group implements Serializable, Comparable {
	private long id;
	
	private String name;
	private String description;
	
	private Set functions;
	
	private Set accessiblemenus;
	
	private Set accessibleservices;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set getFunctions() {
		return functions;
	}

	public void setFunctions(Set functions) {
		this.functions = functions;
	}

	public Set getAccessiblemenus() {
		return accessiblemenus;
	}

	public void setAccessiblemenus(Set accessiblemenus) {
		this.accessiblemenus = accessiblemenus;
	}
	
	public Set getAccessibleservices() {
		return accessibleservices;
	}

	public void setAccessibleservices(Set accessibleservices) {
		this.accessibleservices = accessibleservices;
	}

	@Override
	public int compareTo(Object o) {
		if(o == null) return 1;
		if(o instanceof Group) {
			Group g = (Group)o;
			if(getId() == g.getId()) return 0;
			return getName().compareTo(g.getName());
		}
		return 1;
	}	
}
