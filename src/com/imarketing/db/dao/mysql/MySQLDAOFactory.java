package com.imarketing.db.dao.mysql;

import com.imarketing.db.dao.DAOFactory;
import com.imarketing.db.dao.IAccessibleMenuDAO;
import com.imarketing.db.dao.IAdsDAO;
import com.imarketing.db.dao.IAnswerOptionDAO;
import com.imarketing.db.dao.IAnswerOptionStatsDAO;
import com.imarketing.db.dao.IConfigDAO;
import com.imarketing.db.dao.IFAQCategoryDAO;
import com.imarketing.db.dao.IFAQDAO;
import com.imarketing.db.dao.IFunctionDAO;
import com.imarketing.db.dao.IGroupDAO;
import com.imarketing.db.dao.ILogsDAO;
import com.imarketing.db.dao.IParticipantDAO;
import com.imarketing.db.dao.IPendingSurveyUpgradeDAO;
import com.imarketing.db.dao.IPendingUserUpgradeDAO;
import com.imarketing.db.dao.IPollDAO;
import com.imarketing.db.dao.IPollStatsDAO;
import com.imarketing.db.dao.IQuestionDAO;
import com.imarketing.db.dao.IQuestionStatsDAO;
import com.imarketing.db.dao.IServiceTypeDefDAO;
import com.imarketing.db.dao.IOneResponseDAO;
import com.imarketing.db.dao.ISurveyDAO;
import com.imarketing.db.dao.ISurveyPageDAO;
import com.imarketing.db.dao.ISurveyStatsDAO;
import com.imarketing.db.dao.IUserCreditDAO;
import com.imarketing.db.dao.IUserDAO;
import com.imarketing.db.dao.IUserHistoryDAO;

public class MySQLDAOFactory extends DAOFactory{
	
	@Override
	public IUserDAO getUserDAO() {
		return new UserMySQLDAO();
	}

	@Override
	public IAnswerOptionDAO getAnswerOptionDAO() {
		return new AnswerOptionMySQLDAO();
	}

	@Override
	public IFunctionDAO getFunctionDAO() {
		return new FunctionMySQLDAO();
	}

	@Override
	public IGroupDAO getGroupDAO() {
		return new GroupMySQLDAO();
	}

	@Override
	public IQuestionDAO getQuestionDAO() {
		return new QuestionMySQLDAO();
	}

	@Override
	public IOneResponseDAO getResponseDAO() {
		return new OneResponseMySQLDAO();
	}
	
	@Override
	public IUserCreditDAO getUserCreditDAO() {
		return new UserCreditMySQLDAO();
	}

	@Override
	public IUserHistoryDAO getUserHistoryDAO() {
		return new UserHistoryMySQLDAO();
	}

	@Override
	public IAccessibleMenuDAO getAccessibleMenuDAO() {
		return new AccessibleMenuMySQLDAO();
	}

	@Override
	public IFAQDAO getFAQDAO() {
		return new FAQMySQLDAO();
	}

	@Override
	public IConfigDAO getConfigDAO() {
		return new ConfigMySQLDAO();
	}

	@Override
	public IFAQCategoryDAO getFAQCategoryDAO() {
		return new FAQCategoryMySQLDAO();
	}

	@Override
	public IAnswerOptionStatsDAO getAnswerOptionStatsDAO() {
		return new AnswerOptionStatsMySQLDAO();
	}

	@Override
	public IQuestionStatsDAO getQuestionStatsDAO() {
		return new QuestionStatsMySQLDAO();
	}

	@Override
	public ISurveyDAO getSurveyDAO() {
		return new SurveyMySQLDAO();
	}

	@Override
	public ISurveyPageDAO getSurveyPageDAO() {
		return new SurveyPageMySQLDAO();
	}

	@Override
	public IServiceTypeDefDAO getServiceTypeDefDAO() {
		return new ServiceTypeDefMySQLDAO();
	}

	@Override
	public ISurveyStatsDAO getSurveyStatsDAO() {
		return new SurveyStatsMySQLDAO();
	}

	@Override
	public IAdsDAO getAdsDAO() {
		return new AdsMySQLDAO();
	}

	@Override
	public ILogsDAO getLogsDAO() {
		return new LogsMySQLDAO();
	}

	@Override
	public IParticipantDAO getParticipantDAO() {
		return new ParticipantMySQLDAO();
	}

	@Override
	public IPollDAO getPollDAO() {
		return new PollMySQLDAO();
	}

	@Override
	public IPollStatsDAO getPollStatsDAO() {
		return new PollStatsMySQLDAO();
	}

	@Override
	public IPendingUserUpgradeDAO getPendingUserUpgradeDAO() {
		return new PendingUserUpgradeMySQLDAO();
	}

	@Override
	public IPendingSurveyUpgradeDAO getPendingSurveyUpgradeDAO() {
		return new PendingSurveyUpgradeMySQLDAO();
	}
	
}
