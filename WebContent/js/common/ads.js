
var divname = new Array();
var adtype = new Array();
var adnum = new Array();
var vertical = new Array();
var nextCall = null;
var count = 0;
var period = 0;

function autoRefresh(name, type, num, t, v) {
    divname[count] = name;
    adtype[count] = type;
    adnum[count] = num;
    period = t;
    vertical[count] = v;
    count = count + 1;
}

function startUpdateAds() { updateAds(); }

function stopUpdateAds() {
    clearTimeout(nextCall); 
    count = 0;
    divname = new Array(); 
    adtype = new Array();
    vertical = new Array(); 
}

function updateAd(name, type, num, vertical) {
    var respBack = function(data) {
            var divObj = document.getElementById(name);
            if(!divObj || divObj == null) return;
            divObj.innerHTML = data;
    }
    AdsManagement.getRandomAds(type, num, vertical, respBack);    
}

function updateAds() {
    for(var i = 0; i < divname.length; i++) {
	    updateAd(divname[i], adtype[i], adnum[i], vertical[i]);
    }
    nextCall = setTimeout('updateAds();', period);
}


function displaySlider(div, type, num) {
    var respBack = function(data) {
        var html  = "<div id='page-wrap'>";
            html += "<div class='slider-wrap'>";
	            html += "<div id='main-photo-slider' class='csw'>";
		            html += "<div class='panelContainer'>";
				        for(var i=0; i< data.length; i++) {
				            html += "<div class='panel' title ='Panel "+(i+1)+"'>";
					            html += "<div class='wrapper'><img src='"+data[i].htmlcode+"' alt='temp'/>";
					                html += "<div class='photo-meta-data'>Credit: " +data[i].provider+"</div>";
					            html += "</div>";
					        html += "</div>";
				        }
				        html +="</div></div>";
		        
			        html += "<a href='#1' class='cross-link active-thumb'><img src='"+data[0].htmlcode+"' class='nav-thumb' alt='temp-thumb'/></a>";
			        html += "<div id='movers-row'>";
				        for(var i=1;i< data.length; i++) {
				            html += "<div><a href='#' class='cross-link'> <img src='"+data[i].htmlcode+"' class='nav-thumb' alt='temp-thumb'/></a></div>";
				        }
		            html += "</div>";
        
        html +="</div></div>";
        
        $("#"+div).html(html);
        $("#"+div).show();
        startSliding(div);
    }
    AdsManagement.getRandomAds(type, num, respBack);
}

function startSliding(div) {
    var theInt = null;
        var $crosslink, $navthumb;
        var curclicked = 0;
        
        theInterval = function(cur){
            clearInterval(theInt);
            
            if( typeof cur != 'undefined' )
                curclicked = cur;
            
            $crosslink.removeClass("active-thumb");
            $navthumb.eq(curclicked).parent().addClass("active-thumb");
                $(".stripNav ul li a").eq(curclicked).trigger('click');
            
            theInt = setInterval(function(){
                $crosslink.removeClass("active-thumb");
                $navthumb.eq(curclicked).parent().addClass("active-thumb");
                $(".stripNav ul li a").eq(curclicked).trigger('click');
                curclicked++;
                if( 6 == curclicked )
                    curclicked = 0;
                
            }, 3000);
        };
        
        $(function(){
            $("#main-photo-slider").codaSlider();
            
            $navthumb = $(".nav-thumb");
            $crosslink = $(".cross-link");
            
            $navthumb
            .click(function() {
                var $this = $(this);
                theInterval($this.parent().attr('href').slice(1) - 1);
                return false;
            });
            theInterval();
        });
}

