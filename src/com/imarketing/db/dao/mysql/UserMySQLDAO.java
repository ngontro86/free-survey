package com.imarketing.db.dao.mysql;

import java.util.List;
import java.util.Map;

import org.hibernate.Query;

import com.imarketing.db.Group;
import com.imarketing.db.User;
import com.imarketing.db.dao.IUserDAO;

public class UserMySQLDAO extends CommonMySQLDAO implements IUserDAO {
	
	public UserMySQLDAO() { super(); }
	
	@Override
	@SuppressWarnings("unchecked")
	public User getUser(String username, String password) throws Exception {
		beginTransaction();
		Query query = session.createQuery("from User where username_emailaddress = :username AND password =:password");
		query.setString("username", username);
		query.setString("password", password);
		List l = query.list();
		User ret = (l != null && !l.isEmpty()) ? (User)l.get(0) : null;
		return ret;
	}

	@Override
	public boolean insertUser(User u) throws Exception {
		if(!userExist(u.getUsername_emailaddress())) return false;
		beginTransaction();
		session.saveOrUpdate(u);
		flush();
		return true;
	}

	@Override
	public boolean insertUser(Map<String, Object> map) throws Exception {
		// Not implemented yet
		return false;
	}

	@Override
	public boolean updateUserGroup(User u, Group newGroup) throws Exception {
		u.setRolegroup(newGroup);
		beginTransaction();
		session.saveOrUpdate(u);
		flush();
		return false;
	}

	@Override
	public boolean userExist(String username) throws Exception {
		User u = getAnyUser(username);
		return u == null;
	}

	@Override
	public User getUser(long id) throws Exception {
		beginTransaction();
		return (User)session.get(User.class, new Long(id));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public User getAnyUser(String username) throws Exception {
		beginTransaction();
		Query query = session.createQuery("from User where username_emailaddress = :username");
		query.setString("username", username);
		List<User> ret = query.list();
		return ret != null && !ret.isEmpty()? (User)ret.get(0) : null;
	}

	@Override
	public User updateUser(User u) throws Exception {
		beginTransaction();
		session.update(u);
		flush();
		return u;
	}

	@SuppressWarnings("unchecked")
	@Override
	public User getUserByUID(String uid) throws Exception {
		beginTransaction();
		Query query = session.createQuery("from User where uid = :uid");
		query.setString("uid", uid);
		List<User> l = query.list();
		return (l == null || l.isEmpty())? null : (User)l.get(0);
	}

}
