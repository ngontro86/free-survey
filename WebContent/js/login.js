

function postLoginCheckData(data) {
    $("#spinningwheel").html('');
    if(data != null && data.success == "true"){
        window.location="user.jsp";
    } else {
    	alert(data.message)
    }
}

function validateLogin(){
    var obj = getFormValueObject("loginForm");
    var username=obj.username;
    var password=obj.password;
    if(username!="" && password!=""){
        $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');      
        UserManagement.validateLogin(username,password,postLoginCheckData);
    } else { alert("Vui lòng điền đủ email và mật khẩu"); }   
}

function checkLogin() {
	var username = document.getElementById("username").value;
	var password = document.getElementById("password").value;
	if(username!="" && password!=""){ 
	    $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
        UserManagement.validateLogin(username,password,postLoginCheckData);
    } else { alert("Vui lòng điền đủ email và mật khẩu"); }   
}

function logout() {
    var respBack = function(data) { 
        $("#spinningwheel").html('');
        stopUpdateAds(); 
        window.location="index.jsp";
        alert(data);          
    }
	$("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
	UserManagement.logout(respBack);
}

