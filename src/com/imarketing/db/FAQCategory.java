package com.imarketing.db;

import java.util.Set;

@SuppressWarnings("unchecked")
public class FAQCategory implements Comparable<FAQCategory> {
	private long id;
	private String name;
	private Set faqs;
	
	public Set getFaqs() {
		return faqs;
	}
	
	public void setFaqs(Set faqs) {
		this.faqs = faqs;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public int compareTo(FAQCategory o) {
		if(o == null) return 1;
		if(getId() > o.getId()) return 1;
		if(getId() < o.getId()) return -1;
		return 0;
	}
}
