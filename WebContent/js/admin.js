
$(document).ready(function() {displayMenu("Survey upgrade request");});


function displayMenu(name) {
    $('#surveyupgraderequestlist').hide(); $('#surveyupgraderequestlist').hide();
    $('#userlist').hide(); $('#moderatorlist').hide(); $('#surveylist').hide();
    $('#polllist').hide(); 
    
    if(name == "Survey upgrade request") { 
        var respBack = function(data) {
            $("#spinningwheel").html('');
            if(data != null && data.length > 0) {
	            $('#surveyupgraderequestlist').html(data);
	            $('#surveyupgraderequestlist').show();
            }
        }
        $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
        PendingSurveyUpgradeManagement.getAllPendingSurveyUpgrade(50, respBack);   
    }
}


function confirmsurveyupgrade(requestId) {
    var respBack = function(data) { if(data) alert("Successfully confirm the request"); displayMenu("Survey upgrade request"); }
    PendingSurveyUpgradeManagement.confirm(requestId, respBack);
}

function invalidatesurveyupgrade(requestId) {
    var respBack = function(data) { if(data) alert("Successfully invalidate the request"); displayMenu("Survey upgrade request"); }
    PendingSurveyUpgradeManagement.invalidate(requestId, respBack);
}

/*
// This part is copied directly from login.js - need to revise this in the future
function postLoginCheckData(data) {
    $("#spinningwheel").html('');
    if(data != null && data.success == "true"){
        window.location="admin.jsp";
    } else {
        alert(data.message)
    }
}

function validateLogin(){
    var obj = getFormValueObject("loginForm");
    var username=obj.username;
    var password=obj.password;
    if(username!="" && password!=""){
        $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');      
        UserManagement.validateLogin(username,password,postLoginCheckData);
    } else { alert("Vui lòng điền đủ email và mật khẩu"); }   
}

function checkLogin() {
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    if(username!="" && password!=""){ 
        $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
        UserManagement.validateLogin(username,password,postLoginCheckData);
    } else { alert("Vui lòng điền đủ email và mật khẩu"); }   
}

function logout() {
    var respBack = function(data) { 
        $("#spinningwheel").html('');
        window.location="index.jsp";
        alert(data);          
    }
    $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
    UserManagement.logout(respBack);
}
*/
