package com.imarketing.db.dao.mysql;

import java.util.List;

import org.hibernate.Query;

import com.imarketing.db.Group;
import com.imarketing.db.dao.IGroupDAO;

public class GroupMySQLDAO extends CommonMySQLDAO implements IGroupDAO {

	public GroupMySQLDAO() {
		super();
	}
	
	@Override
	public boolean insertGroup(Group g) throws Exception {
		beginTransaction();
		session.save(g);
		flush();
		return true;
	}

	@Override
	public Group getGroup(long id) throws Exception {
		beginTransaction();
		return (Group)session.get(Group.class, new Long(id));
	}

	@SuppressWarnings("unchecked")
	@Override
	public Group getGroup(String groupName) throws Exception {
		beginTransaction();
		Query q = session.createQuery("from Group where name = :gname");
		q.setString("gname", groupName);
		List l = q.list();
		return (l == null || l.isEmpty())? null : (Group)l.get(0);
	}

}
