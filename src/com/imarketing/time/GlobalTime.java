package com.imarketing.time;

import java.util.Date;

public class GlobalTime {
	
	private static int SECOND = 1000;
	
	public static long currentTimeSeconds() {
		return System.currentTimeMillis()/SECOND;
	}
	
	public static Date getDate(long timeInSeconds) {
		return new Date(timeInSeconds*1000);
	}
}
