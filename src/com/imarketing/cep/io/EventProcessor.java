package com.imarketing.cep.io;

import org.apache.log4j.Logger;

import com.imarketing.cep.engine.CepEngine;
import com.imarketing.cep.util.ObjectEvent;

public class EventProcessor extends AbstractProcessor<Object, ObjectEvent> {

	private Logger logger = Logger.getLogger(EventProcessor.class);
	
	public EventProcessor(CepEngine e) { name = "EventProcessor"; engine = e; }
	
	@Override
	public void processOneData(OneData one) {
		logger.debug("processOneData: " + one.data.getData().toString());
		engine.accept(one.data.getData());
		ok++;
	}
}
