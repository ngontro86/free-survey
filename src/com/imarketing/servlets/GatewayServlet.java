package com.imarketing.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.imarketing.conf.ConfigStore;

public class GatewayServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	private Logger logger = Logger.getLogger(GatewayServlet.class);
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
		        	throws ServletException, IOException {
		doPost(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
						throws ServletException, IOException {
		try {
			
			response.setHeader("Cache-Control", "no-cache");
			response.setHeader("Cache-Control","no-store");
			response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility
			response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
		     
			String uri = request.getRequestURI();
			String forwardingUrl = uri.replaceAll("/" + ConfigStore.getConfig("Application.WarFile.Name") + "/" + ConfigStore.getConfig("Application.URL.Prefix"), "");
			request.getRequestDispatcher(forwardingUrl).forward(request, response);
		} catch (Exception e) {
			logger.error("doPost() got exception: ", e);
		}
	}
}
