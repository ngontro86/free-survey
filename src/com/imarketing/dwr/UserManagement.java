package com.imarketing.dwr;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.directwebremoting.WebContextFactory;

import com.imarketing.common.Common;
import com.imarketing.conf.ConfigStore;
import com.imarketing.conf.xml.SimpleXMLFileParserFactory;
import com.imarketing.db.Group;
import com.imarketing.db.User;
import com.imarketing.db.UserHistory;
import com.imarketing.db.dao.IUserDAO;
import com.imarketing.db.dao.DAOFactory;
import com.imarketing.db.dao.IGroupDAO;
import com.imarketing.db.dao.IUserHistoryDAO;
import com.imarketing.dbwriter.DBWriterListener;
import com.imarketing.email.EmailListener;
import com.imarketing.email.EmailTask;
import com.imarketing.time.GlobalTime;

public class UserManagement extends AbstractManagement {
	
	private Logger logger = Logger.getLogger(UserManagement.class);
	
	public UserManagement() throws Exception {
		super();
		setRequest(WebContextFactory.get().getHttpServletRequest());
	}
	
	public UserManagement(HttpServletRequest req) throws Exception {
		super(req);
	}
	
	public String displayUserAccount() {
		try {
			if(userAuthenticated()) {
				User user = (User)getRequest().getSession().getAttribute(Common.HTTPSESSION_USER_ATTRIBUTION);
				Map<String, Object> fixedValues = new HashMap<String, Object>();
				fixedValues.put("username_emailaddress", user.getUsername_emailaddress());
				fixedValues.put("fullname", user.getFullname());
				fixedValues.put("password", user.getPassword());
				fixedValues.put("sex", user.getSex());
				fixedValues.put("dob", user.getDob());
				fixedValues.put("address", user.getAddress());
				fixedValues.put("companyname", user.getCompanyname());
				fixedValues.put("companyindustry", user.getCompanyindustry());
				return SimpleXMLFileParserFactory.getCustomizedInputTable(Common.USER_REGISTRATION_FULL_INPUT_FILE).toHTMLString(fixedValues);
			}
		} catch (Exception e){
			logger.error("displayUserAccount() got exception: ", e);
			DBWriterListener.writeLogs("displayUserAccount() got exception: " +e.getMessage());
		}
		return "";
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, String> updateUser(Map map) {
		Map<String, String> ret = new HashMap<String, String>();
		try {
			if(userAuthenticated()) {
				User newUser = (User)getRequest().getSession().getAttribute(Common.HTTPSESSION_USER_ATTRIBUTION);
				IUserDAO userDAO = DAOFactory.getDefaultFactory().getUserDAO();
				newUser = userDAO.getUser(newUser.getId());
				fillUserInfo(newUser, map);
				newUser.setLastupdated_timestamp(GlobalTime.currentTimeSeconds());
				userDAO.updateUser(newUser);
				ret.put("success", "true");
				ret.put("message", "Tài khoản của bạn đã được cập nhật thành công.");
				userDAO.close();
				return ret;
			}
		} catch (Exception e){
			logger.error("updateUser() got exception: ", e);
			DBWriterListener.writeLogs("updateUser() got exception: " +e.getMessage());
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, String> validateLogin(String username, String password) throws Exception {
		logger.debug("Start -- ValidateLogin() uName: " + username);
		IUserDAO userDao = DAOFactory.getDefaultFactory().getUserDAO();
		User u = userDao.getUser(username, password);
		userDao.close();
		Map<String, String> ret = new HashMap<String, String>();
		if(u != null){
			if(u.getStatus().equals(CommonDWR.ACTIVATED)) {
				ret.put("success", "true");
				ret.put("message", String.format("Chào mừng bạn đến với %s", ConfigStore.getConfig("Application.Website.Name")));
			
				UserHistory userHistory = new UserHistory();
				userHistory.setLocation_ip(getRequest().getRemoteAddr());
				userHistory.setLogin_timestamp(GlobalTime.currentTimeSeconds());
				userHistory.setNotes("User Login Success");
				userHistory.setLogout_timestamp(GlobalTime.currentTimeSeconds());
					
				getRequest().getSession().setAttribute(Common.HTTPSESSION_USER_ATTRIBUTION, u);
				getRequest().getSession().setAttribute(Common.HTTPSESSION_USERHISTORY, userHistory);
				getRequest().getSession().setMaxInactiveInterval(CommonDWR.MAX_IDLE_DURATION);
			} else {
				ret.put("success", "false");
				ret.put("message", "Tài khoản của bạn chưa được kích hoạt, vui lòng kiểm tra link kích hoạt tại email của bạn");
			}
		} else {
			ret.put("success", "false");
			ret.put("message", String.format("Không tìm thấy địa chỉ email và mật khẩu này."));
			getRequest().getSession().removeAttribute(Common.HTTPSESSION_USER_ATTRIBUTION);
		}
		
		return ret;
	}
	
	@SuppressWarnings("unchecked")
	private boolean fillUserInfo(User newUser, Map map) {
		newUser.setUsername_emailaddress(Common.toString(map.get("username_emailaddress"), ""));
		newUser.setPassword(Common.toString(map.get("password"), ""));
		newUser.setFullname(Common.toString(map.get("fullname"), ""));
		newUser.setAddress(Common.toString(map.get("address"), ""));
		newUser.setPostalcode(Common.toString(map.get("postalcode"), "N.A"));
		newUser.setSex(Common.toString(map.get("sex"), ""));
		newUser.setCompanyname(Common.toString(map.get("companyname"), ""));
		newUser.setCreated_timestamp(GlobalTime.currentTimeSeconds());
		newUser.setCurrent_role(Common.toString(map.get("current_role"), "Other"));
		newUser.setDob(Common.toLong(map.get("dob"), 19270101L));
		newUser.setCompanyindustry(Common.toString(map.get("companyindustry"), ""));
		newUser.setCompanysize(getCompanySize(Common.toString(map.get("companysize"), "0-1000")));
		newUser.setFax_number(Common.toString(map.get("fax_number"), "N.A"));
		newUser.setMobile_contact(Common.toString(map.get("mobile_contact"), "N.A"));
		newUser.setOffice_contact(Common.toString(map.get("office_contact"), "N.A"));
		newUser.setService_plan(Common.toString(map.get("service_plan"), CommonDWR.FREEMIUMGROUP));
		newUser.setRolegroup(getGroup(Common.toString(map.get("service_plan"), CommonDWR.FREEMIUMGROUP)));
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, String> insertUser(Map map) {
		Map<String, String> ret = new HashMap<String, String>();
		try {
			IUserDAO userDao = DAOFactory.getDefaultFactory().getUserDAO();
			User newUser = new User();
			newUser.setStatus(CommonDWR.WAITING_ACTIVATION);
			newUser.setUid(UUID.randomUUID().toString());
			fillUserInfo(newUser, map);
			newUser.setSurvey_polls(null);
			newUser.setHistories(null);
			newUser.setCredit(null); // TODO null for now!
			newUser.setExpired_timestamp(GlobalTime.currentTimeSeconds() + Common.EXPIRATION_TIME); // TODO this is wrong, it depends on the plan
			if(!userDao.insertUser(newUser)) {
				ret.put("success", "false");
				ret.put("message", "Email này đã được sử dụng bởi một thành viên khác");
				userDao.close();
				return ret;
			}
			// Send the email asking for activation
			// Would love to send an email if there is an email in the survey setting
			EmailTask task = new EmailTask();
			task.setSubject("From EventCliqk: Account activation");
			task.setToAddresses(new String[] { newUser.getUsername_emailaddress() });
			task.setBody(String.format(CommonDWR.ACCOUNT_ACTIVATION_EMAILBODY, newUser.getFullname(), newUser.getUid()));
			EmailListener.addEmailTask(task);
			logger.error("userRegistration(), finished scheduling an email!");
			
			ret.put("success", "true");
			ret.put("message", "Cảm ơn bạn đã đăng ký thành viên. Link kích hoạt sẽ gửi đến email của bạn trong giây lát");
			userDao.close();
			return ret;
		} catch (Exception e){
			logger.error("insertUser(): got exception: ", e);
			DBWriterListener.writeLogs("insertUser() got exception: " +e.getMessage());
			ret.put("success", "false");
			ret.put("message", "Báo lỗi");
			return ret;
		}
	}
	
	public boolean activateUser(String uid) {
		try {
			IUserDAO userDao = DAOFactory.getDefaultFactory().getUserDAO();
			User u = userDao.getUserByUID(uid);
			if(u != null && !CommonDWR.ACTIVATED.equals(u.getStatus())) {
				u.setStatus(CommonDWR.ACTIVATED);
				u = userDao.updateUser(u);
				
				UserHistory userHistory = new UserHistory();
				userHistory.setLocation_ip(getRequest().getRemoteAddr());
				userHistory.setLogin_timestamp(GlobalTime.currentTimeSeconds());
				userHistory.setNotes("Account Activated Success");
				userHistory.setLogout_timestamp(GlobalTime.currentTimeSeconds());
					
				getRequest().getSession().setAttribute(Common.HTTPSESSION_USER_ATTRIBUTION, u);
				getRequest().getSession().setAttribute(Common.HTTPSESSION_USERHISTORY, userHistory);
				return true;
			}
		} catch (Exception e){
			logger.error("activateUser() got exception: ", e);
			DBWriterListener.writeLogs("activateUser() got exception: " +e.getMessage());
		}
		return false;
	}
	
	private Group getGroup(String servicePlan) {
		try {
			String value = ConfigStore.getConfig(servicePlan);
			IGroupDAO groupDAO = DAOFactory.getDefaultFactory().getGroupDAO();
			Group ret = groupDAO.getGroup(value); 
			groupDAO.close();
			return ret;
		} catch (Exception e){
			logger.error("getGroup() failed for service plan: ",e);
			DBWriterListener.writeLogs("getGroup() got exception: " +e.getMessage());
			return null;
		}
	}
	
	private int getCompanySize(String size) {
		try {
			if(size.equals(">500")) return 500;
			String[] minAndMax = size.split("-");
			logger.debug("min: " + Arrays.toString(minAndMax));
			int min = Common.toInt(minAndMax[0], 0), max = Common.toInt(minAndMax[1], 1000);
			return (min + max) / 2; 
		} catch (Exception e){
			logger.error(String.format("getCompanySize(%s) got exception: ", size),e);
			DBWriterListener.writeLogs("getCompanySize() got exception: " +e.getMessage());
			return 5;
		}
	}
	
	@SuppressWarnings("unchecked")
	public String logout() {
		try {
			if(userAuthenticated()) {
				UserHistory userHistory = (UserHistory)getRequest().getSession().getAttribute(Common.HTTPSESSION_USERHISTORY);
				userHistory.setLogout_timestamp(GlobalTime.currentTimeSeconds());
				IUserHistoryDAO userHistoryDAO = DAOFactory.getDefaultFactory().getUserHistoryDAO();
				userHistory = userHistoryDAO.insertOrUpdateUserHistory(userHistory);
				
				User user = (User)getRequest().getSession().getAttribute(Common.HTTPSESSION_USER_ATTRIBUTION);
				IUserDAO userDAO = DAOFactory.getDefaultFactory().getUserDAO();
				user = userDAO.getUser(user.getId());
				((Set<UserHistory>)user.getHistories()).add(userHistory);
				userDAO.updateUser(user);
				
				userDAO.close(); userHistoryDAO.close();
				
				getRequest().getSession().removeAttribute(Common.HTTPSESSION_USER_ATTRIBUTION);
				getRequest().getSession().removeAttribute(Common.HTTPSESSION_USERHISTORY);
				getRequest().getSession().invalidate();
				return "Cảm ơn";
			} 
			return "Bạn chưa đăng nhập";
		} catch (Exception e){
			logger.error("logout() got exception: ", e);
			DBWriterListener.writeLogs("logout() got exception: " +e.getMessage());
			return "Cảm ơn";
		}
	}
	
	public boolean resendPassword(String username) {
		try {
			IUserDAO userDAO = DAOFactory.getDefaultFactory().getUserDAO();
			User u = userDAO.getAnyUser(username);
			if(u != null) {
				EmailTask task = new EmailTask();
				task.setSubject(String.format("From %s: Password recovery", ConfigStore.getConfig("Application.Website.Name")));
				task.setToAddresses(new String[] { u.getUsername_emailaddress() });
				task.setBody(String.format(CommonDWR.ACCOUNT_PASSWORD_RECOVERY, u.getFullname(), u.getPassword()));
				EmailListener.addEmailTask(task);
				logger.error("userRegistration(), finished scheduling an email!");
				userDAO.close();
			} else { userDAO.close(); return false; }
		} catch (Exception e){
			logger.error("resendPassword(): got exception: ", e);
			DBWriterListener.writeLogs("resendPassword() got exception: " +e.getMessage());
		}
		return true;
	}

}
