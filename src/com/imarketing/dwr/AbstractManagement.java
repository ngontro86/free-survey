package com.imarketing.dwr;

import javax.servlet.http.HttpServletRequest;

import com.imarketing.common.Common;
import com.imarketing.db.User;

public abstract class AbstractManagement {
	
	private HttpServletRequest request;
	
	public AbstractManagement() throws Exception {}
	
	public AbstractManagement(HttpServletRequest req) throws Exception {
		request = req;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	// TODO will need to check the user authentication session is expired (after 30 mins) in the future
	protected boolean userAuthenticated() {
		if(getRequest() != null && getRequest().getSession() != null 
				&& getRequest().getSession().getAttribute(Common.HTTPSESSION_USER_ATTRIBUTION) instanceof User){ return true; }
		return false;
	}
}
