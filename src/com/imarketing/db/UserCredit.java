package com.imarketing.db;

import java.io.Serializable;

@SuppressWarnings({ "serial", "unchecked" })
public class UserCredit implements Serializable, Comparable{
	private long id;
	private long credit;
	private double conversionrate;
	private long lastupdated_timestamp;
	private String reason;
	
	private UserCredit history; 

	@Override
	public int compareTo(Object o) {
		if(o instanceof UserCredit){
			UserCredit uc = (UserCredit)o;
			if(uc.getId() == getId()) return 0;
			if(getId() > uc.getId()) return 1;
			return -1;
		}
		return 1;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCredit() {
		return credit;
	}

	public void setCredit(long credit) {
		this.credit = credit;
	}

	public double getConversionrate() {
		return conversionrate;
	}

	public void setConversionrate(double conversionrate) {
		this.conversionrate = conversionrate;
	}

	public long getLastupdated_timestamp() {
		return lastupdated_timestamp;
	}

	public void setLastupdated_timestamp(long lastupdated_timestamp) {
		this.lastupdated_timestamp = lastupdated_timestamp;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public UserCredit getHistory() {
		return history;
	}

	public void setHistory(UserCredit history) {
		this.history = history;
	}
}
