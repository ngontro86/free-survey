function errh(msg, exc) {
  alert("Error message is: " + msg + " - Error Details: " + dwr.util.toDescriptiveString(exc, 2));
}
//dwr.engine.setErrorHandler(errh);

var surveyQuestionId = 'surveyquestion';
var surveyconstructionId = 'surveyconstruction';

var updatingUserAccount = false;

function displayAds() {
    autoRefresh('leftads', 'ADS', 5, 60000, true);
    autoRefresh('rightads', 'ADS', 5, 60000, true);
    autoRefresh('topbanner', 'BANNER', 1, 60000, false);
    startUpdateAds();
}

function checkMandatoryField(name, compulsoryFields) {
    var obj = getFormValueObject(name);
    var fieldArr = compulsoryFields.split(',');
    for(i = 0; i < fieldArr.length; i++) {
        var field = fieldArr[i];
        if(field != '' && (obj[field] == null || obj[field].length < 1)) return field;
    }
    return '';
}

function submitbttnonclick(name, compulsoryFields) {
    $('#continuebttn').attr("disabled", "true");
    var ret = checkMandatoryField(name, compulsoryFields);
    if(ret != '') {
         alert("Vui lòng điền đầy đủ các mục bắt buộc. Ví dụ: " + ret); 
         $('#continuebttn').attr("disabled", "false"); return;
    }
    var obj=getFormValueObject(name);
    if(name == "AnotherLogin") {
        $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
        UserManagement.validateLogin(obj.email_address,obj.pswd,postLoginCheck);
        if(obj.rememberme != '0') {
            toMem(null, obj.email_address, obj.pswd);
        } else {
            delMem(null);
        }
    } else if(name == "Đăng ký") {
         var gobacktoSurveyList = function(data) {
            $("#spinningwheel").html(''); $('#continuebttn').attr("disabled", "false");
            updatingUserAccount = false;
            alert(data.msg);            
            displayMenu("Khảo sát đã tạo");
         }
         $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');   
         if(updatingUserAccount) UserManagement.updateUser(obj, gobacktoSurveyList);
         else UserManagement.insertUser(obj, checkUserExist);
    }
    else if(name == "Đăng nhập") {
        $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
        UserManagement.validateLogin(obj.username_emailaddress,obj.password,postLoginCheck);
    }
    else if(name.match("Bước 3")) {
  		processServiceCreateData(obj, name);
  	    hideElementById(surveyQuestionId);
  	    addQuestionIntoSurvey();      
    } else if(name.match("Bước 2")) {
        $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
        processServiceCreateData(obj, name);
        displayQuestionDiv(obj);
        SurveyManagement.getHTMLFormFromMapping(obj.type, createNextStepFunc);
    } else if(name.match("Bước 1")) {
        processServiceCreateData(obj, name);
        createSurveyBasicInputs(obj);
    } else if(name == "Nâng cấp khảo sát") {
        upgradeOnesurvey(name);
    }
}

function postLoginCheck(data) {
    $("#spinningwheel").html(''); $('#continuebttn').attr("disabled", "false");
    if(data != null && data.success == "true"){
        stopUpdateAds();
        window.location="user.jsp";
    } else {
        alert(data.message);
    }
}

function createNextStepFunc(data) {
    $("#spinningwheel").html(''); $('#continuebttn').attr("disabled", "false");
    var newDiv = document.getElementById('svq_answeroption'); 
    newDiv.innerHTML = data;
    newDiv.style.display='true';
}

function sendpassword() {
    if(confirm("Bạn đã điền đúng địa chỉ email?")) {
        var username = $('#username').val();
        var respBack = function(data){ 
            $("#spinningwheel").html('');
            if(data) { alert('Mật khẩu mới đã được gửi đến địa chỉ email của bạn. Vui lòng kiểm tra.'); } 
            else { alert("Không tìm thấy địa chỉ email này, vui lòng kiểm tra lại."); }
        }
        $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
        UserManagement.resendPassword(username, respBack);
    }
}

function displayprivacypolicy() {
      $.get('docs/privacy_policy.txt', function(data){
            new $.msgbox({
                onClose: function(){},
                title: 'Chính sách bảo mật',
                width: 800,
                height: 500,
                type: 'alert',
                content: data
            }).show();}
            );  
}

function displaytermandcondition() {
    $.get('docs/terms_condition.txt', function(data){
            new $.msgbox({
	            onClose: function(){},
	            title: 'Điều khoản sử dụng',
	            width: 800,
	            height: 500,
	            type: 'alert',
	            content: data
	        }).show();}
	        );
}

function displaycontactus() {
    $.get('docs/contact_us.txt', function(data){
            new $.msgbox({
                onClose: function(){},
                title: 'Liên hệ',
                width: 500,
                height: 400,
                type: 'alert',
                content: data
            }).show();}
            );
}

function hideElementById(name) {
   var divObj = document.getElementById(name);
   divObj.style.display="none";
}

function disableEnterKey() {
    alert("disableEnterKey!" + !(window.event && window.event.keyCode == 13));
    return !(window.event && window.event.keyCode == 13);
} 

function getFormValueObject(formid){
        var dl = document.getElementById(formid);
        var _f1 = new Object();
        _f1=_getObj(dl.getElementsByTagName("textarea"),_f1); 
        _f1=_getObj(dl.getElementsByTagName("select"),_f1); 
        _f1=_getObj(dl.getElementsByTagName("input"),_f1); 
        return _f1;
}

function _getObj(lis, fields) {
        for(mtlgloop=0;mtlgloop<lis.length;mtlgloop++){
            if (lis[mtlgloop].type=='radio' || lis[mtlgloop].type=='checkbox') {  
                 if(lis[mtlgloop].checked) {
                    fields[lis[mtlgloop].name]=lis[mtlgloop].value;
                 } else{
                    if(lis[mtlgloop].type=='checkbox') fields[lis[mtlgloop].name]="0";
                    if(lis[mtlgloop].type=='radio' && (fields[lis[mtlgloop].name] == null || fields[lis[mtlgloop].name].length < 1)) fields[lis[mtlgloop].name]=""; 
                 }
            } else if(lis[mtlgloop].type=='select-multiple') {
               var rr=new Array();
               for(ib=0;ib<lis[mtlgloop].options.length;ib++){
                    if(lis[mtlgloop].options[ib].selected) { rr[rr.length]=lis[mtlgloop].options[ib].value; }
               }
               var vvval="";
               for(iab=0;iab<rr.length;iab++){
                    vvval+=(iab>0)?","+rr[iab]:rr[iab];
               }
               fields[lis[mtlgloop].name]=vvval;              
            } else {   
                fields[lis[mtlgloop].name]=lis[mtlgloop].value;
            }
        } // end of for loop
        return fields;
}

