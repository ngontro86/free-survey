package com.imarketing.db.dao.mysql;

import java.util.List;

import org.hibernate.Query;

import com.imarketing.db.Config;
import com.imarketing.db.dao.IConfigDAO;

public class ConfigMySQLDAO extends CommonMySQLDAO implements IConfigDAO {

	@Override
	public Config getConfig(long id) throws Exception {
		beginTransaction();
		return (Config)session.get(Config.class, new Long(id));
	}

	@SuppressWarnings("unchecked")
	@Override
	public Config getConfig(String param) throws Exception {
		beginTransaction();
		Query q = session.createQuery("from Config where param= :param");
		q.setString("param", param);
		List l = q.list();
		return (l != null && !l.isEmpty())? (Config)l.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Config> getAllConfigs() throws Exception {
		beginTransaction();
		Query q = session.createQuery("from Config");
		return (List<Config>)q.list();
	}
	
}
