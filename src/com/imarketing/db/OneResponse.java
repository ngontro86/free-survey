package com.imarketing.db;

import java.io.Serializable;

import javax.persistence.Id;

@SuppressWarnings({ "serial", "unchecked" })
public class OneResponse implements Serializable, Comparable {
	@Id 
	private long id;
	
	private String action;
	
	private long timestamp;
	
	private Participant participant;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
	public Participant getParticipant() {
		return participant;
	}
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	@Override
	public int compareTo(Object o) {
		if(o == null) return 1;
		if(o instanceof OneResponse){
			OneResponse r = (OneResponse)o;
			if(r.getId() == getId()) return 0;
			if(getId() > r.getId()) return 1;
			return -1;
		}
		return -1;
	}
	
}
