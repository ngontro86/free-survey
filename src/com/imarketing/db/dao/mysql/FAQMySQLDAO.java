package com.imarketing.db.dao.mysql;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.imarketing.db.FAQ;
import com.imarketing.db.FAQCategory;
import com.imarketing.db.dao.DAOFactory;
import com.imarketing.db.dao.IFAQDAO;

public class FAQMySQLDAO extends CommonMySQLDAO implements IFAQDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<FAQ> getAllFAQs(long categoryID) throws Exception {
		beginTransaction();
		FAQCategory faqCategory = DAOFactory.getDefaultFactory().getFAQCategoryDAO().getFAQCategory(categoryID);
		Set  set = faqCategory.getFaqs();
		List<FAQ> ret = new ArrayList(set);
		return ret;
	}

	@Override
	public FAQ getFAQ(long id) throws Exception {
		beginTransaction();
		return (FAQ)session.get(FAQ.class, new Long(id));
	}
}
