package com.imarketing.db;

import java.io.Serializable;

@SuppressWarnings("serial")
public class PendingUserUpgrade implements Serializable{
	private long id;
	private String status; // Either: UNDER PROCESSING or PROCESSED
	private User user;
	private String refnum;
	private long timestamp;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public String getRefnum() {
		return refnum;
	}
	public void setRefnum(String refnum) {
		this.refnum = refnum;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
