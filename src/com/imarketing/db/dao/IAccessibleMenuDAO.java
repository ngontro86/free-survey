package com.imarketing.db.dao;

import com.imarketing.db.AccessibleMenu;

public interface IAccessibleMenuDAO extends ICommonDAO{
	
	public boolean insertAccessibleMenu(AccessibleMenu am) throws Exception;
	
	public AccessibleMenu getAccessibleMenu(long id) throws Exception;
	
	
}
