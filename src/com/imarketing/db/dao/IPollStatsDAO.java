package com.imarketing.db.dao;

import com.imarketing.db.PollStats;

public interface IPollStatsDAO extends ICommonDAO {
	
	public PollStats getPollStatsById(long id) throws Exception;

}
