package com.imarketing.cep.util;

public class SocketData<T> {
	private String connectionId;
	private T data;
	
	public SocketData(String id, T d) {connectionId = id; data = d;}
	
	public String getConnectionId() {
		return connectionId;
	}
	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
}
