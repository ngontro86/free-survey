package com.imarketing.db.dao;

import java.util.List;

import com.imarketing.db.Ads;

public interface IAdsDAO extends ICommonDAO {
	
	public Ads getAdsById(long id) throws Exception;
	
	public Ads getAdsByProvider(String provider) throws Exception;
	
	public List<Ads> getAllAds() throws Exception;

}
