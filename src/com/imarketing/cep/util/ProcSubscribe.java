package com.imarketing.cep.util;

import java.io.Serializable;

public class ProcSubscribe implements Serializable {
	private static final long serialVersionUID = 1L;
	public String query;
	public String eventID;
	
	public ProcSubscribe(String query, String eventID) {
		this.query = query;
		this.eventID = eventID;
	}
}
