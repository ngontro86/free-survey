package com.imarketing.cep.engine;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.espertech.esper.client.Configuration;
import com.espertech.esper.client.EPAdministrator;
import com.espertech.esper.client.EPOnDemandQueryResult;
import com.espertech.esper.client.EPRuntime;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.deploy.DeploymentException;
import com.espertech.esper.client.deploy.DeploymentOptions;
import com.espertech.esper.client.deploy.DeploymentOrder;
import com.espertech.esper.client.deploy.DeploymentOrderOptions;
import com.espertech.esper.client.deploy.EPDeploymentAdmin;
import com.espertech.esper.client.deploy.Module;
import com.espertech.esper.client.deploy.ParseException;
import com.espertech.esper.epl.join.rep.Node;
import com.espertech.esper.event.bean.BeanEventBean;
import com.imarketing.cep.util.NamedMap;
import com.imarketing.common.Common;
import com.imarketing.conf.ConfigStore;

public class CepEngine {
	
	private Logger logger = Logger.getLogger(CepEngine.class);
		
	private EPRuntime epRuntime;
	private EPAdministrator epAdmin;
	private EPServiceProvider epServiceProvider;
	
	private int ok = 0, error = 0;
	
	public CepEngine() {
		Configuration config = new Configuration();
		this.epServiceProvider = EPServiceProviderManager.getProvider(String.format("CepEngine_%d",System.currentTimeMillis()), config);
		this.epRuntime = epServiceProvider.getEPRuntime();
		this.epAdmin = epServiceProvider.getEPAdministrator();
		init();
	}
	
	@SuppressWarnings("unchecked")
	public boolean accept(Object event) {
		if(event instanceof NamedMap) {
			this.epRuntime.sendEvent((NamedMap)event, ((NamedMap)event).name);
		} else if(event instanceof Node) {
			this.epRuntime.sendEvent((Node)event);
		} else {
			this.epRuntime.sendEvent(event);
		}
		return true;
	}
	
	public boolean init() {
		try {
			EPDeploymentAdmin deployAdmin = epAdmin.getDeploymentAdmin();
			String modulePath = ConfigStore.getConfig(ConfigStore.BASEPATH) + File.separator + Common.ESPER_FILE_PATH;
			File moduleFolder = new File(modulePath);
			if(!moduleFolder.exists()) {logger.error("module folder doesnt exist, fail initializing the esper engine"); return false;}
			List<Module> modules = new ArrayList<Module>();
			for(File f : moduleFolder.listFiles()){
				if(!f.getName().endsWith(".esper")) continue;
				modules.add(deployAdmin.read(f));
			}
			try {
				DeploymentOrder order = deployAdmin.getDeploymentOrder(modules, new DeploymentOrderOptions());
				for(Module orderedModule : order.getOrdered()){
					deployAdmin.deploy(orderedModule, new DeploymentOptions());
				}
				return true;
			} catch (DeploymentException e) {
				logger.error("init() failed. DeploymentException: ", e);
				return false;
			}
		} catch (IOException e) {
			logger.error("init() failed.IOException: ", e);
			return false;
		} catch (ParseException e) {
			logger.error("init() failed. ParsedException: ", e);
			return false;
		}
	}
	
	public List<Map<String, Object>> snapShotQuery(String query) {
		List<Map<String, Object>> ret = new ArrayList<Map<String,Object>>();
		EPOnDemandQueryResult result = epRuntime.executeQuery(query);
		for(EventBean row : result.getArray()){
			ret.add(castEventBeanToMap(row));
		}
		return ret;
	}
	
	public boolean registerSubscriber(String query, Runnable subsriber){
		try {
			logger.debug("registerSubscriber: " + query);
			EPStatement stt = epAdmin.createEPL(query);
			stt.setSubscriber(subsriber);
			new Thread(subsriber).start();
			return true;
		} catch (Exception e){
			logger.error("registerSubscriber failed, exception: ",e);
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> castEventBeanToMap(EventBean eb){
		if(eb instanceof BeanEventBean) {
			Map<String, Object> map = new HashMap<String, Object>();
			for(String key : eb.getEventType().getPropertyNames()) map.put(key, eb.get(key));
			return map;
		} else {
			return (Map<String, Object>)eb.getUnderlying();
		}
	}

	public String status() {
		return String.format("EsperEngine status: ok =%d, error =%d", ok, error);
	}
	
}
