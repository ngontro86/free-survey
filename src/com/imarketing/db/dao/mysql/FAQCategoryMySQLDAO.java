package com.imarketing.db.dao.mysql;


import java.util.List;

import org.hibernate.Query;

import com.imarketing.db.FAQCategory;
import com.imarketing.db.dao.IFAQCategoryDAO;

public class FAQCategoryMySQLDAO extends CommonMySQLDAO implements IFAQCategoryDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<FAQCategory> getAllFAQCategories() throws Exception {
		beginTransaction();
		Query q = session.createQuery("from FAQCategory");
		return q.list();
	}

	@Override
	public FAQCategory getFAQCategory(long id) throws Exception {
		beginTransaction();
		FAQCategory ret = (FAQCategory)session.get(FAQCategory.class, new Long(id));
		return ret;
	}

}
