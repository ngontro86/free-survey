package com.imarketing.db.dao;

import java.util.HashMap;
import java.util.Map;

import com.imarketing.db.dao.mysql.MySQLDAOFactory;

public abstract class DAOFactory {
	private final static String defaultType = "MySQL";
	private static Map<String, DAOFactory> factories = new HashMap<String, DAOFactory>();
	
	public static DAOFactory getDefaultFactory(){
		if(!factories.containsKey(defaultType)) factories.put(defaultType, new MySQLDAOFactory());
		return factories.get(defaultType);
	}
	
	public static DAOFactory getDaoFactory(String type){
		if(type == null || type.equalsIgnoreCase(defaultType)) return getDefaultFactory();
		return null;
	}
	
	public abstract IUserDAO getUserDAO();
	
	public abstract IUserCreditDAO getUserCreditDAO();
	
	public abstract IUserHistoryDAO getUserHistoryDAO();
	
	public abstract ISurveyDAO getSurveyDAO();
	
	public abstract IPollDAO getPollDAO();
	
	public abstract ISurveyStatsDAO getSurveyStatsDAO();
	
	public abstract IPollStatsDAO getPollStatsDAO();
	
	public abstract ISurveyPageDAO getSurveyPageDAO();
	
	public abstract IOneResponseDAO getResponseDAO();
	
	public abstract IQuestionDAO getQuestionDAO();
	
	public abstract IQuestionStatsDAO getQuestionStatsDAO();
	
	public abstract IGroupDAO getGroupDAO();
	
	public abstract IFunctionDAO getFunctionDAO();
	
	public abstract IAnswerOptionDAO getAnswerOptionDAO();
	
	public abstract IAnswerOptionStatsDAO getAnswerOptionStatsDAO();
	
	public abstract IParticipantDAO getParticipantDAO();
	
	public abstract IAccessibleMenuDAO getAccessibleMenuDAO();
	
	public abstract IFAQDAO getFAQDAO();
	
	public abstract IFAQCategoryDAO getFAQCategoryDAO();
	
	public abstract IConfigDAO getConfigDAO();
	
	public abstract IServiceTypeDefDAO getServiceTypeDefDAO();
	
	public abstract IAdsDAO getAdsDAO();
	
	public abstract ILogsDAO getLogsDAO();
	
	public abstract IPendingUserUpgradeDAO getPendingUserUpgradeDAO();
	
	public abstract IPendingSurveyUpgradeDAO getPendingSurveyUpgradeDAO();
	
}
