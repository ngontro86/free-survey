package com.imarketing.db.dao.mysql;

import com.imarketing.db.OneResponse;
import com.imarketing.db.dao.IOneResponseDAO;

public class OneResponseMySQLDAO extends CommonMySQLDAO implements IOneResponseDAO {

	@Override
	public boolean insertOrUpdateSurveyResponse(OneResponse response) {
		beginTransaction();
		session.saveOrUpdate(response);
		flush();
		return true;
	}

}
