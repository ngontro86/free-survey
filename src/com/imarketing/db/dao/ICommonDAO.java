package com.imarketing.db.dao;

import org.hibernate.Session;

public interface ICommonDAO {
	
	public Session getSession();
	
	public void close();
	
	public void beginTransaction();
	
	public void rollBackTransaction();
	
	public void flush();
	
}
