package com.imarketing.db;

public class Config implements Comparable<Config>{
	private long id;
	private String param;
	private String value;
	private String type;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getParam() {
		return param;
	}
	public void setParam(String param) {
		this.param = param;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public int compareTo(Config o) {
		if(o == null) return 1;
		if(getId() > o.getId()) return 1;
		if(getId() < o.getId()) return -1;
		return 0;
	}
}
