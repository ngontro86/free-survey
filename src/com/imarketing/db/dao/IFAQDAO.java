package com.imarketing.db.dao;

import java.util.List;

import com.imarketing.db.FAQ;

public interface IFAQDAO extends ICommonDAO {

	public List<FAQ> getAllFAQs(long categoryID) throws Exception;
	
	public FAQ getFAQ(long id) throws Exception;
	
}
