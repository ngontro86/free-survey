package com.imarketing.db.dao;

import java.util.Map;

import com.imarketing.db.Group;
import com.imarketing.db.User;

public interface IUserDAO extends ICommonDAO {
	
	public boolean insertUser(Map<String, Object> map) throws Exception;
	
	public User updateUser(User u) throws Exception;
	
	public boolean insertUser(User u) throws Exception;
	
	public User getUser(String username, String password) throws Exception;
	
	public User getAnyUser(String username) throws Exception;
	
	public User getUser(long id) throws Exception;
	
	public boolean userExist(String username) throws Exception;
	
	public boolean updateUserGroup(User u, Group newGroup) throws Exception;
	
	public User getUserByUID(String uid) throws Exception;
	
}
