package com.imarketing.db.dao;

import com.imarketing.db.Question;

public interface IQuestionDAO extends ICommonDAO {
	
	public Question insertQuestion(Question q) throws Exception;
	
	public Question getQuestionByUid(String uid) throws Exception;
	
	public Question getQuestionById(long questionId) throws Exception;
	
	public boolean updateQuestion(Question e) throws Exception;

	public boolean deleteQuestion(Question e) throws Exception;
}
