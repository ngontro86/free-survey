package com.imarketing.db.dao;

import java.util.List;

import com.imarketing.db.Config;

public interface IConfigDAO extends ICommonDAO {
	
	public Config getConfig(long id) throws Exception;
	public Config getConfig(String param) throws Exception;
	
	public List<Config> getAllConfigs() throws Exception;

}
