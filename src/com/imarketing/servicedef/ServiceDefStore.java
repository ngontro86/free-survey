package com.imarketing.servicedef;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.imarketing.db.ServiceTypeDef;
import com.imarketing.db.dao.DAOFactory;
import com.imarketing.db.dao.IServiceTypeDefDAO;
import com.imarketing.dbwriter.DBWriterListener;

public class ServiceDefStore implements ServletContextListener {
	private static final long serialVersionUID = -808826552433043958L;
	private static Logger logger = Logger.getLogger(ServiceDefStore.class);
	private static Map<String, ServiceTypeDef> cache = new HashMap<String, ServiceTypeDef>();
	
	public static ServiceTypeDef getServiceTypeDef(String serviceName) {
		return cache.get(serviceName);
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		logger.error("contextInitialized() got called, start initializing the ServiceDefStore now");
		try {
			IServiceTypeDefDAO serviceTypeDefDAO = DAOFactory.getDefaultFactory().getServiceTypeDefDAO();
			List<ServiceTypeDef> l = serviceTypeDefDAO.loadServiceTypeDef();
			for(ServiceTypeDef one : l) cache.put(one.getName(), one);
			serviceTypeDefDAO.close();
		} catch (Exception e){
			logger.error("contextInitialized() got exception: ", e);
			DBWriterListener.writeLogs("ServiceDefStore:contextInitialized() got exception: " +e.getMessage());
		}
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) { logger.error("contextDestroyed() got called"); cache.clear(); }
}