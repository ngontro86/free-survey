package com.imarketing.db.dao.mysql;

import com.imarketing.db.AnswerOption;
import com.imarketing.db.dao.IAnswerOptionDAO;

public class AnswerOptionMySQLDAO extends CommonMySQLDAO implements IAnswerOptionDAO {

	@Override
	public boolean insertOrUpdateAnswerOption(AnswerOption ao) throws Exception {
		beginTransaction();
		session.saveOrUpdate(ao);
		flush();
		return true;
	}
	
	@Override
	public AnswerOption getAnswerOptionById(long answerId)
			throws Exception {
		beginTransaction();
		AnswerOption ao = (AnswerOption)session.get(AnswerOption.class, new Long(answerId));
		return ao;
	}
}
