package com.imarketing.dwr;

import java.util.List;

import org.apache.log4j.Logger;
import org.directwebremoting.WebContextFactory;

import com.imarketing.common.Common;
import com.imarketing.db.PendingSurveyUpgrade;
import com.imarketing.db.dao.DAOFactory;
import com.imarketing.db.dao.IPendingSurveyUpgradeDAO;
import com.imarketing.dbwriter.DBWriterListener;
import com.imarketing.time.GlobalDateTimeFactory;

public class PendingSurveyUpgradeManagement extends AbstractManagement {
	private Logger logger = Logger.getLogger(PendingSurveyUpgradeManagement.class);
	public PendingSurveyUpgradeManagement() throws Exception { super(); setRequest(WebContextFactory.get().getHttpServletRequest()); }
	
	public String getAllPendingSurveyUpgrade(int limit) {
		try {
			if(userAuthenticated()) {
				IPendingSurveyUpgradeDAO pendingSurveyUpgradeDAO = DAOFactory.getDefaultFactory().getPendingSurveyUpgradeDAO();
				List<PendingSurveyUpgrade> list = pendingSurveyUpgradeDAO.getAllPendingRequest(limit, 0);
				String tmpRet = "", ret = "";
				tmpRet += String.format("<table class='allpendingsurveyupgraderequest'>" +
						"<tr>" + 
						"<th width='175'><b>Status</b></th>" + 
						"<th><b>Survey title</b></th>" +
						"<th width='100'><b>Ngày tạo</b></th>" +
						"<th width='175'><b>Ref num</b></th>" +
						"<th width='125'><b>Công cụ</b></th>" +  
				"</tr>");
				int minLen = tmpRet.length();
				for (PendingSurveyUpgrade psu : list) {
					String createdTime = GlobalDateTimeFactory.getInstance().format(psu.getTimestamp(),"yyyy-MMM-dd");
					tmpRet += String.format("<tr>" +
							"<td class='newtable'><img alt='' src='img/preset/play.gif'>%s</td>" +
							"<td class='newtable'><a class='surveytitlelink' href='%s'>%s</a>" +
							"<td class='newtable'>%s</td>" +
							"<td class='newtable'>%s</td>" +
							"<td class='newtable'>" +
							"<input type='image' border='0' src='img/preset/buttons/upgradeconfirm.png' onclick='confirmsurveyupgrade(\"%d\")'/>" +
							"<input type='image' border='0' src='img/preset/buttons/upgradeinvalidate.png' onclick='invalidatesurveyupgrade(\"%d\")'/>" +
							"</td>" +
							"</tr>", psu.getStatus(), psu.getSurvey().getTypicalURL(), psu.getSurvey().getTitle(), 
							createdTime, psu.getRefnum(), psu.getId(), psu.getId());
				}
				tmpRet += String.format("</table>");
				if(tmpRet.length() > minLen + 10) ret = tmpRet;
				pendingSurveyUpgradeDAO.close();
				return ret;
			}
		} catch (Exception e) {
			logger.error("getAllPendingSurveyUpgradeRequest() got exception: ",e);
			DBWriterListener.writeLogs("PendingSurveyUpgradeManagement: getAllPendingSurveyUpgradeRequest() got exception: " +e.getMessage());
		}
		return "";
	}

	public boolean confirm(long id) {
		try {
			IPendingSurveyUpgradeDAO pendingSurveyUpgradeDAO = DAOFactory.getDefaultFactory().getPendingSurveyUpgradeDAO();
			PendingSurveyUpgrade psu = pendingSurveyUpgradeDAO.getPendingSurveyUpgradeById(id);
			
			psu.getSurvey().setType(Common.PREMIUM_INTERNET_SURVEY);
			pendingSurveyUpgradeDAO.updateSurvey(psu);
			psu.setStatus(Common.UPGRADE_STATUS_PROCESSED);
			pendingSurveyUpgradeDAO.updatePendingSurveyUpgrade(psu);
			pendingSurveyUpgradeDAO.close();
			
			return true;
		} catch (Exception e){
			logger.error("confirm() got exception: ",e);
			DBWriterListener.writeLogs("PendingSurveyUpgradeManagement: confirm() got exception: " +e.getMessage());
			return false;
		}
	}
	
	public boolean invalidate(long id) {
		try {
			IPendingSurveyUpgradeDAO pendingSurveyUpgradeDAO = DAOFactory.getDefaultFactory().getPendingSurveyUpgradeDAO();
			PendingSurveyUpgrade psu = pendingSurveyUpgradeDAO.getPendingSurveyUpgradeById(id);
			psu.setStatus(Common.UPGRADE_STATUS_INVALIDATE);
			pendingSurveyUpgradeDAO.updatePendingSurveyUpgrade(psu);
			pendingSurveyUpgradeDAO.close();
			return true;
		} catch (Exception e){
			logger.error("confirm() got exception: ",e);
			DBWriterListener.writeLogs("PendingSurveyUpgradeManagement: invalidate() got exception: " +e.getMessage());
			return false;
		}
	}
}
