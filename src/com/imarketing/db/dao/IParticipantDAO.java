package com.imarketing.db.dao;

import com.imarketing.db.Participant;

public interface IParticipantDAO extends ICommonDAO {
	
	public Participant getParticipantById(long id) throws Exception;
	
	public Participant insertParticipant(Participant p) throws Exception;
	
	public Participant insertOrUpdateParticipant(Participant p) throws Exception;
	
}
