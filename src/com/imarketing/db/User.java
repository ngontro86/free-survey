package com.imarketing.db;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@SuppressWarnings({ "serial", "unchecked" })
@Entity
public class User implements Serializable, Comparable{
	
	@Id
	private long id;
	
	private String status;
	private String uid;
	private String username_emailaddress;
	private String password;
	
	private String fullname;
	private String sex;
	private long dob;
	
	private String address;
	private String postalcode;
	private String mobile_contact;
	private String fax_number;
	private String companyname;
	private String companyindustry;
	private long companysize;
	private String current_role;
	private String office_contact;
	
	private String service_plan;
	
	@OneToOne
	private Group rolegroup;
	
	@OneToMany
	private Set survey_polls = new HashSet();;
	@OneToOne
	private UserCredit credit;
	@OneToMany
	private Set histories = new HashSet();
	
	private long lastlogin_timestamp;
	private long lastupdated_timestamp;
	private long expired_timestamp;
	private long created_timestamp; 

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getUsername_emailaddress() {
		return username_emailaddress;
	}

	public void setUsername_emailaddress(String username_emailaddress) {
		this.username_emailaddress = username_emailaddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getMobile_contact() {
		return mobile_contact;
	}

	public void setMobile_contact(String mobile_contact) {
		this.mobile_contact = mobile_contact;
	}

	public String getFax_number() {
		return fax_number;
	}

	public void setFax_number(String fax_number) {
		this.fax_number = fax_number;
	}

	public String getOffice_contact() {
		return office_contact;
	}

	public void setOffice_contact(String office_contact) {
		this.office_contact = office_contact;
	}
	
	public Group getRolegroup() {
		return rolegroup;
	}

	public void setRolegroup(Group rolegroup) {
		this.rolegroup = rolegroup;
	}

	public Set getSurvey_polls() {
		return survey_polls;
	}

	public void setSurvey_polls(Set survey_polls) {
		this.survey_polls = survey_polls;
	}

	public UserCredit getCredit() {
		return credit;
	}

	public void setCredit(UserCredit credit) {
		this.credit = credit;
	}

	public Set getHistories() {
		return histories;
	}

	public void setHistories(Set histories) {
		this.histories = histories;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPostalcode() {
		return postalcode;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public String getCompanyname() {
		return companyname;
	}

	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}

	public String getCompanyindustry() {
		return companyindustry;
	}

	public void setCompanyindustry(String companyindustry) {
		this.companyindustry = companyindustry;
	}
	
	public long getDob() {
		return dob;
	}

	public void setDob(long dob) {
		this.dob = dob;
	}

	public long getLastlogin_timestamp() {
		return lastlogin_timestamp;
	}

	public void setLastlogin_timestamp(long lastlogin_timestamp) {
		this.lastlogin_timestamp = lastlogin_timestamp;
	}

	public long getLastupdated_timestamp() {
		return lastupdated_timestamp;
	}

	public void setLastupdated_timestamp(long lastupdated_timestamp) {
		this.lastupdated_timestamp = lastupdated_timestamp;
	}

	public long getExpired_timestamp() {
		return expired_timestamp;
	}

	public void setExpired_timestamp(long expired_timestamp) {
		this.expired_timestamp = expired_timestamp;
	}

	public long getCreated_timestamp() {
		return created_timestamp;
	}

	public void setCreated_timestamp(long created_timestamp) {
		this.created_timestamp = created_timestamp;
	}

	public long getCompanysize() {
		return companysize;
	}

	public void setCompanysize(long companysize) {
		this.companysize = companysize;
	}

	public String getCurrent_role() {
		return current_role;
	}

	public String getService_plan() {
		return service_plan;
	}

	public void setService_plan(String service_plan) {
		this.service_plan = service_plan;
	}

	public void setCurrent_role(String current_role) {
		this.current_role = current_role;
	}

	@Override
	public int compareTo(Object o) {
		if(o instanceof User){
			User u = (User)o;
			if(u.getId() == getId()) return 0;
			if(u.getUsername_emailaddress().equals(getUsername_emailaddress())) return 0;
			return getUsername_emailaddress().compareTo(u.getUsername_emailaddress());
		}
		return 1;
	}
}
