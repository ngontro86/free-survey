package com.imarketing.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import com.imarketing.ads.AdsListenerFactory;

import com.imarketing.common.Common;
import com.imarketing.servicedef.ServiceDefStore;

@SuppressWarnings({ "unchecked", "serial" })
public class Survey implements Serializable, Comparable {
	private long id;
	
	private String uid;
	
	private String displayresultstatus;
	private String displaysurveystatus;
	private String type;
	
	// Design part
	private String design;
	private String language;
	private boolean questionnumbering;
	
	private String title;
	private String titledesign;
	
	private String introduction;
	private String introductiondesign;
	
	private String thankyou;
	private String thankyoudesign;
	
	private String logoimg;
	private boolean emailnotification;
	private String email;
	
	private boolean passwordprotected;
	private String password;
	
	private boolean publicresult;
	private boolean prevent_duplicateresponse;
	
	private long responsecount;
	
	@SuppressWarnings("unchecked")
	private Set pages;
	
	private User user;
	
	private long created_timestamp;
	private long lastupdated_timestamp;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getDisplayresultstatus() {
		return displayresultstatus;
	}
	public void setDisplayresultstatus(String displayresultstatus) {
		this.displayresultstatus = displayresultstatus;
	}
	public String getDisplaysurveystatus() {
		return displaysurveystatus;
	}
	public void setDisplaysurveystatus(String displaysurveystatus) {
		this.displaysurveystatus = displaysurveystatus;
	}
	@SuppressWarnings("unchecked")
	public Set getPages() {
		return pages;
	}
	@SuppressWarnings("unchecked")
	public void setPages(Set pages) {
		this.pages = pages;
	}
	public long getResponsecount() {
		return responsecount;
	}
	public void setResponsecount(long responsecount) {
		this.responsecount = responsecount;
	}
	public long getCreated_timestamp() {
		return created_timestamp;
	}
	public void setCreated_timestamp(long created_timestamp) {
		this.created_timestamp = created_timestamp;
	}
	public long getLastupdated_timestamp() {
		return lastupdated_timestamp;
	}
	public void setLastupdated_timestamp(long lastupdated_timestamp) {
		this.lastupdated_timestamp = lastupdated_timestamp;
	}
	public String getDesign() {
		return design;
	}
	public void setDesign(String design) {
		this.design = design;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public boolean isQuestionnumbering() {
		return questionnumbering;
	}
	public void setQuestionnumbering(boolean questionnumbering) {
		this.questionnumbering = questionnumbering;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitledesign() {
		return titledesign;
	}
	public void setTitledesign(String titledesign) {
		this.titledesign = titledesign;
	}
	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	public String getIntroductiondesign() {
		return introductiondesign;
	}
	public void setIntroductiondesign(String introductiondesign) {
		this.introductiondesign = introductiondesign;
	}
	public String getThankyou() {
		return thankyou;
	}
	public void setThankyou(String thankyou) {
		this.thankyou = thankyou;
	}
	public String getThankyoudesign() {
		return thankyoudesign;
	}
	public void setThankyoudesign(String thankyoudesign) {
		this.thankyoudesign = thankyoudesign;
	}
	public String getLogoimg() {
		return logoimg;
	}
	public void setLogoimg(String logoimg) {
		this.logoimg = logoimg;
	}
	public boolean isEmailnotification() {
		return emailnotification;
	}
	public void setEmailnotification(boolean emailnotification) {
		this.emailnotification = emailnotification;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isPasswordprotected() {
		return passwordprotected;
	}
	public void setPasswordprotected(boolean passwordprotected) {
		this.passwordprotected = passwordprotected;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public boolean isPrevent_duplicateresponse() {
		return prevent_duplicateresponse;
	}
	public void setPrevent_duplicateresponse(boolean prevent_duplicateresponse) {
		this.prevent_duplicateresponse = prevent_duplicateresponse;
	}
	
	public boolean isPublicresult() {
		return publicresult;
	}
	public void setPublicresult(boolean publicresult) {
		this.publicresult = publicresult;
	}
	@Override
	public int compareTo(Object o) {
		if(o instanceof Survey){
			Survey s = (Survey)o;
			if(s.getId() == getId()) return 0;
			if(getId() > s.getId()) return 1;
			return -1;
		}
		return 1;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Survey other = (Survey) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	public String getTypicalURL(){ return String.format("publisher.jsp?uid=%s",getUid()); }
	
	public String toHTMLString(boolean editing) { return toHTMLString(editing, "step1"); }
	
	@SuppressWarnings("unchecked")
	public String toHTMLString(boolean editing, String step) {
		String ret = "";
		ret += editing? getStepSettingHTMLString(step) : "";
		String s = "";
		s += String.format("<table style='margin-bottom:15px;' border=0 width=100%%>");
		s += String.format("<tr><td>");
		s += String.format("<div id='mytitle'>");
		s += String.format("<p class='surveytitle'> %s <input type='image' border='0' src='img/preset/buttons/edit_title.gif' onclick='edittitle(\"%s\")' /></p>",getTitle(), getUid());
		s += String.format("<p class='surveyintroduction'><p>%s</p></p>", getIntroduction());
		s += String.format("</tr></table>");
		
		ret += editing ? s:getHeaderPublisher();
		List<SurveyPage> listPages = new ArrayList<SurveyPage>(getPages());
		Collections.sort(listPages, new Comparator<SurveyPage>() {
			@Override
			public int compare(SurveyPage o1, SurveyPage o2) {
				if(o1.getIdx() == o2.getIdx()) return 0;
				if(o1.getIdx() > o2.getIdx()) return 1;
				return -1;
			}
		});
		
		String compulsoryQuestionDivIds = "", allQuestionDivIds = "";
		for(SurveyPage sp : listPages) {
			List<Question> listQuestion = new ArrayList<Question>(sp.getQuestions());
			Collections.sort(listQuestion, new Comparator<Question>(){
				@Override
				public int compare(Question o1, Question o2) {
					if(o1.getIdx() == o2.getIdx()) return 0;
					if(o1.getIdx() > o2.getIdx()) return 1;
					return -1;
				}});
			for(Question q : listQuestion) {
				allQuestionDivIds += String.format("%s,", q.getUid());
				if(q.isAnswer_compulsory()) compulsoryQuestionDivIds += String.format("%s,", q.getUid()); // as defined in this case
			}
		}
		for(SurveyPage sp : listPages) ret += sp.toHTMLString(getUid(), editing); 
		
		ret+= appendButtonHTMLString(editing, compulsoryQuestionDivIds, allQuestionDivIds);
		
		return ret; 
	}
	
	
	// if editing mode, add the two buttons ( Add Question + Add Survey)
	//otherwise, add a Submit button
	private String appendButtonHTMLString(boolean editing, String compulsoryQuestionDivIds, String allQuestionDivIds) {
		String ret = "";
		if(editing) {
			// Add the two buttons: add new question + add new survey page
			ret += String.format("<table align='center' border='1'>");
			ret += String.format("<tr>" +
					"<td><input type='image' src='img/preset/buttons/addquestion.png' onclick='addNewQuestion()'></td>" +
					"<td></td>"+
					"<td></td>"+
					"<td><input type='image' src='img/preset/buttons/addpage.png' onclick='addNewSurveyPage()'></td>" +
					"<td></td>"+
					"<td></td>"+
					"<td><input type='image' src='img/preset/buttons/saveexit.png' onclick='saveExit()'></td>" +
					"<td></td>"+
					"<td></td>"+
					String.format("<td><input type='image' src='img/preset/buttons/launch.png' %s onclick='golaunch(\"%s\")'></td>", getDisplaysurveystatus().equals(Common.SURVEYPOLL_STATUS_LAUNCHED)? "disabled='disabled'":"", getUid()) +
					"</tr>");
		} else {
			// Add the submit button + footer
			ret += String.format("<div class='submit'>");
			ret += String.format("<input type='button' value='Submit' length='35' id='submit_surveyresponse' name='Submit' onclick='submitsurveyresponse(\"%s\", \"%s\", \"%s\")'></div>", getUid(), compulsoryQuestionDivIds, allQuestionDivIds);
			ret += String.format("</div>");
			ret += String.format("</div>");
			ret += String.format("</div>");
		}
		return ret;
	}
	
	public String getStepSettingHTMLString() { return getStepSettingHTMLString("step1"); }
	
	public static String getStepSettingHTMLString(String step) {
		return String.format("<table width='900' border='0' cellpadding='0' style='margin-top:12px;margin-bottom:8px;'>" +
				"<tr>" +
					"<td>" +
						String.format("<div align='left' class='stepsetting'><input type='image' src='img/preset/buttons/%s.jpg' border='0'></div>", step) +
					"</td>" +
				"</tr>" +
			"</table>");
	}
	
	public String getHeaderPublisher() {
		String ret = "";
		ret += String.format("<center>");
		ret = String.format("<div id='top'>");
        ret += String.format("<table width='100%%'>");
        ret += String.format("<tr>");
        ret += String.format("<td width='80%%' class='t1'>");
        ret += String.format("<b>%s</b>",getTitle());
        ret += String.format("<p>%s</p>", getIntroduction());
        ret += String.format("</td>");
        ret += String.format("<td width='20%%' class='t2'><div id='bigbanner_pos1'>");
        ret += ServiceDefStore.getServiceTypeDef(getType()).getDisplaycustomizedlogo() == 1? getLogoimg() : AdsListenerFactory.getOneAds().getHtmlcode();	
        ret += String.format("</div></td>");
        ret += String.format("</tr>");
        ret += String.format("</table>");
        ret += String.format("</div>");	
		return ret;
	}
}
