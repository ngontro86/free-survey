package com.imarketing.db.dao;

import com.imarketing.db.UserCredit;

public interface IUserCreditDAO extends ICommonDAO {
	
	public boolean insertUserCredit(UserCredit uc) throws Exception;
	
	public UserCredit getUserCredit(long id) throws Exception;
	
	public UserCredit getUserCredit(String reason) throws Exception;
	
}

