
$(document).ready(function() {
    displayAds();
});

function showFAQs(id) {
	var FAQ = document.getElementById("DetailsFAQ");
	FAQ.style.display = "none";
	var FAQs = document.getElementById("FAQs");
	FAQs.style.display = "block";
	
	FAQManagement.getAllFAQsByCategoryId(id, showCategoryList);
}

function showCategoryList(data) {
    document.getElementById("FAQs").innerHTML = data;
}

function showDetailsFAQ(idCategory, idFAQ) {
	var FAQs = document.getElementById("FAQs");
	FAQs.style.display = "none";
	var FAQ = document.getElementById("DetailsFAQ");
	FAQ.style.display = "block";
	FAQManagement.getDetailsFAQ(idCategory, idFAQ, showFAQ);
}

function showFAQ(data) {
    document.getElementById("DetailsFAQ").innerHTML = data;
}