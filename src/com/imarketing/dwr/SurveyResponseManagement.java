package com.imarketing.dwr;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.directwebremoting.WebContextFactory;
import com.imarketing.common.Common;
import com.imarketing.conf.ConfigStore;
import com.imarketing.db.AnswerOption;
import com.imarketing.db.AnswerOptionStats;
import com.imarketing.db.Participant;
import com.imarketing.db.Question;
import com.imarketing.db.QuestionStats;
import com.imarketing.db.Survey;
import com.imarketing.db.OneResponse;
import com.imarketing.db.SurveyStats;
import com.imarketing.db.User;
import com.imarketing.db.UserCredit;
import com.imarketing.db.dao.DAOFactory;
import com.imarketing.db.dao.IAnswerOptionDAO;
import com.imarketing.db.dao.IAnswerOptionStatsDAO;
import com.imarketing.db.dao.IParticipantDAO;
import com.imarketing.db.dao.IQuestionDAO;
import com.imarketing.db.dao.IQuestionStatsDAO;
import com.imarketing.db.dao.ISurveyDAO;
import com.imarketing.db.dao.IOneResponseDAO;
import com.imarketing.db.dao.ISurveyStatsDAO;
import com.imarketing.db.dao.IUserCreditDAO;
import com.imarketing.db.dao.IUserDAO;
import com.imarketing.servicedef.ServiceDefStore;
import com.imarketing.time.GlobalTime;


public class SurveyResponseManagement extends AbstractManagement {
	
	private Logger logger = Logger.getLogger(SurveyResponseManagement.class);
	public SurveyResponseManagement() throws Exception {
		super();
		setRequest(WebContextFactory.get().getHttpServletRequest());
	}
	
	public SurveyResponseManagement(HttpServletRequest req) throws Exception {
		super(req);
	}
	
	@SuppressWarnings("unchecked")
	// allResponses is a map of questionUID map to a map of (AnswerOption Id as 'name' and value)
	public Map<String, String> processOneSurveyResponse(String surveyUid, Map<String, Map<String, String>> allResponses) {
		Map<String, String> ret = new HashMap<String, String>();
		ret.put("success", "false"); 
		try {
			long timeNow = GlobalTime.currentTimeSeconds();
			
			IParticipantDAO participantDAO = DAOFactory.getDefaultFactory().getParticipantDAO();
			ISurveyDAO surveyDAO = DAOFactory.getDefaultFactory().getSurveyDAO();
			IQuestionDAO questionDAO = DAOFactory.getDefaultFactory().getQuestionDAO();
			IAnswerOptionDAO answerOptionDAO = DAOFactory.getDefaultFactory().getAnswerOptionDAO();
			IOneResponseDAO surveyResponseDAO = DAOFactory.getDefaultFactory().getResponseDAO();
			
			Participant newParticipant = new Participant();
			newParticipant.setTimestamp(timeNow);
			newParticipant.setIporphone(getRequest().getRemoteHost());
			newParticipant = participantDAO.insertParticipant(newParticipant);
			
			Survey survey = surveyDAO.getSurveyByUID(surveyUid);
			
			for(Entry<String, Map<String, String>> e : allResponses.entrySet()){
				Question q = questionDAO.getQuestionByUid(e.getKey());
				String questionType = q.getQuestion_type();
				for(Entry<String, String> res : e.getValue().entrySet()){
					String value = res.getValue();
					if(value == null || value.length() < 1) continue;
					AnswerOption ao = null;
					OneResponse surveyResponse = new OneResponse();
					surveyResponse.setTimestamp(timeNow);
					if(questionType.equals(Common.QUESTION_TYPE_MULTIPLE_CHOICES)) {
						Long aoId = Common.toLong(value, -1L);	
						ao = answerOptionDAO.getAnswerOptionById(aoId);
						surveyResponse.setAction("");
					} else if(questionType.equals(Common.QUESTION_TYPE_TRUE_FALSE)) {
						Long aoId = Common.toLong(value, -1L);	
						ao = answerOptionDAO.getAnswerOptionById(aoId);
						surveyResponse.setAction("");
					} else if(questionType.equals(Common.QUESTION_TYPE_MORE_THAN_ONE) && Common.toInt(value, 0) != 0){
						Long aoId = Common.toLong(res.getKey(), -1L);
						ao = answerOptionDAO.getAnswerOptionById(aoId);	
						surveyResponse.setAction("");
					} else if(questionType.equals(Common.QUESTION_TYPE_OPEN_ENDED)){
						Long aoId = Common.toLong(res.getKey(), -1L);
						ao = answerOptionDAO.getAnswerOptionById(aoId);	
						surveyResponse.setAction(res.getValue());
					} else if(questionType.equals(Common.QUESTION_TYPE_ORDER_ITEM)) {
						Long aoId = Common.toLong(value, -1L);
						ao = answerOptionDAO.getAnswerOptionById(aoId);	
						surveyResponse.setAction(value);
					} else if(questionType.equals(Common.QUESTION_TYPE_RATING_SCALE)){
						Long aoId = Common.toLong(value, -1L);
						ao = answerOptionDAO.getAnswerOptionById(aoId);	
						surveyResponse.setAction(value);
					}
					surveyResponse.setParticipant(newParticipant);
					newParticipant.getResponses().add(surveyResponse);
					surveyResponseDAO.insertOrUpdateSurveyResponse(surveyResponse);
					participantDAO.insertOrUpdateParticipant(newParticipant);
					boolean tempRet = ao == null ? true : addSurveyResponse(survey, q, ao, surveyResponse);
					if(!tempRet) { 
						ret.put("message", "attempt to insert a survey response failed.");
						participantDAO.close(); 
						surveyDAO.close(); 
						questionDAO.close(); 
						answerOptionDAO.close();
						surveyResponseDAO.close();
						return ret;
					}
				}
			}
			// Update the total responses in survey
			survey.setResponsecount(survey.getResponsecount() + 1);
			surveyDAO.updateSurvey(survey);
			
			ret.put("success", "true"); ret.put("message", ServiceDefStore.getServiceTypeDef(survey.getType()).getDisplaycustomizethankyoupage() == 1? survey.getThankyou() : ConfigStore.getConfig("Message.DefaultThankyou"));
			
			// Update the user credit if the survey is not done by anonymous
			updateUserCredit();
			
			participantDAO.close(); surveyDAO.close(); questionDAO.close(); 
			answerOptionDAO.close(); surveyResponseDAO.close();
			
			return ret;
		} catch (Exception e) {
			logger.error("processOneSurveyResponse got exception: ", e);
			ret.put("message", e.getMessage());
		}
		return ret;
	}
	
	@SuppressWarnings("unchecked")
	public boolean addSurveyResponse(Survey survey, Question question, AnswerOption answerOption, OneResponse res) throws Exception {
		IAnswerOptionStatsDAO answerOptionStatsDAO = DAOFactory.getDefaultFactory().getAnswerOptionStatsDAO();
		AnswerOptionStats stats = answerOptionStatsDAO.getStatsByAnswerOption(answerOption);
		if(stats == null) {
			stats = new AnswerOptionStats(); 
			stats.setAnsweroption(answerOption);
		}
		stats.getResponses().add(res);
		stats.setLastupdated_timestamp(GlobalTime.currentTimeSeconds());
		answerOptionStatsDAO.insertOrUpdateAnswerOptionStats(stats);
		
		IQuestionStatsDAO questionStatsDAO = DAOFactory.getDefaultFactory().getQuestionStatsDAO();
		QuestionStats qStats = questionStatsDAO.getStatsByQuestion(question);
		if(qStats == null) {
			qStats = new QuestionStats();
			qStats.setQuestion(question);
			qStats.setTotal_responses(0L);
		}
		qStats.setTotal_responses(qStats.getTotal_responses() + 1);
		((Set<AnswerOptionStats>)qStats.getAnswer_stats()).add(stats);
		questionStatsDAO.insertOrUpdateQuestionStats(qStats);
		
		ISurveyStatsDAO surveyStatsDAO = DAOFactory.getDefaultFactory().getSurveyStatsDAO();
		SurveyStats surveyStats = surveyStatsDAO.getStatsBySurvey(survey);
		if(surveyStats == null) {
			surveyStats = new SurveyStats();
			surveyStats.setSurvey(survey);
		}
		surveyStats.setLastupdated_timestamp(GlobalTime.currentTimeSeconds());
		((Set<QuestionStats>)surveyStats.getQuestionstats()).add(qStats);
		surveyStats.setTotal_response(surveyStats.getTotal_response() + 1);
		surveyStatsDAO.insertOrUpdateSurveyStats(surveyStats);
		
		answerOptionStatsDAO.close();
		questionStatsDAO.close();
		surveyStatsDAO.close();
		
		return true;
	}
	
	private void updateUserCredit() throws Exception {
		if(userAuthenticated()){
			User u = (User)getRequest().getSession().getAttribute(Common.HTTPSESSION_USER_ATTRIBUTION);
			IUserDAO userDAO = DAOFactory.getDefaultFactory().getUserDAO();
			IUserCreditDAO creditUserDAO = DAOFactory.getDefaultFactory().getUserCreditDAO();
			u = userDAO.getUser(u.getId());
			
			UserCredit uc = u.getCredit();
			UserCredit newUC = new UserCredit();
			newUC.setHistory(uc);
			newUC.setConversionrate(1);
			newUC.setCredit((uc == null? 0 : uc.getCredit()) + Common.DEFAULT_ADDED_CREDIT); 
			newUC.setLastupdated_timestamp(GlobalTime.currentTimeSeconds());
			newUC.setReason("Finished a survey");
			creditUserDAO.insertUserCredit(newUC);
			u.setCredit(newUC);
			u = userDAO.updateUser(u);
			
			userDAO.close(); creditUserDAO.close();
			
			getRequest().getSession().setAttribute(Common.HTTPSESSION_USER_ATTRIBUTION, u);
			
		}
	}
}
	
	
