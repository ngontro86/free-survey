<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="includes/google_analytics.inc"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>KhaosatOnline - utility</title>

<script type='text/javascript' src='dwr/engine.js'></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src='dwr/interface/UserManagement.js'></script>

<script type='text/javascript' src='js/common/common.js'></script>

<script language="javascript">
    <% String userUid = request.getParameter("uid"); %>
    var userUid = "<%=userUid%>";
    if(userUid != null) {
     var respBack = function(data) {
         if(data) {
            alert("Tài khoản của bạn đã được kích hoạt thành công. Bạn sẽ được chuyển tới trang cá nhân trong vòng giây lát");
            window.location="user.jsp"; 
          } else {
            alert("Báo lỗi...");
          }
      }
      UserManagement.activateUser(userUid, respBack);
    }
</script>

</head>
<body>

</body>
</html>