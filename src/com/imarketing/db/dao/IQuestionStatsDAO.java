package com.imarketing.db.dao;

import com.imarketing.db.Question;
import com.imarketing.db.QuestionStats;

public interface IQuestionStatsDAO extends ICommonDAO {
	
	public QuestionStats getStatsById(long id) throws Exception;
	
	public QuestionStats getStatsByQuestion(Question question) throws Exception;
	
	public boolean insertOrUpdateQuestionStats(QuestionStats questionStats) throws Exception;

}
