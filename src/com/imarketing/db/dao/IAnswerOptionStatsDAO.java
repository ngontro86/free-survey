package com.imarketing.db.dao;

import com.imarketing.db.AnswerOption;
import com.imarketing.db.AnswerOptionStats;

public interface IAnswerOptionStatsDAO extends ICommonDAO {
	
	public boolean insertOrUpdateAnswerOptionStats(AnswerOptionStats answerOptionStats) throws Exception;
	
	public AnswerOptionStats getStatsByAnswerOption(AnswerOption option) throws Exception;
}
