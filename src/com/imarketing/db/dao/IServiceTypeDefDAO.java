package com.imarketing.db.dao;

import java.util.List;

import com.imarketing.db.ServiceTypeDef;

public interface IServiceTypeDefDAO extends ICommonDAO {
	public List<ServiceTypeDef> loadServiceTypeDef() throws Exception;
}
