package com.imarketing.db.dao.mysql;

import java.util.List;
import org.hibernate.Query;
import com.imarketing.db.Question;
import com.imarketing.db.dao.IQuestionDAO;

public class QuestionMySQLDAO extends CommonMySQLDAO implements IQuestionDAO {

	@SuppressWarnings("unchecked")
	@Override
	public Question insertQuestion(Question q) throws Exception {
		beginTransaction();
		session.save(q);
		flush();
		return q;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Question getQuestionByUid(String uid) throws Exception {
		beginTransaction();
		Query query = session.createQuery("from Question where uid = :uid");
		query.setString("uid", uid);
		List<Question> l = query.list();
		return (l == null || l.isEmpty())? null : l.get(0);
	}

	@Override
	public Question getQuestionById(long questionId) throws Exception {
		beginTransaction();
		Question ret = (Question) session.get(Question.class, new Long(questionId));
		return ret;
	}

	@Override
	public boolean updateQuestion(Question e) throws Exception {
		beginTransaction();
		session.saveOrUpdate(e);
		flush();
		return true;
	}

	@Override
	public boolean deleteQuestion(Question e) throws Exception {
		beginTransaction();
		session.delete(e);
		flush();
		return true;
	}

}
