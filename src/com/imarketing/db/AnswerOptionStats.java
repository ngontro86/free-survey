package com.imarketing.db;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@SuppressWarnings({ "serial", "unchecked" })
public class AnswerOptionStats implements Serializable, Comparable {
	@Id
	private long id;
	
	@OneToOne
	private AnswerOption answeroption;
	
	@OneToMany
	private Set responses = new HashSet(); // a set of SurveyResponse
	
	private long lastupdated_timestamp;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public AnswerOption getAnsweroption() {
		return answeroption;
	}
	public void setAnsweroption(AnswerOption answeroption) {
		this.answeroption = answeroption;
	}
	public Set getResponses() {
		return responses;
	}
	public void setResponses(Set responses) {
		this.responses = responses;
	}
	
	public long getLastupdated_timestamp() {
		return lastupdated_timestamp;
	}
	public void setLastupdated_timestamp(long lastupdated_timestamp) {
		this.lastupdated_timestamp = lastupdated_timestamp;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final AnswerOptionStats other = (AnswerOptionStats) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	@Override
	public int compareTo(Object o) {
		if(o == null) return 1;
		if(o instanceof AnswerOptionStats){
			AnswerOptionStats aos = (AnswerOptionStats)o;
			if(aos.getId() == getId()) return 0;
			if(getId() > aos.getId()) return 1;
		}
		return -1;
	}
}
