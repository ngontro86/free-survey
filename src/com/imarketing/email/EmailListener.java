package com.imarketing.email;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.imarketing.conf.ConfigStore;
import com.imarketing.dbwriter.DBWriterListener;

public class EmailListener implements ServletContextListener, Runnable {
	private static Logger logger = Logger.getLogger(EmailListener.class);
	
	private static BlockingQueue<EmailTask> q = new LinkedBlockingQueue<EmailTask>();
	
	private volatile boolean stop = false;
		
	@Override
	public void run() {
		while(!stop) {
			try {
				EmailTask task = q.take();
				if(!FetchMail.sendEmail(task)) {
					task.setRetriesCnt(task.getRetriesCnt()+1);
					if(task.getRetriesCnt() < FetchMail.MAX_RETRIES) q.put(task);
				}
			} catch (Exception e){
				logger.error("run() got exception: ", e);
				DBWriterListener.writeLogs(EmailListener.class + ":run got exception: " + e.getMessage());
			}
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		this.stop = true;
		List<EmailTask> remainingTasks = new ArrayList<EmailTask>();
		q.drainTo(remainingTasks);
		logger.debug("contextDestroyed got called, sending the remaining EmailTask now!");
		for(EmailTask task : remainingTasks) FetchMail.sendEmail(task);
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		if(!ConfigStore.containConfig(ConfigStore.BASEPATH)) 
			ConfigStore.putConfig(ConfigStore.BASEPATH, arg0.getServletContext().getRealPath(""));
		logger.error("contextInitialized() called. " +
				"Base path: " + ConfigStore.getConfig(ConfigStore.BASEPATH) 
				+ ", contain key: " + ConfigStore.containConfig(ConfigStore.BASEPATH));
		
		new Thread(this).start();
	}
	
	public static void addEmailTask(EmailTask task) {
		try { q.put(task); } catch (Exception e){ logger.error("addEmailTask() got exception: ", e); }
	}
}
