package com.imarketing.db.dao.mysql;

import com.imarketing.db.Participant;
import com.imarketing.db.dao.IParticipantDAO;

public class ParticipantMySQLDAO extends CommonMySQLDAO implements
		IParticipantDAO {

	@Override
	public Participant getParticipantById(long id) throws Exception {
		beginTransaction();
		return (Participant)session.get(Participant.class, new Long(id));
	}

	@Override
	public Participant insertOrUpdateParticipant(Participant p) throws Exception {
		beginTransaction();
		session.saveOrUpdate(p);
		flush();
		return getParticipantById(p.getId());
	}

	@Override
	public Participant insertParticipant(Participant p) throws Exception {
		beginTransaction();
		Long newId = (Long)session.save(p);
		flush();
		System.err.println("newId: " + newId);
		return getParticipantById(newId);
	}

}
