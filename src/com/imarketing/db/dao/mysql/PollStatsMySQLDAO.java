package com.imarketing.db.dao.mysql;

import com.imarketing.db.PollStats;
import com.imarketing.db.dao.IPollStatsDAO;

public class PollStatsMySQLDAO extends CommonMySQLDAO implements IPollStatsDAO {

	@Override
	public PollStats getPollStatsById(long id) throws Exception {
		beginTransaction();
		return (PollStats)session.get(PollStats.class, new Long(id));
	}

}
