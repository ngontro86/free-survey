package com.imarketing.db.dao;

import com.imarketing.db.SurveyPage;

public interface ISurveyPageDAO extends ICommonDAO {
	
	public boolean insertSurveyPage(SurveyPage surveyPage) throws Exception;
	
	public SurveyPage updateSurveyPage(SurveyPage spage) throws Exception;
	
	public SurveyPage getSurveyPage(long id) throws Exception;
}
