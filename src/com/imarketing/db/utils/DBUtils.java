package com.imarketing.db.utils;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

public class DBUtils {
	
	private Logger logger = Logger.getLogger(DBUtils.class);
	
	public boolean insertMap(Session session, Map<String, Object> map, String table) throws Exception {
		if(map == null || map.isEmpty()) {
			logger.error("insertMap() got an invalid map table!");
			return false;
		}
		
		String query = String.format("replace into `%s`", table), keyPart = "", valuePart = "";
		
		for(Entry<String, Object> e : map.entrySet()) {
			keyPart += String.format("`%s`,", e.getKey());
			valuePart += e.getValue() == null ? "null, " : String.format("%s,", e.getValue().toString());
		}
		
		keyPart = keyPart.substring(0, keyPart.length() - 1);
		valuePart = valuePart.substring(0, valuePart.length() - 1);
		
		query += String.format("(%s) values (%s)", keyPart, valuePart);
		
		session.beginTransaction();
		Query q = session.createQuery(query);
		return q.executeUpdate() >= 0;
	}
}
