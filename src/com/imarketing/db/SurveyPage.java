package com.imarketing.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import com.imarketing.common.Common;

@SuppressWarnings({ "serial", "unchecked" })
public class SurveyPage implements Serializable, Comparable {
	private long id;

	private long idx;
	private String design;
	private String header;
	private String footer;
	
	private Set questions; // set of question

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public long getIdx() {
		return idx;
	}

	public void setIdx(long idx) {
		this.idx = idx;
	}

	public String getDesign() {
		return design;
	}

	public void setDesign(String design) {
		this.design = design;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getFooter() {
		return footer;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	public Set getQuestions() {
		return questions;
	}

	public void setQuestions(Set questions) {
		this.questions = questions;
	}

	@Override
	public int compareTo(Object o) {
		if(o instanceof SurveyPage){
			SurveyPage sp = (SurveyPage)o;
			if(sp.getId() == getId()) return 0;
			if(getId() > sp.getId()) return 1;
			return -1;
		}
		return 1;
	}
	
	@SuppressWarnings("unchecked")
	public String toHTMLString(String surveyUid, boolean editing) {
		String ret = "";
		ret += String.format("<div id='surveyContainer' class='myBox' style='margin-bottom:30px;'><br/> <b>Page %d</b>", getIdx());
		ret += String.format("<div class='questionContainer'> " +
				"<table border=0 width=100%%>");
		
		List<Question> listQuestion = new ArrayList<Question>(getQuestions());
		Collections.sort(listQuestion, new Comparator<Question>(){
			@Override
			public int compare(Question o1, Question o2) {
				if(o1.getIdx() == o2.getIdx()) return 0;
				if(o1.getIdx() > o2.getIdx()) return 1;
				return -1;
			}
		});
		
		for(Question q : listQuestion){
			ret += q.toHTMLString(editing, surveyUid, getId());
			ret += Common.DIVIDER_CONTAINER;
		}
		
		// Remove the divider container for the last question
		if(ret.length() > Common.DIVIDER_CONTAINER.length()) 
			ret = ret.substring(0, ret.length() - Common.DIVIDER_CONTAINER.length());
		
		ret += String.format("</table> ");
		return ret;
	}
	
}
