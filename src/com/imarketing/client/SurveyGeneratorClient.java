package com.imarketing.client;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.imarketing.common.Common;
import com.imarketing.conf.xml.SimpleXMLFileParserFactory;
import com.imarketing.db.Survey;
import com.imarketing.db.SurveyStats;
import com.imarketing.db.dao.DAOFactory;
import com.imarketing.db.dao.ISurveyDAO;
import com.imarketing.db.dao.ISurveyStatsDAO;
import com.imarketing.dbwriter.DBWriterListener;
import com.imarketing.obj.CustomizedInputTable;

public class SurveyGeneratorClient extends AbstractClient {

	private Logger logger = Logger.getLogger(SurveyGeneratorClient.class);
	
	public SurveyGeneratorClient(HttpServletRequest req,
			HttpServletResponse res) {
		super(req, res);
	}
	
	public String getHTMLForm(String name) throws Exception {
		try {
			if(ClientStoreFactory.getInstance().cached(String.format("%s:%s", SurveyGeneratorClient.class.toString(), name))) {
				return ClientStoreFactory.getInstance().getCachedString(String.format("%s:%s", SurveyGeneratorClient.class.toString(), name)).toString();
			}
			String ret = "";
			CustomizedInputTable cit = SimpleXMLFileParserFactory.getCustomizedInputTable(name);
			ret = cit.toHTMLString();
			ClientStoreFactory.getInstance().cacheObject(String.format("%s:%s", SurveyGeneratorClient.class.toString(), name), ret);
			return ret;
		} catch (Exception e){
			logger.error("getHTMLForm() got exception: ", e);
			DBWriterListener.writeLogs("getHTMLForm() got exception: " +e.getMessage());
			return "";
		}
	}
	
	public String getSurveyHTML(String uid) throws Exception {
		String ret = "";
		ISurveyDAO surveyDAO = DAOFactory.getDefaultFactory().getSurveyDAO();
		Survey s = surveyDAO.getSurveyByUID(uid);
		if(s.getDisplaysurveystatus().equals(Common.SURVEYPOLL_STATUS_LAUNCHED)) {
			ret += s.toHTMLString(false);
			surveyDAO.close();
			return ret;
		}
		ret += s.getDisplaysurveystatus().equals(Common.SURVEYPOLL_STATUS_EDITING)? String.format("Khảo sát đang được %s, xin vui lòng quay lại sau ít giây", s.getDisplaysurveystatus())
				: String.format("Khảo sát này đã quá thời gian sử dụng cho phép");
		surveyDAO.close();
		return ret;
	}	
	
	public String getSurveyStatsHTML(String uid) throws Exception {
		String ret = "";
		try {
			ISurveyDAO surveyDAO = DAOFactory.getDefaultFactory().getSurveyDAO();
			ISurveyStatsDAO surveyStatsDAO = DAOFactory.getDefaultFactory().getSurveyStatsDAO();
			Survey s = surveyDAO.getSurveyByUID(uid);
			if(s.getDisplayresultstatus().equals(Common.SURVEY_RESULT_EXPIRED_STATUS)) {
				surveyDAO.close(); surveyStatsDAO.close();
				return String.format("Khảo sát này đã quá thời gian sử dụng cho phép");
			}
			SurveyStats surveyStats = surveyStatsDAO.getStatsBySurvey(s);
			if(surveyStats != null) ret = surveyStats.toHTMLString();
			surveyDAO.close(); surveyStatsDAO.close();
			return ret;
		} catch (Exception e){
			logger.error("getSurveyStatsHTML() got exception: ", e);
			DBWriterListener.writeLogs("getSurveyStatsHTML() got exception: " +e.getMessage());
			return "";
		}
	}
	
}
