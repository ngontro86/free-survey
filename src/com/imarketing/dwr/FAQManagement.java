package com.imarketing.dwr;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.imarketing.db.FAQ;
import com.imarketing.db.dao.DAOFactory;
import com.imarketing.db.dao.IFAQDAO;

public class FAQManagement extends AbstractManagement {
	
	public FAQManagement(HttpServletRequest req) throws Exception { super(req); }
	public FAQManagement() throws Exception { super(); }
	
	public String getAllFAQsByCategoryId(int id) throws Exception {
		String ret = "";
		IFAQDAO faqDAO = DAOFactory.getDefaultFactory().getFAQDAO();
		List<FAQ> faqList = faqDAO.getAllFAQs(id);
		if (faqList.isEmpty() == true) {
			return "Không có câu hỏi";
		}
		for ( int i = 0; i < faqList.size(); i++) {
			ret += String.format("%d. <a href='%s'>%s</a><br />",(i+1), "javascript:showDetailsFAQ("+id+","+faqList.get(i).getId()+")", faqList.get(i).getQuestion());
		}
		faqDAO.close();
		return ret;
	}
	
	public String getDetailsFAQ(int idCategory, int idFAQ) throws Exception {
		String ret = "";
		IFAQDAO faqDAO = DAOFactory.getDefaultFactory().getFAQDAO();
		FAQ faq = faqDAO.getFAQ(idFAQ);
		ret += String.format("<b>Câu hỏi: %s</b><br> Trả lời: %s</li>", faq.getQuestion(), faq.getAnswer());
		
		List<FAQ> faqList = faqDAO.getAllFAQs(idCategory);
		if(faqList.isEmpty() == true || faqList.size() == 1)
			return ret;

		ret += "<br /> <br /><b>Câu hỏi khác:</b><br /> <br />";
		for ( int i = 0; i < faqList.size(); i++) {
			if( faqList.get(i).getId() == idFAQ ) {
				ret += "";
			} else {
				ret += String.format("<a href='%s'>%s</a><br />", "javascript:showDetailsFAQ("+idCategory+","+faqList.get(i).getId()+")", faqList.get(i).getQuestion());
			}
		}
		faqDAO.close();
		return ret;
	}

}
