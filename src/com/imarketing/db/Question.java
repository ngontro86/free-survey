package com.imarketing.db;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.imarketing.common.Common;

@SuppressWarnings("unchecked")
public class Question implements Comparable {
	private long id;

	private long idx;

	private String uid;
	private String question;
	private String question_type;
	private boolean answer_compulsory;
	private boolean append_default_option;
	private String defaultoptionstyle; // single line or multiple line

	private String displayansweroptionstyle; // vertically or in a dropdown

	private long displayanswercolumns;

	private Set answer_options;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getQuestion_type() {
		return question_type;
	}

	public void setQuestion_type(String question_type) {
		this.question_type = question_type;
	}

	public boolean isAnswer_compulsory() {
		return answer_compulsory;
	}

	public void setAnswer_compulsory(boolean answer_compulsory) {
		this.answer_compulsory = answer_compulsory;
	}

	public boolean isAppend_default_option() {
		return append_default_option;
	}

	public void setAppend_default_option(boolean append_default_option) {
		this.append_default_option = append_default_option;
	}

	public Set getAnswer_options() {
		return answer_options;
	}

	public void setAnswer_options(Set answer_options) {
		this.answer_options = answer_options;
	}

	public String getDisplayansweroptionstyle() {
		return displayansweroptionstyle;
	}

	public void setDisplayansweroptionstyle(String displayansweroptionstyle) {
		this.displayansweroptionstyle = displayansweroptionstyle;
	}

	public String getDefaultoptionstyle() {
		return defaultoptionstyle;
	}

	public void setDefaultoptionstyle(String defaultoptionstyle) {
		this.defaultoptionstyle = defaultoptionstyle;
	}

	public long getIdx() {
		return idx;
	}

	public void setIdx(long idx) {
		this.idx = idx;
	}

	public long getDisplayanswercolumns() {
		return displayanswercolumns;
	}

	public void setDisplayanswercolumns(long displayanswercolumns) {
		this.displayanswercolumns = displayanswercolumns;
	}

	@Override
	public int compareTo(Object o) {
		if(o instanceof Question){
			Question q = (Question)o;
			if(q.getId() == getId()) return 0;
			if(getId() > q.getId()) return 1;
			return -1;
		}
		return 1;
	}

	// TODO will need to return different HTML string depends on the display answer options style,... etc.
	public String toHTMLString(boolean editing, String surveyUid, long surveyPageId) {
		String ret = "";

		// Put all the question in a div
		ret += String.format("<div class='questioncontainer' id='%s'>", getUid());

		ret += String.format("<table width='100%%' border='0'>" +
				"<tbody>" +
				"<tr>" +
				"<td>" +
				// For editing
				appendManagedButtonHTMLString(editing, surveyUid, surveyPageId));

		ret += String.format("<table border=0 width=100%% class='questiontexttable'>");
		ret += String.format("<tr>" +
				"<td valign=top width=25 class='questionnumber'><a name=question%d></a>%d)</td>", getIdx(), getIdx());
		ret += String.format("<td valign=center> %s </td>" +
				"</tr>" +
				"</table>", getQuestion() + (isAnswer_compulsory()? "<font color='red'>*</font>" : ""));

		ret += String.format("<div class='answercontainer' id='answeroption_%s'> <table border=0 cellpadding=0>", getUid());
		ret += getAnswerOptionHTMLString();
		ret += String.format("</div>"); // End of the answercontainer

		ret += String.format("</td>" +
				"</tr>" +
				"</tbody>"+
				"</table>" +
		"</div>"); // End of questionContainer

		return ret; 
	}
	
	private String appendManagedButtonHTMLString(boolean editing, String surveyUid, long surveyPageId) {
		return editing? String.format("<p class='norm'>" +
				String.format("<input type='image' border='0' src='img/preset/buttons/insert.gif' onclick='insertquestion(\"%d\",\"%d\")' /></a>", getIdx(), surveyPageId) +
				String.format("<input type='image' border='0' src='img/preset/buttons/edit.gif' onclick='editquestion(\"%s\", \"%d\", \"%s\")' /></a>", surveyUid, getIdx(), getUid()) +
				String.format("<input type='image' border='0' src='img/preset/buttons/delete_question.gif' onclick='deletequestion(\"%d\",\"%d\")' /></a></p>", getId(), surveyPageId)) 
				: "";
	}

	@SuppressWarnings("unchecked")
	private String getAnswerOptionHTMLString() {
		String ret = "";

		// TODO Display the answer options vertically or horizontally
		if(!getDisplayansweroptionstyle().equals(Common.DISPLAY_ANSWER_OPTION_VERTICAL)){
			// Need to implement differently for this case
		}

		List<AnswerOption> listAnswerOptions = new ArrayList<AnswerOption>(getAnswer_options());
		Collections.sort(listAnswerOptions, new Comparator<AnswerOption>(){
			@Override
			public int compare(AnswerOption o1, AnswerOption o2) {
				if(o1.getIdx() == o2.getIdx()) return 0;
				if(o1.getIdx() > o2.getIdx()) return 1;
				return -1;
			}
		});

		if(question_type.equals(Common.QUESTION_TYPE_MULTIPLE_CHOICES) 
				|| question_type.equals(Common.QUESTION_TYPE_TRUE_FALSE)) {
			ret += String.format("<table cellpadding='0' border='0'>" +
			"<tbody>");
			for(AnswerOption ao :  listAnswerOptions) {
				ret += String.format("<tr><td valign='top'>" +
						"<input type='radio' onkeypress='return disableEnterKey()' value='%d' id='%d' name='%s'>%s</input></td></tr>", ao.getId(), ao.getId(), getUid(), ao.getAnswer());
			}	
			ret += String.format("</tbody></table>");
		} else if(question_type.equals(Common.QUESTION_TYPE_MORE_THAN_ONE)) {
			ret += String.format("<table cellpadding='0' border='0'>" +
			"<tbody>");
			for(AnswerOption ao :  listAnswerOptions) {
				ret += String.format("<tr><td valign='top'>" +
						"<input type='checkbox' onkeypress='return disableEnterKey()' value='1' id='%d' name='%d'>%s</input></td></tr>", ao.getId(), ao.getId(), ao.getAnswer());
			}	
			ret += String.format("</tbody></table>");
		} else if(question_type.equals(Common.QUESTION_TYPE_OPEN_ENDED)) {
			ret += String.format("<table cellpadding='0' border='0'>" +
			"<tbody>");
			for(AnswerOption ao : listAnswerOptions) {
				ret += String.format("<tr><td><textarea rows='10' cols='50' name='%d' id='%d' value='%s'></textarea></td></tr>", ao.getId(), ao.getId(), ao.getAnswer());
			}
			ret += "</tbody></table>";
		} else if(question_type.equals(Common.QUESTION_TYPE_RATING_SCALE)) {
			ret += String.format("<br><table width='10px' align='left' cellpadding=3 cellspacing=3 style='border:1px solid #000000;background-color:#efefef;'>");
			
			Map<String, List<AnswerOption>> map = new HashMap<String, List<AnswerOption>>();
			List<AnswerOption> mainRows = new ArrayList<AnswerOption>();
			for(AnswerOption ao : listAnswerOptions) {
				if(!map.containsKey(ao.getAnswer())) { 
					map.put(ao.getAnswer(), new ArrayList<AnswerOption>());
					mainRows.add(ao);
				}
				List<AnswerOption> l = map.get(ao.getAnswer());
				l.add(ao);
			}
			List<AnswerOption> temp = map.get(mainRows.get(0).getAnswer());
			int headerCount = temp.size() + 1;
			String headerHtml = String.format("<tr><th width='%d%%'></th>", (int)(100/headerCount));
			for(AnswerOption ao : temp) headerHtml += String.format("<th width='%d%%'>%s</th>", (int)(100/headerCount), ao.getValue()); 
			headerHtml += "</tr>";
			ret += headerHtml;
			for(AnswerOption tmpAO : mainRows) {
				List<AnswerOption> tmpL = map.get(tmpAO.getAnswer());
				String oneRowHTML = String.format("<tr><td width='%d%%'>%s</td>", (int)(100/headerCount), tmpL.get(0).getAnswer());
				for(AnswerOption ao : tmpL) {
					oneRowHTML += String.format("<td width='%d%%'><input type='radio' onkeypress='return disableEnterKey()' name='%d' id='%s' value='%d'</td>", (int)(100/headerCount), tmpAO.getId(), tmpAO.getAnswer(), ao.getId());
				}
				oneRowHTML += String.format("</tr>");
				ret += oneRowHTML;
			}
			ret += "</table>";
			
		} else if(question_type.equals(Common.QUESTION_TYPE_ORDER_ITEM)) {
			ret += String.format("<br><table width='10px' align='left' cellpadding=3 cellspacing=3 style='border:1px solid #000000;background-color:#efefef;'>");
			
			Map<String, List<AnswerOption>> map = new HashMap<String, List<AnswerOption>>();
			List<AnswerOption> mainRows = new ArrayList<AnswerOption>();
			for(AnswerOption ao : listAnswerOptions) {
				if(!map.containsKey(ao.getAnswer())) { 
					map.put(ao.getAnswer(), new ArrayList<AnswerOption>());
					mainRows.add(ao);
				}
				List<AnswerOption> l = map.get(ao.getAnswer());
				l.add(ao);
			}
			
			for(AnswerOption mainAO : mainRows) {
				String optionString = "";
				List<AnswerOption> tmpL = map.get(mainAO.getAnswer());
				for(AnswerOption ao : tmpL) {
					optionString += String.format("<option value='%d' id='%d'>%s</option>", ao.getId(), ao.getId(), ao.getValue()); 
				}
				ret += String.format("<tr>" +
						"<td>%s</td>" +
						"<td>%s</td>" +
						"</tr>", mainAO.getAnswer(), String.format("<select name='%d' id='%d'>%s</select>", mainAO.getId(), mainAO.getId(), optionString));
			}
			ret += "</table>";
		} 
		return ret;
	}
	
	
}
