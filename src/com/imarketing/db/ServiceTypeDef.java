package com.imarketing.db;

public class ServiceTypeDef implements Comparable<ServiceTypeDef> {
	private long id;
	private String name;
	private long maxpage;
	private long maxquestion;
	private long resnum_openquestion;
	private long displaycustomizedlogo;
	private long displaycustomizethankyoupage;
	private long expirationduration; // in days
	private long resultdisplayduration; // in days
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getMaxpage() {
		return maxpage;
	}
	public void setMaxpage(long maxpage) {
		this.maxpage = maxpage;
	}
	public long getMaxquestion() {
		return maxquestion;
	}
	public void setMaxquestion(long maxquestion) {
		this.maxquestion = maxquestion;
	}
	
	public long getResnum_openquestion() {
		return resnum_openquestion;
	}
	public void setResnum_openquestion(long resnum_openquestion) {
		this.resnum_openquestion = resnum_openquestion;
	}
	
	public long getDisplaycustomizedlogo() {
		return displaycustomizedlogo;
	}
	public void setDisplaycustomizedlogo(long displaycustomizedlogo) {
		this.displaycustomizedlogo = displaycustomizedlogo;
	}
	public long getDisplaycustomizethankyoupage() {
		return displaycustomizethankyoupage;
	}
	public void setDisplaycustomizethankyoupage(long displaycustomizethankyoupage) {
		this.displaycustomizethankyoupage = displaycustomizethankyoupage;
	}
	public long getExpirationduration() {
		return expirationduration;
	}
	public void setExpirationduration(long expirationduration) {
		this.expirationduration = expirationduration;
	}
	
	public long getResultdisplayduration() {
		return resultdisplayduration;
	}
	public void setResultdisplayduration(long resultdisplayduration) {
		this.resultdisplayduration = resultdisplayduration;
	}
	@Override
	public int compareTo(ServiceTypeDef o) {
		if(o != null){
			if(this.getId() > o.getId()) return 1;
			if(this.getId() < o.getId()) return -1;
		}
		return 0;
	}
	
	
}
