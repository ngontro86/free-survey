<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@page import="java.util.Map"%>
<head>
<%@include file="includes/google_analytics.inc"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>KhaosatOnline - data graph</title>
<script type='text/javascript' src='dwr/engine.js'></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script type='text/javascript' src='dwr/interface/UserManagement.js'></script>
<script type='text/javascript' src='dwr/interface/SurveyManagement.js'></script>

<script type='text/javascript' src='js/utils/richtext/wysiwyg.js'></script>
<script type='text/javascript' src='js/utils/jquery.js'></script>
<script type='text/javascript' src='js/utils/msgbox/jquery.dragndrop.min.js'></script>
<script type='text/javascript' src='js/utils/msgbox/jquery.msgbox.js'></script>

<script type='text/javascript' src='js/common/common.js'></script>
<script type='text/javascript' src='js/surveygraph.js'></script>
<script type='text/javascript' src='js/login.js'></script>

<script language="javascript">
	<% String questionStatsId = request.getParameter("questionstatsid"); String chart = request.getParameter("chart"); %>
    var questionStatsId = "<%=questionStatsId%>";
    var chart = "<%=chart%>";
</script>

</head>
<body>
<div id='questionstats'></div>
<div id='spinningwheel'></div>
</body>
</html>