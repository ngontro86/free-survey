package com.imarketing.db.dao;

import java.util.List;

import com.imarketing.db.Survey;

public interface ISurveyDAO extends ICommonDAO {
		
	public Survey insertSurvey(Survey s) throws Exception;
	
	public Survey updateSurvey(Survey s) throws Exception;
		
	public List<Survey> getAllSurveysPublished() throws Exception;
	
	public List<Survey> getLastPublishedSurveys(int limit, long startingId) throws Exception;
	
	public List<Survey> getValidSurveys(int limit, long startingId) throws Exception;
	
	public Survey getSurveyById(long id) throws Exception;
	
	public Survey getSurveyByUID(String uid) throws Exception;
	
	public boolean deleteSurvey(String uid) throws Exception;
	
	public boolean deleteSurvey(Survey s) throws Exception;
	
}
