package com.imarketing.cep.io;

import com.imarketing.cep.util.SocketData;

public interface IProcessor<RECV, SEND> {
	public void process(SocketData<RECV> r, IHandle<SocketData<SEND>> handler);
}
