<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.imarketing.client.SurveyGeneratorClient"%>
<html>
<head>
<%@include file="includes/google_analytics.inc"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>KhaosatOnline - kết quả khảo sát mở</title>

<script type='text/javascript' src='dwr/engine.js'></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src='dwr/interface/SurveyResponseManagement.js'></script>
<script type='text/javascript' src='dwr/interface/AdsManagement.js'></script>


<script type='text/javascript' src='js/utils/richtext/wysiwyg.js'></script>
<script type='text/javascript' src='js/utils/jquery.js'></script>
<script type='text/javascript' src='js/utils/msgbox/jquery.dragndrop.min.js'></script>
<script type='text/javascript' src='js/utils/msgbox/jquery.msgbox.js'></script>

<script type='text/javascript' src='js/common/ads.js'></script>
<script type='text/javascript' src='js/common/common.js'></script>
<script type='text/javascript' src='js/publisher.js'></script>

<link href="css/publisher_style.css" rel="stylesheet" type="text/css" />

<script type="javascript">
<% 
String header = "";
SurveyGeneratorClient surveyGeneratorClient = new SurveyGeneratorClient(request, response);
String surveyUid = request.getParameter("uid") == null? 
        "" : request.getParameter("uid").toString();
String surveyStatsHTML = surveyGeneratorClient.getSurveyStatsHTML(surveyUid);
%>
</script>

</head>
<body>
<div id="surveystats">
<%=surveyStatsHTML%>
</div>
<div id='spinningwheel'></div>
</body>
</html>