package com.imarketing.client;

import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

public class ClientStoreFactory extends ConcurrentHashMap<String, Object> { 
	private static final long serialVersionUID = 1L;
	
	private Logger logger = Logger.getLogger(ClientStoreFactory.class);
	
	private int MAXIMUM_CACHED_ELEMENT = 1000;
	
	private static ClientStoreFactory instance = new ClientStoreFactory();
	
	public static ClientStoreFactory getInstance() {
		return instance;
	}
	
	public boolean cached(String key) throws Exception {
		return containsKey(key);
	}
	
	public String getCachedString(String key) throws Exception {
		logger.debug(String.format("getCachedString(): key: %s", key));
		return get(key).toString();
	}
	
	public void cacheObject(String key, Object value) throws Exception {
		if(instance.size() > MAXIMUM_CACHED_ELEMENT) instance.clear();
		put(key, value);
	}
	
}
