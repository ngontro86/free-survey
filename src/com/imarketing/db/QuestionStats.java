package com.imarketing.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.imarketing.common.Common;
import com.imarketing.servicedef.ServiceDefStore;

@SuppressWarnings({ "serial", "unchecked" })
public class QuestionStats implements Serializable, Comparable {
	private long id;

	private Question question;
	private long total_responses;
	private Set answer_stats = new HashSet(); // a set of AnswerOptionStats
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	
	public long getTotal_responses() {
		return total_responses;
	}
	public void setTotal_responses(long total_responses) {
		this.total_responses = total_responses;
	}
	
	public Set getAnswer_stats() {
		return answer_stats;
	}
	public void setAnswer_stats(Set answer_stats) {
		this.answer_stats = answer_stats;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final QuestionStats other = (QuestionStats) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public int compareTo(Object o) {
		if(o instanceof QuestionStats){
			QuestionStats qs = (QuestionStats)o;
			if(qs.getId() == getId()) return 0;
			if(getId() > qs.getId()) return 1;
			return -1;
		}
		return 1;
	}
	
	@SuppressWarnings("unchecked")
	public String toHTMLString(String surveyType) {
		String ret = "";
		long totalQuesionResponses = 0;
		Map<String, List<AnswerOption>> uniqueAnswerOptions = new HashMap<String, List<AnswerOption>>();
		List<AnswerOption> rowsAO = new ArrayList<AnswerOption>();
		for(AnswerOption ao : new ArrayList<AnswerOption>(getQuestion().getAnswer_options())) {
			if(!uniqueAnswerOptions.containsKey(ao.getAnswer())) {
				uniqueAnswerOptions.put(ao.getAnswer(), new ArrayList<AnswerOption>());
				rowsAO.add(ao);
			}
			uniqueAnswerOptions.get(ao.getAnswer()).add(ao);
		}
		List<AnswerOption> columnsAO = new ArrayList<List<AnswerOption>>(uniqueAnswerOptions.values()).get(0);
		Collections.sort(rowsAO, new AOComparator());
		Collections.sort(columnsAO, new AOComparator());
		Map<String, List<AnswerOptionStats>> collectedStats = new HashMap<String, List<AnswerOptionStats>>();
		for(AnswerOptionStats aos : new ArrayList<AnswerOptionStats>(getAnswer_stats())) {
			if(!collectedStats.containsKey(aos.getAnsweroption().getAnswer()))
				collectedStats.put(aos.getAnsweroption().getAnswer(), new ArrayList<AnswerOptionStats>());
			collectedStats.get(aos.getAnsweroption().getAnswer()).add(aos);
			totalQuesionResponses += aos.getResponses().size();  
		}
		
		ret += String.format("<tr>" +
					"<td valign='top'>" +
						"<table width='100%%' cellspacing='0' cellpadding='0' border='0'>" +
							"<tbody>" +
								"<tr>" +
									"<td width='30' valign='top'>" +
										"<p class='norm'>%d)</p>" +
									"</td>" +
									"<td valign='top'>" +
										"<p class='norm'>%s</p>" +
									"</td>" + 
									String.format("<td><div><input type='image' src='img/preset/buttons/bar_chart.jpg' border='0' onclick='window.open(\"surveygraph.jsp?questionstatsid=%d&chart=BarChart\", \"Response_Graph\", \"menubar=1,resizable=1,width=500,height=400\");return false;'/></div></td>", getId()) +
									String.format("<td><div><input type='image' src='img/preset/buttons/pie_chart.jpg' border='0' onclick='window.open(\"surveygraph.jsp?questionstatsid=%d&chart=PieChart\", \"Response_Graph\", \"menubar=1,resizable=1,width=500,height=400\");return false;'/></div></td>", getId()) +
								"</tr>" +
							"</tbody>" +
						"</table>" +
					"</td>" +
			"</tr>", getQuestion().getIdx(), getQuestion().getQuestion());
		ret += getStatsTableHeader(surveyType, columnsAO);
		for(AnswerOption ao : rowsAO) { // Must be in order
			ret += getAOSHTML(surveyType, ao, columnsAO, collectedStats.get(ao.getAnswer()), totalQuesionResponses);
		}
		ret += "</tbody></table></td></tr>";
		return ret;
	}
	
	private String getStatsTableHeader(String surveyType, List<AnswerOption> list){
		String questionType = getQuestion().getQuestion_type();
		if(questionType.equals(Common.QUESTION_TYPE_RATING_SCALE)) {
			String ret = "";
			ret += String.format("<tr>" +
					"<td>" +
					"<table width='95%%' cellspacing='0' cellpadding='4' border='0' align='center'>" +
						"<tbody>" +
						"<tr>" +
							"<td></td>");
			for(AnswerOption ao : list) { 
				ret += String.format("<td class='tinytext' width='90'>" +
										"<div align='center'>" +
											"<b>%s</b>" +
										"</div>" +
									"</td>", ao.getValue());
			}
			ret += "<td class='tinytext' width='90'>" +
						"<div align='center'>" +
							"<b>Trung bình</b>" +
						"</div>" +
						"</td>" +
						"<td class='tinytext' width='40'>" +
							"<div align='center'>" +
								"<b>Số phản hồi</b>" +
							"</div>" +
						"</td>" +
						"<td width='70'></td>" +
					"</tr>";
			return ret;
		} else if(questionType.equals(Common.QUESTION_TYPE_ORDER_ITEM)) {
			return String.format("<tr>" +
					"<td>" +
					"<table width='95%%' cellspacing='0' cellpadding='4' border='0' align='center'>" +
						"<tbody>" +
						"<tr>" +
							"<td></td><td></td>" +
							"<td class='tinytext' width='90'>" +
								"<div align='center'>" +
									"<b>Trung bình</b>" +
								"</div>" +
							"</td>" +
							"<td class='tinytext' width='40'>" +
								"<div align='center'>" +
									"<b>Số phản hồi</b>" +
								"</div>" +
							"</td>" +
							"<td width='70'></td>" +
						"</tr>");
		} else if(questionType.equals(Common.QUESTION_TYPE_OPEN_ENDED)) {
			return String.format("<tr>" +
					"<td>" +
					"<table width='95%%' cellspacing='0' cellpadding='4' border='0' align='center'>" +
						"<tbody>" +
						"<tr>" +
							"<td class='tinytext'>" +
								String.format("(Hiển thị %d trả lời mới nhất)", ServiceDefStore.getServiceTypeDef(surveyType).getResnum_openquestion())+
							"</td>" +
						"</tr>");
		} else {
			return String.format("<tr>" +
					"<td>" +
					"<table width='95%%' cellspacing='0' cellpadding='4' border='0' align='center'>" +
						"<tbody>" +
						"<tr>" +
							"<td></td><td></td>" +
							"<td class='tinytext' width='90'>" +
								"<div align='center'>" +
									"<b>Phần trăm</b>" +
								"</div>" +
							"</td>" +
							"<td class='tinytext' width='40'>" +
								"<div align='center'>" +
									"<b>Số phản hồi</b>" +
								"</div>" +
							"</td>" +
							"<td width='70'></td>" +
						"</tr>");
		}
	}
	
	@SuppressWarnings("unchecked")
	private String getAOSHTML(String surveyType, AnswerOption ao, List<AnswerOption> headers, List<AnswerOptionStats> li, long totalQuestionResponse){
		String ret = "";
		String questionType = getQuestion().getQuestion_type();
		if(li != null && !li.isEmpty()) Collections.sort(li, new AOStatsComparator());
		if(li == null) li = new ArrayList<AnswerOptionStats>();
		if(questionType.equals(Common.QUESTION_TYPE_RATING_SCALE)){
			double averageScore = 0; long totalMainAOStatsResponse = 0;
			for(AnswerOptionStats aos : li) {
				averageScore = (double)(averageScore * totalMainAOStatsResponse + aos.getResponses().size() * Common.toLong(aos.getAnsweroption().getValue(), 1L))/(totalMainAOStatsResponse + aos.getResponses().size());
				totalMainAOStatsResponse += aos.getResponses().size();
			}
			ret += String.format("<tr bgcolor='#f6f6f6'>" +
					"<td class='tinytext' width='250'>%s<br></td>", ao.getAnswer());
			for(AnswerOption header : headers) {
				boolean gotResponse = false;
				for(AnswerOptionStats aos : li) {
					if(aos.getAnsweroption().getAnswer().equals(ao.getAnswer()) 
							&& header.getValue().equals(aos.getAnsweroption().getValue())){
						ret += String.format("<td class='tinytext'><div align='center'>%.2f%%</div></td>", ((double)(aos.getResponses().size() * 100.0/totalMainAOStatsResponse)));
						gotResponse = true;
						break;
					} 
				}
				if(!gotResponse) {
					ret += String.format("<td class='tinytext'><div align='center'>0%%</div></td>");
				}
			}
			ret += String.format("<td class='tinytext' align='center'>%.2f</td>" +
					"<td class='tinytext' align='center'>%d</td>" +
				"</tr>", averageScore, totalMainAOStatsResponse);
			
		} else if(questionType.equals(Common.QUESTION_TYPE_ORDER_ITEM)) {
			long totalScore = 0, totalMainAOStatsResponse = 0, maxScore = 0;
			for(AnswerOption aop : headers) maxScore = Math.max(maxScore, Common.toLong(aop.getValue(), 0L));
			for(AnswerOptionStats aos : li) {
				totalScore += aos.getResponses().size() * Common.toLong(aos.getAnsweroption().getValue(), 0L);
				totalMainAOStatsResponse += aos.getResponses().size();
			}
			ret += String.format("<tr bgcolor='#f6f6f6'>" +
					"<td class='tinytext' width='250'>%s<br></td>" +
					"<td class='tinytext'><img width='%.2f' height='12' src='img/preset/results_bar.gif'></td>" +
					"<td class='tinytext' align='center'>%d/%d</td>" +
					"<td class='tinytext' align='center'>%d</td>" +
				"</tr>", ao.getAnswer(), (double)(2*totalScore*100.0/totalMainAOStatsResponse/maxScore), totalScore, totalMainAOStatsResponse, totalMainAOStatsResponse, totalMainAOStatsResponse);
		} else if(questionType.equals(Common.QUESTION_TYPE_OPEN_ENDED)) {
			AnswerOptionStats aos = li.get(0); // only one answer option stats in this case
			List<OneResponse> listResponse = new ArrayList<OneResponse>(aos.getResponses());
			Collections.sort(listResponse, new Comparator<OneResponse>(){
				@Override
				public int compare(OneResponse o1, OneResponse o2) {
					if(o1.getId() > o2.getId()) return -1;
					if(o1.getId() < o2.getId()) return 1;
					return 0;
				}
			});
			int cnt = 0;
			for(OneResponse sv : listResponse) {
				if(cnt++ > ServiceDefStore.getServiceTypeDef(surveyType).getResnum_openquestion()) break;
				ret += String.format("<tr bgcolor='#f6f6f6'><td class='tinytext' width='250'>%s<br></td></tr>", sv.getAction());
			}
		} else if(questionType.equals(Common.QUESTION_TYPE_MULTIPLE_CHOICES)
				|| questionType.equals(Common.QUESTION_TYPE_MORE_THAN_ONE)
				|| questionType.equals(Common.QUESTION_TYPE_TRUE_FALSE)){
			long totalScore = 0;
			for(AnswerOptionStats aos : li) totalScore += aos.getResponses().size(); 
			ret += String.format("<tr bgcolor='#f6f6f6'>" +
					"<td class='tinytext' width='250'>%s<br></td>" +
					"<td class='tinytext'><img width='%.2f' height='12' src='img/preset/results_bar.gif'></td>" +
					"<td class='tinytext' align='center'>%.2f%%</td>" +
					"<td class='tinytext' align='center'>%d</td>" +
				"</tr>", ao.getAnswer(), (double)(2*totalScore*100.0/totalQuestionResponse), (double)(totalScore*100.0/totalQuestionResponse), totalQuestionResponse);
		} 
		return ret;
	}
	
	class AOStatsComparator implements Comparator<AnswerOptionStats> {
		@Override
		public int compare(AnswerOptionStats o1, AnswerOptionStats o2) {
			if(o1.getAnsweroption().getIdx() > o2.getAnsweroption().getIdx()) return 1;
			if(o1.getAnsweroption().getIdx() == o2.getAnsweroption().getIdx()) return 0;
			return -1;
		}
	}
	class AOComparator implements Comparator<AnswerOption> {
		@Override
		public int compare(AnswerOption o1, AnswerOption o2) {
			if(o1.getId() > o2.getId()) return 1;
			if(o1.getId() < o2.getId()) return -1;
			return 0;
		}
		
	}
}
