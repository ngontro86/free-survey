package com.imarketing.email;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

import com.imarketing.conf.ConfigStore;

public class FetchMail {
	
	private static Logger logger = Logger.getLogger(FetchMail.class);
	
	public static int MAX_RETRIES = 3;
	
	private static String host = ConfigStore.getConfig("Email.Host");
	private static String port = ConfigStore.getConfig("Email.Port");
	private static String username = ConfigStore.getConfig("Email.Username");
	private static String password = ConfigStore.getConfig("Email.Password");
	
	
	private static Properties props = System.getProperties();
	
	static {
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", port);
		props.put("mail.smtp.starttls.enable", ConfigStore.getConfig("Email.UseSSL"));
		props.put("mail.smtp.user", username);
		props.put("mail.smtp.password", password);
		props.put("mail.smtp.auth", "true");
	}

	private static Session session = Session.getDefaultInstance(props, null);;
	
	public static boolean sendEmail(EmailTask task) {
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			InternetAddress[] addresses = new InternetAddress[task.getToAddresses().length];
			int i = 0;
			for(String addr : task.getToAddresses()) if(addr != null && !addr.equals("")) addresses[i++] = new InternetAddress(addr);
			message.addRecipients(Message.RecipientType.TO, addresses);
			if(task.getCcAddresses() != null) {
				i = 0;
				for(String addr : task.getCcAddresses()) if(addr != null && !addr.equals("")) addresses[i++] = new InternetAddress(addr);
				message.addRecipients(Message.RecipientType.CC, addresses);
			}
			if(task.getBccAddresses() != null) {
				i = 0;
				for(String addr : task.getBccAddresses()) if(addr != null && !addr.equals("")) addresses[i++] = new InternetAddress(addr);
				message.addRecipients(Message.RecipientType.BCC, addresses);
			}
		
			message.setSubject(task.getSubject(), "utf-8");
			message.setText(task.getBody());
			
			message.setHeader("Content-Type","text/html; charset=\"utf-8\"");
			message.setContent(task.getBody(), "text/html; charset=utf-8");
			message.setHeader("Content-Transfer-Encoding", "quoted-printable");
			
			Transport transport = session.getTransport("smtp");
			transport.connect(host, username, password);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
			return true;
		} catch (Exception e){
			logger.error("sendEmail() got exception: ", e);
			return false;
		}
	}
}
