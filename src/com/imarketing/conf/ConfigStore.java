package com.imarketing.conf;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.imarketing.db.Config;
import com.imarketing.db.dao.DAOFactory;
import com.imarketing.db.dao.IConfigDAO;

public class ConfigStore implements ServletContextListener {
	
	private static Logger logger = Logger.getLogger(ConfigStore.class);
	
	public static final String BASEPATH = "__BASEPATH__";
	
	private static final int UPDATE_FREQ = 30 * 60 * 1000;
	
	private static ConcurrentHashMap<String, String> memory = new ConcurrentHashMap<String, String>();
			
	public static String getConfig(String key) { return memory.get(key); }
	
	public static void putConfig(String param, String value) { memory.put(param, value); }
	
	public static boolean containConfig(String key) { return memory.containsKey(key); }

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		logger.error("ConfigStore: contextDestroyed() stop the reloadConfig timer now!");
		reloadConfig.cancel();
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		logger.error("Configstore: contextInitialized() loading config now!");
		new Timer().scheduleAtFixedRate(reloadConfig, 1000, UPDATE_FREQ);
	}
	
	private TimerTask reloadConfig = new TimerTask() {
		@Override
		public void run() {
			try {
				IConfigDAO configDAO = DAOFactory.getDefaultFactory().getConfigDAO();
				List<Config> l = configDAO.getAllConfigs();
				for(Config c : l) memory.put(c.getParam(), c.getValue());
				configDAO.close();
			} catch (Exception e) { logger.error("static got exception: ", e);}
		}
	}; 
	
}
