<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.imarketing.client.FAQClient"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<%@include file="includes/google_analytics.inc"%>

<title>KhaosatOnline - Câu hỏi thường gặp</title>

<script type='text/javascript' src='dwr/engine.js'></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script type='text/javascript' src='dwr/interface/FAQManagement.js'></script>
<script type='text/javascript' src='dwr/interface/UserManagement.js'></script>
<script type='text/javascript' src='dwr/interface/AdsManagement.js'></script>

<script type='text/javascript' src='js/utils/richtext/wysiwyg.js'></script>
<script type='text/javascript' src='js/utils/jquery.js'></script>
<script type='text/javascript' src='js/utils/msgbox/jquery.dragndrop.min.js'></script>
<script type='text/javascript' src='js/utils/msgbox/jquery.msgbox.js'></script>
<script type="text/javascript" src="js/utils/zopim.js"></script>

<script type='text/javascript' src='js/common/cookies.js'></script>
<script type='text/javascript' src='js/common/common.js'></script>
<script type='text/javascript' src='js/common/ads.js'></script>
<script type='text/javascript' src='js/faq.js'></script>

<link rel="stylesheet" href="css/common.css" type="text/css" />
<link rel="stylesheet" href="css/index.css" type="text/css" />

<script type="javascript">
<%
FAQClient faqClient = new FAQClient(request, response);
String listCategory = faqClient.getAllCategories();
%>
</script>


</head>

<body>
<table>
  <tr>
    <td width="15%" rowspan="6" bgcolor="#333333"><div class="leftads" id="leftads">Content for  class "leftads" id "leftads" Goes Here</div></td>
    <td height="143" rowspan="2" align="center" bgcolor="#D93911"><div class="toplogo" id="toplogo"> <h1 class="style6">KhaosatOnline</h1></div></td>
    <td height="135" bgcolor="#EAEAEA"><div class="topbanner" id="topbanner">Content for  class "topbanner" id "topbanner" Goes Here</div></td>
    <td width="15%" rowspan="6"><div class="rightads" id="rightads">Content for  class "rightads" id "rightads" Goes Here</div></td>
  </tr>
  <tr>
    <td height="30" bgcolor="#D93911"><div class="menutab" id="menutab"><ul><a href='index.jsp'>Trang chủ</a></ul></div></td>
  </tr>
  <tr>
     <td height="160" valign="top" bgcolor="#D93911"><h3 class="style2"><strong>Ba bước tham gia</strong>:</h3>
        <p class="style2">1. Đăng ký thành viên</p>
        <p class="style2">2. Tạo khảo sát MIỄN PHÍ</p>
        <p class="style2">3. Gửi khảo sát đến cho bạn bè</p>
     </td>
     <td width="51%" rowspan="3" valign="top" bgcolor="#EAEAEA">
        <div class="menubody" id="menubody">
            
            <!-- Should be FAQ Categories & FAQ here -->
            <table border="0">
                <tr>
                    <td valign='top' align='left'>
	                    <table border="0" cellspacing="0" cellpadding="0">
	                        <tr>
                                <td class="pageHeader">&nbsp;</td>
                            </tr>   
		                    <tr>
				                <td width="200" class="smallText" id="padding">
					                <ul>
					                    <%out.println(listCategory);%>
					                </ul>
				                    <br/>&nbsp;<br/>
				                </td>
			                </tr>
		                </table>
	                </td>
	                <td valign='top' align='left'>
	                   <table border="0" cellspacing="0" cellpadding="0" width="440">
				        <tr>
				            <td class="pageHeader">&nbsp;</td>
				        </tr>
				        <tr>
				            <td class="bodyText">
					            <p><div id="DetailsFAQ"></div></p>
					            <p><div id="FAQs"></div></p><br/>        
				            </td>
				        </tr>
					   </table>    
	                </td>
                </tr>
            </table>
            <!-- End of FAQs -->
            
            <div id='spinningwheel'></div>
        </div>
     </td>    
  </tr>
  <tr>
    <td height="244" valign="top" align="center" bgcolor="#D93911"><div class="signup" id="signup">
      <h2 class="style4">Đăng nhập</h2>
      <div class='signinform' id='AnotherLogin'>
          <table width="100%" height="128" border="0">
            <tr>
              <td width="40%"><span class="style2">Email:</span></td>
              <td width="60%"><input name="email_address" type="text" id="email_address" size="20" /></td>
            </tr>
            <tr>
              <td><span class="style2">Mật khẩu:</span></td>
              <td><input name="pswd" type="password" id="pswd" size="20" />&nbsp;</td>
            </tr>
            <tr>
              <td align="left"><input name="rememberme" type="checkbox"><span class="style2">Nhớ?</span> </td>
              <td alight="right"><input name="Submit" type="button" onClick="submitbttnonclick('AnotherLogin', 'email_address,pswd')" value="Submit" /></td>
            </tr>
          </table>
      </div>
      <p class="style3">&nbsp;</p>
      <p class="style3">&nbsp;</p>
      <p class="style3">&nbsp;</p>
    </div></td>
  </tr>
  
  <tr>
    <td height="228" colspan="2" bgcolor="#000000"><div class="footer" id="footer"><%@ include file="includes/footer.inc" %></div></td>
  </tr>
</table>
</body>
</html>
