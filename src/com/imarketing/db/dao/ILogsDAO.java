package com.imarketing.db.dao;

import com.imarketing.db.Logs;

public interface ILogsDAO extends ICommonDAO {
	public boolean insertLog(Logs log) throws Exception;
}
