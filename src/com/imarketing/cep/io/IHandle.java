package com.imarketing.cep.io;

public interface IHandle<T> {
	public void handle(T data);
}
