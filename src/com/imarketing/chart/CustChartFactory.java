package com.imarketing.chart;

public class CustChartFactory {
	
	public static final String BARSTACK_CHART = "BarStackChart";
	
	public static final String BAR_CHART = "BarChart";
	
	public static final String PIE_CHART = "PieChart";
	
}
