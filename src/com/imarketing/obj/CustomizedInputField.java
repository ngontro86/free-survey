package com.imarketing.obj;

import com.imarketing.common.Common;

public class CustomizedInputField implements Comparable<CustomizedInputField>{
	
	private int col;
	private int row;
	private String column;
	
	private FieldDetails fielddetails;

	public int getCol() {
		return col;
	}
	public void setCol(int col) {
		this.col = col;
	}
	public int getRow() {
		return row;
	}
	public void setRow(int row) {
		this.row = row;
	}
	public String getColumn() {
		return column;
	}
	public void setColumn(String column) {
		this.column = column;
	}
	public FieldDetails getFieldDetails() {
		return fielddetails;
	}
	public void setFieldDetails(FieldDetails fieldDetails) {
		this.fielddetails = fieldDetails;
	}
	
	@Override
	public String toString() {
		return String.format("col: %d, row: %d, column: %s, FieldDetails: %s", col, row, column, fielddetails.toString());
	}

	public String toHTMLString(String tableName, String compulsoryFields) {
		if(fielddetails.getType().equals(Common.TYPE_BUTTON)) {
			return String.format("<tr><td></td><td><input type='button' value='%s' id='%s' name='%s' onclick='submitbttnonclick(\"%s\", \"%s\")'></td></tr>", fielddetails.getName(), column, fielddetails.getFieldvalues(), tableName, compulsoryFields);
		}
		String ret = "";
		ret += String.format("<tr><td align='right'><b>%s</b></td>", (fielddetails.getName().replaceAll("N!L", "<br/>") + (fielddetails.getMandatory() == 1? "<font color='red'>*</font>" : "")));
		String type = fielddetails.getType();
		if(type.equals(Common.TYPE_CHECKBOX)) {
			ret += String.format("<td><input type='%s' name='%s' id='%s' value='%s'></td></tr>", type, column, column, fielddetails.getFieldvalues());
		} else if(type.equals(Common.TYPE_INPUTBOX)) {
			ret += String.format("<td><input type='text' name='%s' id='%s' value='%s' size=%d></td></tr>", column, column, fielddetails.getFieldvalues(), fielddetails.getLength());
		} else if(type.equals(Common.TYPE_TEXTBOX)) {
			ret += String.format("<td><input type='text' name='%s' id='%s' value='%s' size=%d></td></tr>", column, column, fielddetails.getFieldvalues(), fielddetails.getLength());
		} else if(type.equals(Common.TYPE_TEXTAREA)) {
			ret += String.format("<td><textarea rows='%d' cols='%d' name='%s' id='%s' value='%s'></textarea></td></tr>", fielddetails.getRow(), fielddetails.getColumn(), column, column, fielddetails.getFieldvalues());
		} else if(type.equals(Common.TYPE_INPUTBOX_PASSWORD)) {
			ret += String.format("<td><input type='password' name='%s' id='%s' value='%s' size=%d></td></tr>", column, column, fielddetails.getFieldvalues(), fielddetails.getLength());
		} else if(type.equals(Common.TYPE_INPUTBOX_DATE)) {
			// Need to implement again
			ret += String.format("<td><input type='text' name='%s' id='%s' value='%s'  size=%d></td></tr>", column, column, fielddetails.getFieldvalues(), fielddetails.getLength());
		} else if(type.equals(Common.TYPE_PICKLIST_SINGLE)) {
			String optionHtml = "";
			for(String option : fielddetails.getFieldvalues().split(";[ ]*")) optionHtml += String.format("<option value='%s'>%s</option>", option, option);
			ret += String.format("<td><select name='%s' id='%s'>%s</select></td></tr>", column, column, optionHtml);
		} else if(type.equals(Common.TYPE_PICKLIST_MULTIPLE)){
			String optionHtml = "";
			for(String option : fielddetails.getFieldvalues().split(";[ ]*")) optionHtml += String.format("<option value='%s'>%s</option>", option, option);
			ret += String.format("<td><select name = '%s' multiple='yes' size=%d>%s</select></td></tr>", column, fielddetails.getFieldvalues().split(";[ ]*").length, optionHtml);
		} 
		return ret;
	}
	
	public String toHTMLWithFixedValueString(String tableName, String compulsoryFields, Object fixedValue) {
		if(fielddetails.getType().equals(Common.TYPE_BUTTON)) {
			return String.format("<tr><td></td><td><input type='button' value='%s' id='%s' name='%s' onclick='submitbttnonclick(\"%s\", \"%s\")'></td></tr>", fielddetails.getName(), column, fielddetails.getFieldvalues(), tableName, compulsoryFields);
		}
		String ret = "";
		ret += String.format("<tr><td align='right'><b>%s</b></td>", (fielddetails.getName().replaceAll("N!L", "<br/>") + (fielddetails.getMandatory() == 1? "<font color='red'>*</font>" : "")));
		String type = fielddetails.getType();
		if(type.equals(Common.TYPE_CHECKBOX)) {
			ret += String.format("<td><input type='%s' name='%s' id='%s' value='%s'></td></tr>", type, column, column, fixedValue.equals("")? "1" : fixedValue.toString());
		} else if(type.equals(Common.TYPE_INPUTBOX)) {
			ret += String.format("<td><input type='text' name='%s' id='%s' value='%s' size=%d></td></tr>", column, column, fixedValue.toString(), fielddetails.getLength());
		} else if(type.equals(Common.TYPE_TEXTBOX)) {
			ret += String.format("<td><input type='text' name='%s' id='%s' value='%s' size=%d></td></tr>", column, column, fixedValue.toString(), fielddetails.getLength());
		} else if(type.equals(Common.TYPE_TEXTAREA)) {
			ret += String.format("<td><textarea rows='%d' cols='%d' name='%s' id='%s' value='%s'>%s</textarea></td></tr>", fielddetails.getRow(), fielddetails.getColumn(), column, column, fielddetails.getFieldvalues(), fixedValue.toString());
		} else if(type.equals(Common.TYPE_INPUTBOX_PASSWORD)) {
			ret += String.format("<td><input type='password' name='%s' id='%s' value='%s' size=%d></td></tr>", column, column, fixedValue.toString(), fielddetails.getLength());
		} else if(type.equals(Common.TYPE_INPUTBOX_DATE)) {
			// Need to implement again
			ret += String.format("<td><input type='text' name='%s' id='%s' value='%s'  size=%d></td></tr>", column, column, fixedValue.toString(), fielddetails.getLength());
		} else if(type.equals(Common.TYPE_PICKLIST_SINGLE)) {
			String optionHtml = "";
			for(String option : fielddetails.getFieldvalues().split(";[ ]*")) optionHtml += String.format("<option value='%s'>%s</option>", option, option);
			ret += String.format("<td><select name='%s' id='%s'>%s</select></td></tr>", column, column, optionHtml);
		} else if(type.equals(Common.TYPE_PICKLIST_MULTIPLE)){
			String optionHtml = "";
			for(String option : fielddetails.getFieldvalues().split(";[ ]*")) optionHtml += String.format("<option value='%s'>%s</option>", option, option);
			ret += String.format("<td><select name = '%s' multiple='yes' size=%d>%s</select></td></tr>", column, fielddetails.getFieldvalues().split(";[ ]*").length, optionHtml);
		} 
		return ret;
	}
	
	@Override
	public int compareTo(CustomizedInputField o) {
		if(o == null) return 1;
		if(this.getRow() > o.getRow()) return 1;
		if(this.getRow() < o.getRow()) return -1;
		if(this.getCol() > o.getCol()) return 1;
		if(this.getCol() < o.getCol()) return -1;
		return this.getColumn().compareTo(o.getColumn());
	}
	
	
}
