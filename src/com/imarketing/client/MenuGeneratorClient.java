package com.imarketing.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import com.imarketing.common.Common;
import com.imarketing.conf.xml.SimpleXMLFileParserFactory;
import com.imarketing.db.AccessibleMenu;
import com.imarketing.db.User;
import com.imarketing.db.dao.DAOFactory;
import com.imarketing.db.dao.IAccessibleMenuDAO;
import com.imarketing.db.dao.IUserDAO;
import com.imarketing.dbwriter.DBWriterListener;
import com.imarketing.obj.CustomizedInputTable;

public class MenuGeneratorClient extends AbstractClient {
	private Logger logger = Logger.getLogger(MenuGeneratorClient.class);
	private HttpSession session;
	
	public MenuGeneratorClient(HttpServletRequest req, HttpServletResponse res) {
		super(req, res);
		this.session = this.getRequest().getSession();
	}

	private String oneAccessMenu(AccessibleMenu am){
		return String.format("<img onclick='return displayMenu(\"%s\");' src='%s' alt='%s' title='%s'/>",am.getLabel(), am.getImgsrc(), am.getLabel(), am.getLabel());
	}

	@SuppressWarnings("unchecked")
	public String getMenuTabHTML(){
		String ret = "<div id='dock'>";
		try {
			Long menuId = (Long)session.getAttribute(Common.HTTPSESSION_CURRENT_MENU_ID);
			if(menuId == null) { logger.error("The menu id has not specified yet!"); return ret; }
			IAccessibleMenuDAO menuDao = DAOFactory.getDefaultFactory().getAccessibleMenuDAO();
			AccessibleMenu aMenu = null;
			if(session.getAttribute(Common.HTTPSESSION_USER_ATTRIBUTION) instanceof User) { 
				User user = (User)session.getAttribute(Common.HTTPSESSION_USER_ATTRIBUTION);
				IUserDAO userDAO = DAOFactory.getDefaultFactory().getUserDAO();
				user = userDAO.getUser(user.getId());
				aMenu = menuDao.getAccessibleMenu(menuId);
				List<AccessibleMenu> accessibleMenu = new ArrayList<AccessibleMenu>(user.getRolegroup().getAccessiblemenus());
				if(!accessibleMenu.contains(aMenu)) {
					logger.error("No access to this page: " + aMenu.getLink()); 
					menuDao.close(); userDAO.close();
					return ret; 
				}
				userDAO.close();
			} else if(menuId.longValue() == Common.DEFAULT_PAGE_WITHOUT_AUTHENTICATION) { // No user --- the index page only
				logger.debug("Use the default page without authentication. MenuId: " + menuId);
				aMenu = menuDao.getAccessibleMenu(menuId);	
			} else {
				logger.error("No access. Please leave us alone!");
				menuDao.close();
				return ret;
			}
			if(ClientStoreFactory.getInstance().containsKey(String.format("%s:%d", MenuGeneratorClient.class.toString(), aMenu.getId()))) { 
				menuDao.close();
				return ClientStoreFactory.getInstance().getCachedString(String.format("%s:%d", MenuGeneratorClient.class.toString(), aMenu.getId())); 
			}

			ret += oneAccessMenu(aMenu);

			// sort menu by menu id
			Set<AccessibleMenu> aMenus = aMenu.getSubmenus();
			List<AccessibleMenu> accessibleMenus = new ArrayList<AccessibleMenu>(aMenus);
			Collections.sort(accessibleMenus);
			// end sort menu by menu id
			
			for(AccessibleMenu am : accessibleMenus){
				ret += String.format("%s", oneAccessMenu(am));
			}
			ret += "</div>";
			ClientStoreFactory.getInstance().cacheObject(String.format("%s:%d", MenuGeneratorClient.class.toString(), aMenu.getId()), ret);
			menuDao.close(); 
			return ret;

		} catch (Exception e){
			logger.error("getMenuTabHTML() got exception: ", e);
			DBWriterListener.writeLogs("getMenuTabHTML() got exception: " +e.getMessage());
			return "";
		}
	}

	public String getAccessibleMenu(long id) {
		IAccessibleMenuDAO menuDao = DAOFactory.getDefaultFactory().getAccessibleMenuDAO();
		AccessibleMenu aMenu = null;
		String ret = "";
		try {
			aMenu = menuDao.getAccessibleMenu(id);
			ret = oneAccessMenu(aMenu);
			menuDao.close();
			return ret;
		} catch (Exception e) {
			logger.error("No access. Please leave us alone!");
			DBWriterListener.writeLogs("getAccessibleMenu() No access. Please leave us alone!" +e.getMessage());
			menuDao.close();
		}
		return "";
	}

	public String getHTMLForm(String tableName){
		try {
			if(ClientStoreFactory.getInstance().cached(String.format("%s:%s", MenuGeneratorClient.class.toString(), tableName))) {
				return ClientStoreFactory.getInstance().getCachedString(String.format("%s:%s", MenuGeneratorClient.class.toString(), tableName)).toString();
			}
			String ret = "";
			CustomizedInputTable table = SimpleXMLFileParserFactory.getCustomizedInputTable(tableName);
			ret = table.toHTMLString();
			ClientStoreFactory.getInstance().cacheObject(String.format("%s:%s", MenuGeneratorClient.class.toString(), tableName), ret);
			return ret;
		} catch (Exception e){
			logger.error("getHTMLForm() got exception: ", e);
			DBWriterListener.writeLogs("getHTMLForm() No access. Please leave us alone!" +e.getMessage());
			return "";
		}
	}
}
