package com.imarketing.db.dao.mysql;

import java.util.List;

import org.hibernate.Query;

import com.imarketing.db.Ads;
import com.imarketing.db.dao.IAdsDAO;

public class AdsMySQLDAO extends CommonMySQLDAO implements IAdsDAO {

	@Override
	public Ads getAdsById(long id) throws Exception {
		beginTransaction();
		return (Ads)session.get(Ads.class, new Long(id));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Ads> getAllAds() throws Exception {
		beginTransaction();
		Query q = session.createQuery("from Ads");
		return (List<Ads>)q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Ads getAdsByProvider(String provider) throws Exception {
		beginTransaction();
		Query q = session.createQuery("from Ads where provider = :provider");
		q.setString("provider", provider);
		List<Ads> l = (List<Ads>)q.list();
		return (l != null && !l.isEmpty())? l.get(0) : null;
	}
}
