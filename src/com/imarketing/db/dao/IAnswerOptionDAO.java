package com.imarketing.db.dao;

import com.imarketing.db.AnswerOption;

public interface IAnswerOptionDAO extends ICommonDAO {
	
	public boolean insertOrUpdateAnswerOption(AnswerOption ao) throws Exception;
	
	public AnswerOption getAnswerOptionById(long answerId) throws Exception;

}
