package com.imarketing.db;

public class Poll {
	
	private long id;
	private String uid;
	private String status;
	private String title;
	private String introduction;
	private String email;
	private long responsecount;
	
	private boolean publicresult;
	private boolean prevent_duplicateresponse;
	
	private User user;
	private Question question;
	
	private long created_timestamp;
	private long lastupdated_timestamp;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getResponsecount() {
		return responsecount;
	}
	public void setResponsecount(long responsecount) {
		this.responsecount = responsecount;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	public long getCreated_timestamp() {
		return created_timestamp;
	}
	public void setCreated_timestamp(long created_timestamp) {
		this.created_timestamp = created_timestamp;
	}
	public long getLastupdated_timestamp() {
		return lastupdated_timestamp;
	}
	public void setLastupdated_timestamp(long lastupdated_timestamp) {
		this.lastupdated_timestamp = lastupdated_timestamp;
	}
	public boolean isPrevent_duplicateresponse() {
		return prevent_duplicateresponse;
	}
	public void setPrevent_duplicateresponse(boolean prevent_duplicateresponse) {
		this.prevent_duplicateresponse = prevent_duplicateresponse;
	}
	public boolean isPublicresult() {
		return publicresult;
	}
	public void setPublicresult(boolean publicresult) {
		this.publicresult = publicresult;
	}
	
}
