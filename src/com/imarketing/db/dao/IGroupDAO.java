package com.imarketing.db.dao;

import com.imarketing.db.Group;

public interface IGroupDAO extends ICommonDAO {
	
	public Group getGroup(long id) throws Exception;
	
	public Group getGroup(String groupName) throws Exception;
	
	public boolean insertGroup(Group g) throws Exception;
}
