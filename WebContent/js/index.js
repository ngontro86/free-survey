
var props = new Object();
props.surveyRecordPerPage=15;
props.maxPageCount=5;
props.currentPage=1;
props.maxSurveyId=-1;

$(document).ready(function() {
    constructSurveyPagination();
    displayAds();
    jQuery('#dock').jqDock();    
});

function constructSurveyPagination() {
    var respBack = function(data){
        props.totalSurvey = data.totalSurvey;
        props.listEndingId = data.listEndingId;
        displayMenu("Khảo sát mới nhất");
    }
    SurveyManagement.getGenericSurveyPagination(props.surveyRecordPerPage, respBack);
}
function buildSurveyPageNavigation() {
    var html = "<div id='genericSearchPagination'><ul>";
    html +=props.currentPage > 1? "<li class='previousPage'><a href='#' onclick='return loadPrevPagination();'>Trang trước</a></li>" : "";
    for(var i= props.currentPage-5; i < props.currentPage + props.maxPageCount;i++) {
        if(i == props.currentPage && i < Math.round(props.totalSurvey/props.surveyRecordPerPage + 1)) html += "<li class='current'>"+i+"</li>";
        else if(i > 0 && i < Math.round(props.totalSurvey/props.surveyRecordPerPage + 1)) html += "<li><a class='paginationBlock' href='#' onclick='return loadPagination("+i+");'>"+i+"</a></li>";
    }
    html += props.currentPage < props.totalSurvey/props.surveyRecordPerPage? "<li class='nextPage'><a href='#' onclick='return loadNextPagination();'>Trang tiếp</a></li>" : "";
    html += "</ul></div>";
    return html;
}

function loadPrevPagination(){loadPagination(props.currentPage-1);}
function loadNextPagination(){loadPagination(props.currentPage+1);}
function loadPagination(pageId) {
    props.currentPage = pageId;
    props.maxSurveyId = props.listEndingId[pageId-1] +1 ; 
    displayMenu("Khảo sát mới nhất");
}

function displayMenu(name) {
    $('#signin').hide(); $('#registration').hide(); $('#availsurveys').hide();
    
    if(name == "Đăng nhập") { $('#signin').toggle(); }
    if(name == "Đăng ký") { $('#registration').toggle(); }
    if(name == "Tài khoản cá nhân") { stopUpdateAds(); window.location="user.jsp"; }
    if(name == "Câu hỏi thường gặp") { stopUpdateAds(); window.location="faq.jsp";}
    if(name == "Khảo sát mới nhất") {
        var respBack = function(data) {
            $("#spinningwheel").html(''); 
            if(data.success == 'true') {
                $('#availsurveys').html(data.msg + buildSurveyPageNavigation()); 
                $('#availsurveys').toggle();
            }
        }
        $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
        SurveyManagement.getAllAvailableSurveys(props.surveyRecordPerPage, props.maxSurveyId, respBack);
    }   
}

// Need to implement the function for each submit button in every auto-generated form
function checkEmail() {
	var email = document.getElementById("username_emailaddress").value;
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if (!filter.test(email)) {
	   email.focus;
	return false;
	}
}

function checkUserExist(data) {
    if(data.success=="true") {
        alert(data.message);
        window.location="index.jsp";
    } else {
        alert(data.message);
    }
}

