package com.imarketing.db;

import java.util.HashSet;
import java.util.Set;

public class Participant {
	private long id;
	private String iporphone;
	private long timestamp;
	
	@SuppressWarnings("unchecked")
	private Set responses = new HashSet(); // Suppose to have 1 response one time

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getIporphone() {
		return iporphone;
	}

	public void setIporphone(String iporphone) {
		this.iporphone = iporphone;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	@SuppressWarnings("unchecked")
	public Set getResponses() {
		return responses;
	}

	@SuppressWarnings("unchecked")
	public void setResponses(Set responses) {
		this.responses = responses;
	}
	
}
