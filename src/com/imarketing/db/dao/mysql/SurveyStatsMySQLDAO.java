package com.imarketing.db.dao.mysql;

import java.util.List;

import org.hibernate.Query;

import com.imarketing.db.Survey;
import com.imarketing.db.SurveyStats;
import com.imarketing.db.dao.ISurveyStatsDAO;

public class SurveyStatsMySQLDAO extends CommonMySQLDAO implements
		ISurveyStatsDAO {

	@SuppressWarnings("unchecked")
	@Override
	public SurveyStats getStatsBySurvey(Survey survey) throws Exception {
		beginTransaction();
		Query q = session.createQuery("from SurveyStats where survey=:surv");
		q.setEntity("surv", survey);
		List<SurveyStats> l = q.list();
		return (l == null || l.isEmpty())? null : l.get(0);
	}

	@Override
	public boolean insertOrUpdateSurveyStats(SurveyStats stats)
			throws Exception {
		beginTransaction();
		session.saveOrUpdate(stats);
		flush();
		return true;
	}

	@Override
	public boolean deleteSurveyStats(SurveyStats stats) throws Exception {
		beginTransaction();
		session.delete(stats);
		flush();
		return true;
	}

}
