package com.imarketing.db.dao.mysql;

import com.imarketing.db.SurveyPage;
import com.imarketing.db.dao.ISurveyPageDAO;

public class SurveyPageMySQLDAO extends CommonMySQLDAO implements ISurveyPageDAO {

	@Override
	public boolean insertSurveyPage(SurveyPage surveyPage) throws Exception {
		beginTransaction();
		session.saveOrUpdate(surveyPage);
		flush();
		return true;
	}

	@Override
	public SurveyPage getSurveyPage(long id) throws Exception {
		beginTransaction();
		return (SurveyPage)session.get(SurveyPage.class, new Long(id));
	}

	@Override
	public SurveyPage updateSurveyPage(SurveyPage spage) throws Exception {
		beginTransaction();
		session.update(spage);
		flush();
		return getSurveyPage(spage.getId());
	}

}
