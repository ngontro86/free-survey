package com.imarketing.dbwriter;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.imarketing.db.Logs;
import com.imarketing.db.dao.DAOFactory;
import com.imarketing.db.dao.ILogsDAO;

import com.imarketing.time.GlobalTime;

public class DBWriterListener implements ServletContextListener, Runnable {

	private static Logger logger = Logger.getLogger(DBWriterListener.class);
	
	private static BlockingQueue<Object> q = new LinkedBlockingQueue<Object>(5000);
	
	private ILogsDAO logDAO;
	private static final String POISONPILL = "poisonpill";	
	private volatile boolean stop = false;
		
	public static void writeLogs(String msg) {
		try {
			Logs log = new Logs();
			log.setEvent(msg);
			log.setTimestamp(GlobalTime.currentTimeSeconds());
			q.offer(log);
		} catch (Exception e){logger.error("writeLogs() got exception: ", e); }
	}
	
	public static void writeObject(Object o) { try { q.offer(o); } catch(Exception e){logger.error("writeObject() got exception: ", e);}}
	
	private void processObject(Object o) {
		try { if(o instanceof Logs) {logDAO.insertLog((Logs)o);}
		} catch (Exception e){ logger.error("processObject() got exception: ", e); }
	}
	
	@Override
	public void run() {
		while(!stop) {
			try {
				Object obj = q.take();
				if(POISONPILL.equals(obj)) break;
				processObject(obj);
			} catch (Exception e){ logger.error("run() got exception: ", e); }
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) { 
		try {
			this.stop = true; 
			q.offer(POISONPILL);
			logger.info("contextDestroyed, stop the DBWriter thread after the q draining now!");
			List<Object> remaining = new LinkedList<Object>();
			q.drainTo(remaining);
			for(Object o : remaining) processObject(o);
			logDAO.close();
		} catch (Exception e){ logger.error("contextDestroyed() got exception: ", e); }
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		logger.error("contextInitialized, start the DBWriter thread now!");
		logDAO = DAOFactory.getDefaultFactory().getLogsDAO();
		new Thread(this, "DBWriterListener").start();
	}
}
