package com.imarketing.db.dao.mysql;

import com.imarketing.db.AccessibleMenu;
import com.imarketing.db.dao.IAccessibleMenuDAO;

public class AccessibleMenuMySQLDAO extends CommonMySQLDAO implements IAccessibleMenuDAO {

	@Override
	public AccessibleMenu getAccessibleMenu(long id) throws Exception {
		beginTransaction();
		return (AccessibleMenu)session.get(AccessibleMenu.class, new Long(id));
	}

	@Override
	public boolean insertAccessibleMenu(AccessibleMenu am) throws Exception {
		beginTransaction();
		session.saveOrUpdate(am);
		session.flush();
		return true;
	}
}
