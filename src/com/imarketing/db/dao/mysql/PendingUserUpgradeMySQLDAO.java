package com.imarketing.db.dao.mysql;

import java.util.List;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.imarketing.common.Common;
import com.imarketing.db.PendingUserUpgrade;
import com.imarketing.db.dao.IPendingUserUpgradeDAO;

public class PendingUserUpgradeMySQLDAO extends CommonMySQLDAO implements
		IPendingUserUpgradeDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<PendingUserUpgrade> getAllPendingUserUpgrade() throws Exception {
		beginTransaction();
		return session.createCriteria(PendingUserUpgrade.class)
		.add(Restrictions.eq("status", Common.UPGRADE_STATUS_UNDERPROCESSING))
		.addOrder(Order.desc("id"))
		.list();
	}

	@Override
	public PendingUserUpgrade getPendingUserUpgradeById(long id)
			throws Exception {
		beginTransaction();
		return (PendingUserUpgrade)session.get(PendingUserUpgrade.class, new Long(id));
	}

}
