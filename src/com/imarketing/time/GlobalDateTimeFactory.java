package com.imarketing.time;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;


public class GlobalDateTimeFactory extends HashMap<String, SimpleDateFormat> {
	private static final long serialVersionUID = 1L;
	private Object key = new Object();
	private static GlobalDateTimeFactory instance = new GlobalDateTimeFactory();
	public String format(String dateYYYYMMdd, String displayFormat) throws Exception {
		synchronized (key) {
			SimpleDateFormat format1 = getSimpleDateFormatInstance("yyyyMMdd");
			SimpleDateFormat format2 = getSimpleDateFormatInstance(displayFormat);
			return format2.format(format1.parse(dateYYYYMMdd));
		}
	}
	
	public String format(long timeSeconds, String displayFormat) throws Exception {
		synchronized (key) {
			Date d = GlobalTime.getDate(timeSeconds);
			SimpleDateFormat f = getSimpleDateFormatInstance(displayFormat);
			return f.format(d);
		}
	}
	
	private SimpleDateFormat getSimpleDateFormatInstance(String format) {
		if(containsKey(format)) { return get(format); } 
		SimpleDateFormat newInstance = new SimpleDateFormat(format);
		put(format, newInstance);
		return newInstance;
	}
	
	public static GlobalDateTimeFactory getInstance(){ return instance; }
}
