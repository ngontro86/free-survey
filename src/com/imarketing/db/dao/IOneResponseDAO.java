package com.imarketing.db.dao;

import com.imarketing.db.OneResponse;

public interface IOneResponseDAO extends ICommonDAO {

	public boolean insertOrUpdateSurveyResponse(OneResponse response);
}
