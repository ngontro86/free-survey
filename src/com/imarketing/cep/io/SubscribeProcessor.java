package com.imarketing.cep.io;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.imarketing.cep.engine.CepEngine;
import com.imarketing.cep.util.ObjectEvent;
import com.imarketing.cep.util.ProcSubscribe;
import com.imarketing.cep.util.SocketData;

public class SubscribeProcessor extends AbstractProcessor<Object, ObjectEvent> {
	
	private Logger logger = Logger.getLogger(SubscribeProcessor.class);
	
	public SubscribeProcessor(CepEngine e) { name = "SubscribeProcessor"; engine = e; }
	
	@Override
	public void processOneData(OneData one) {
		try {
			logger.debug("SubscribeProcessor: " + one.data.getData());
			SocketData<Object> data = one.data;
			ProcSubscribe subscribeObj = (ProcSubscribe)data.getData();
			IHandle<SocketData<ObjectEvent>> handler = one.handler;
			String query = subscribeObj.query, eventID = subscribeObj.eventID;
			engine.registerSubscriber(query, new OneSubscriber(eventID, handler));
		} catch (Exception e){ logger.error("processOneData() got exception: ", e); }
	}
	
	@SuppressWarnings("unchecked")
	class OneSubscriber implements Runnable {
		private BlockingQueue<Map> q = new LinkedBlockingQueue<Map>();
		public String eventID;
		public IHandle<SocketData<ObjectEvent>> handler;
		public OneSubscriber(String eventId, IHandle<SocketData<ObjectEvent>> h){ this.eventID = eventId; this.handler = h;}
		
		public void update(Map row) {
			try { q.put(row); } catch (Exception e){}
		}

		@Override
		public void run() {
			while(true) {
				try {
					Map one = q.take();
					logger.error("row: " + one.toString());
				} catch (Exception e){}
			}
		}
	}
}
