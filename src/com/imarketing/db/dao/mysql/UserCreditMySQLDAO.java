package com.imarketing.db.dao.mysql;

import org.hibernate.Query;

import com.imarketing.db.UserCredit;
import com.imarketing.db.dao.IUserCreditDAO;

public class UserCreditMySQLDAO extends CommonMySQLDAO implements IUserCreditDAO {

	public UserCreditMySQLDAO() {
		super();
	}
	
	@Override
	public boolean insertUserCredit(UserCredit uc) throws Exception {
		beginTransaction();
		session.save(uc);
		session.flush();
		return true;
	}

	@Override
	public UserCredit getUserCredit(long id) throws Exception {
		beginTransaction();
		return (UserCredit)session.get(UserCredit.class, new Long(id));
	}

	@Override
	public UserCredit getUserCredit(String reason) throws Exception {
		beginTransaction();
		Query query = session.createQuery("from UserCredit where reason = :reason");
		query.setString("reason", reason);
		return query.list().size() > 0 ? (UserCredit)query.list().get(0) : null;
	}
	
}
