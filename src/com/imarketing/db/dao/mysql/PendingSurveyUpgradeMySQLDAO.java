package com.imarketing.db.dao.mysql;

import java.util.List;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.imarketing.common.Common;
import com.imarketing.db.PendingSurveyUpgrade;
import com.imarketing.db.dao.IPendingSurveyUpgradeDAO;

public class PendingSurveyUpgradeMySQLDAO extends CommonMySQLDAO implements
		IPendingSurveyUpgradeDAO {

	@Override
	public PendingSurveyUpgrade getPendingSurveyUpgradeById(long id)
			throws Exception {
		beginTransaction();
		return (PendingSurveyUpgrade)session.get(PendingSurveyUpgrade.class, new Long(id));
	}

	@Override
	public boolean insertPendingSurveyUpgrade(PendingSurveyUpgrade psu)
			throws Exception {
		beginTransaction();
		session.save(psu);
		flush();
		return true;
	}

	@Override
	public boolean updatePendingSurveyUpgrade(PendingSurveyUpgrade psu)
			throws Exception {
		beginTransaction();
		session.update(psu);
		flush();
		return true;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PendingSurveyUpgrade> getAllPendingRequest(int limit, long startingId)
			throws Exception {
		beginTransaction();
		return session.createCriteria(PendingSurveyUpgrade.class)
				.add(Restrictions.eq("status", Common.UPGRADE_STATUS_UNDERPROCESSING))
				.add(Restrictions.gt("id", new Long(startingId)))
				.addOrder(Order.desc("id"))
				.setMaxResults(limit)
				.list();
	}

	@Override
	public boolean updateSurvey(PendingSurveyUpgrade psu) throws Exception {
		beginTransaction();
		session.update(psu.getSurvey());
		flush();
		return true;
	}

}
