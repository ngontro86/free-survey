package com.imarketing.db.dao.mysql;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.imarketing.common.Common;
import com.imarketing.db.Survey;
import com.imarketing.db.dao.ISurveyDAO;

public class SurveyMySQLDAO extends CommonMySQLDAO implements ISurveyDAO {
	
	@SuppressWarnings("unchecked")
	public Survey insertSurvey(Survey s) throws Exception {
		beginTransaction();
		session.save(s);
		flush();
		return s;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Survey> getAllSurveysPublished() throws Exception {
		beginTransaction();
		return session.createCriteria(Survey.class)
		.add(Restrictions.eq("displaysurveystatus", Common.SURVEYPOLL_STATUS_LAUNCHED))
		.addOrder(Order.desc("id"))
		.list();
	}

	@Override
	public Survey getSurveyById(long id) throws Exception {
		beginTransaction();
		return(Survey) session.get(Survey.class, new Long(id));
	}

	@SuppressWarnings("unchecked")
	@Override
	public Survey getSurveyByUID(String uid) throws Exception {
		beginTransaction();
		Query q = session.createQuery("from Survey where uid = :uid");
		q.setString("uid", uid);
		List<Survey> l = q.list();
		return (l == null || l.isEmpty())? null : l.get(0);
	}

	@Override
	public Survey updateSurvey(Survey s) throws Exception {
		beginTransaction();
		session.update(s); // Not merge
		flush();
		return s;
	}

	@Override
	public boolean deleteSurvey(String uid) throws Exception {
		Survey s = getSurveyByUID(uid);
		return deleteSurvey(s);
	}

	@Override
	public boolean deleteSurvey(Survey s) throws Exception {
		beginTransaction();
		session.delete(s);
		flush();
		return true;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Survey> getLastPublishedSurveys(int limit, long endingId) throws Exception {
		beginTransaction();
		return session.createCriteria(Survey.class)
				.add(Restrictions.eq("displaysurveystatus", Common.SURVEYPOLL_STATUS_LAUNCHED))
				.add(Restrictions.lt("id", new Long(endingId >= 0? endingId : Long.MAX_VALUE)))
				.addOrder(Order.desc("id"))
				.setMaxResults(limit)
				.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Survey> getValidSurveys(int limit, long startingId)
			throws Exception {
		beginTransaction();
		return session.createCriteria(Survey.class)
				.add(Restrictions.ne("displaysurveystatus", Common.SURVEYPOLL_STATUS_EXPIRED))
				.add(Restrictions.gt("id", new Long(startingId)))
				.addOrder(Order.asc("id"))
				.setMaxResults(limit)
				.list();
	}

}
