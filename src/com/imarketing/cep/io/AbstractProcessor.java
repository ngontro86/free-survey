package com.imarketing.cep.io;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.imarketing.cep.engine.CepEngine;
import com.imarketing.cep.util.SocketData;

public abstract class AbstractProcessor<RECV, SEND> implements IProcessor<RECV, SEND>, Runnable {
	
	private Logger logger = Logger.getLogger(AbstractProcessor.class);
	protected CepEngine engine;
	protected String name;
	protected long ok = 0, error = 0;
	
	private BlockingQueue<OneData> q = new LinkedBlockingQueue<OneData>();
	
	@Override
	public void process(SocketData<RECV> t, IHandle<SocketData<SEND>> handler) {
		try {
			q.put(new OneData(t, handler));
		} catch (InterruptedException e) { logger.error("process() got exception: ", e); }
	}
	
	protected abstract void processOneData(OneData one);
	
	@Override
	public void run() {
		while(true) {
			try {
				OneData oneData = q.take();
				processOneData(oneData);
			} catch (Exception e) { logger.error("run() got exception: ", e); error++;}
		}
	}
	
	public class OneData {
		SocketData<RECV> data;
		IHandle<SocketData<SEND>> handler;
		public OneData(SocketData<RECV> d, IHandle<SocketData<SEND>> h) { data = d; handler = h; }
	}
	
}
