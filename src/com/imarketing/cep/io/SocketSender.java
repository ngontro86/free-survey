package com.imarketing.cep.io;

import com.imarketing.cep.util.SocketData;

public class SocketSender<SEND> implements IHandle<SocketData<SEND>> {

	private SocketListener<?, SEND> socket;
	
	public SocketSender(SocketListener<?, SEND> socket) { this.socket = socket;}
	
	@Override
	public void handle(SocketData<SEND> data) {
		socket.publish(data.getConnectionId(), data.getData());
	}
}
