package com.imarketing.db.dao.mysql;

import java.util.List;

import org.hibernate.Query;

import com.imarketing.db.ServiceTypeDef;
import com.imarketing.db.dao.IServiceTypeDefDAO;

public class ServiceTypeDefMySQLDAO extends CommonMySQLDAO implements
		IServiceTypeDefDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<ServiceTypeDef> loadServiceTypeDef() throws Exception {
		beginTransaction();
		Query q = session.createQuery("from ServiceTypeDef");
		return (List<ServiceTypeDef>)q.list();
	}

}
