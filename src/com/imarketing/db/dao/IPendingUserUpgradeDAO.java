package com.imarketing.db.dao;

import java.util.List;

import com.imarketing.db.PendingUserUpgrade;

public interface IPendingUserUpgradeDAO extends ICommonDAO {
	
	public PendingUserUpgrade getPendingUserUpgradeById(long id) throws Exception;

	public List<PendingUserUpgrade> getAllPendingUserUpgrade() throws Exception;
	
}
