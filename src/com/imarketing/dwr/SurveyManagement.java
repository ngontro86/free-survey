package com.imarketing.dwr;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.directwebremoting.WebContextFactory;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.MultiplePiePlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.ui.RectangleInsets;
import org.jfree.util.TableOrder;

import com.imarketing.ads.AdsListenerFactory;
import com.imarketing.chart.CustChartFactory;
import com.imarketing.common.Common;
import com.imarketing.conf.ConfigStore;
import com.imarketing.conf.xml.SimpleXMLFileParserFactory;
import com.imarketing.db.Ads;
import com.imarketing.db.AnswerOption;
import com.imarketing.db.AnswerOptionStats;
import com.imarketing.db.PendingSurveyUpgrade;
import com.imarketing.db.Question;
import com.imarketing.db.QuestionStats;
import com.imarketing.db.Survey;
import com.imarketing.db.SurveyPage;
import com.imarketing.db.SurveyStats;
import com.imarketing.db.User;
import com.imarketing.db.dao.DAOFactory;
import com.imarketing.db.dao.IAnswerOptionDAO;
import com.imarketing.db.dao.IPendingSurveyUpgradeDAO;
import com.imarketing.db.dao.IQuestionDAO;
import com.imarketing.db.dao.IQuestionStatsDAO;
import com.imarketing.db.dao.ISurveyDAO;
import com.imarketing.db.dao.ISurveyPageDAO;
import com.imarketing.db.dao.ISurveyStatsDAO;
import com.imarketing.db.dao.IUserDAO;
import com.imarketing.dbwriter.DBWriterListener;
import com.imarketing.email.EmailListener;
import com.imarketing.email.EmailTask;
import com.imarketing.obj.CustomizedInputTable;
import com.imarketing.servicedef.ServiceDefStore;
import com.imarketing.time.GlobalDateTimeFactory;
import com.imarketing.time.GlobalTime;

public class SurveyManagement extends AbstractManagement {
	private Logger logger = Logger.getLogger(SurveyManagement.class);
	public SurveyManagement() throws Exception { 
		super(); 
		setRequest(WebContextFactory.get().getHttpServletRequest());
	}

	public SurveyManagement(HttpServletRequest req) throws Exception { super(req); }

	public String getHTMLFormFromMapping(String type) throws Exception {
		try {
			String ret = ConfigStore.getConfig(type);
			return getHTMLForm(ret);
		} catch (Exception e){
			logger.debug("getHTMLFormFromMapping() got exception: ",e);
			DBWriterListener.writeLogs("getHTMLFormFromMapping() got exception: " +e.getMessage());
			return "";
		}
	}
	
	public String getHTMLFormFromMapping(String type, String step) throws Exception {
		try {
			String ret = ConfigStore.getConfig(type) ; 
			return getHTMLForm(ret, step);
		} catch (Exception e){
			logger.debug("getHTMLFormFromMapping() got exception: ",e);
			DBWriterListener.writeLogs("getHTMLFormFromMapping() got exception: " +e.getMessage());
			return "";
		}
	}
	
	public String getHTMLForm(String tableName) {
		try {
			CustomizedInputTable table = SimpleXMLFileParserFactory.getCustomizedInputTable(tableName);
			return table.toHTMLString();
		} catch (Exception e){
			logger.debug("getHTMLForm() got exception: ",e);
			DBWriterListener.writeLogs("getHTMLForm() got exception: " +e.getMessage());
			return "";
		}
	}
	
	public String getHTMLSurveyBasicInputForm() {
		return getHTMLForm(Common.SURVEY_BASIC_INPUT_FILE, "step1");
	}
	
	public String getHTMLForm(String tableName, String step) {
		try {
			CustomizedInputTable table = SimpleXMLFileParserFactory.getCustomizedInputTable(tableName);
			return table.toHTMLWithDiv(Survey.getStepSettingHTMLString(step));
		} catch (Exception e){
			logger.debug("getHTMLForm() got exception: ",e);
			DBWriterListener.writeLogs("getHTMLForm() got exception: " +e.getMessage());
			return "";
		}
	}
	
	@SuppressWarnings("unchecked")
	public Map getGenericSurveyPagination(int surveyPerPage) {
		Map<String, Object> ret = new HashMap<String, Object>();
		try {
			List<Long> listEndingId = new ArrayList<Long>();
			ISurveyDAO surveyDAO = DAOFactory.getDefaultFactory().getSurveyDAO();
			List<Survey> allSurveyList = surveyDAO.getAllSurveysPublished();
			ret.put("totalSurvey", allSurveyList.size());
			int idx = 0;
			for(Survey s : allSurveyList) { if(idx++ % surveyPerPage == 0) listEndingId.add(s.getId()); }
			ret.put("listEndingId", listEndingId);
			return ret;
		} catch (Exception e){
			logger.debug("getGenericSurveyPagination() got exception: ",e);
			DBWriterListener.writeLogs("getGenericSurveyPagination() got exception: " +e.getMessage());
			return ret;
		}
	}
	
	@SuppressWarnings("unchecked")
	public Map getAllAvailableSurveys(int limit, long endingId) {
		Map<String, Object> ret = new HashMap<String, Object>();
		ret.put("success", "false");
		try {
			String msg = "";
			ISurveyDAO surveyDAO = DAOFactory.getDefaultFactory().getSurveyDAO();
			List<Survey> list = surveyDAO.getLastPublishedSurveys(limit, endingId);
			msg +=  String.format("<table width='100%%' cellspacing='0' cellpadding='0' border='0' class='availsurveys'>");
			Ads[] ads = AdsListenerFactory.getRandomAds("SURVEYICON", limit, true);
			int count = 0;
			for(Survey s : list) {
				msg += String.format("<tr>" +
						"<td><br><img src='%s' alt='%s' style='width: 150px; height: 150px'/></td>" +
						"<td valign='top'>" +
							"<h3 class='survey-title'>" +
								"<a href='%s'>%s</a>" +
							"</h3>" +
							"<div class='survey-intro'>" +
								"<p>%s</p>" +
							"</div>" +
							"<small>Khởi chạy: <abbr>%s</abbr>" + (s.isPublicresult()? String.format(" <a href='opensurveyresult.jsp?uid=%s'>Kết quả</small>", s.getUid()) : "</small>")+
							"%s" +
						"</td></tr><br><br>", 
							(s.getLogoimg() == null || s.getLogoimg().length() < 5)? ads[count>ads.length? (int)(Math.random()*ads.length):count++].getHtmlcode() : s.getLogoimg(), 
									s.getTitle(), s.getTypicalURL(), s.getTitle(), s.getIntroduction(), 
									GlobalDateTimeFactory.getInstance().format(s.getCreated_timestamp(), "yyyy-MM-dd"),
									String.format(ConfigStore.getConfig("Facebook.Like.Code"), s.getTypicalURL()));
			}
			msg += "</table>";
			surveyDAO.close();
			ret.put("success", "true"); ret.put("msg", msg);
			return ret;
		} catch (Exception e){ 
			logger.error("getAllAvailbleSurveys() got exception: ", e);
			DBWriterListener.writeLogs("getAllAvailableSurveys() got exception: " +e.getMessage());
			return ret;
		}
	}

	public String getOneSurveyResult(String surveyUID) {
		String ret = "";
		try {
			ISurveyDAO surveyDAO = DAOFactory.getDefaultFactory().getSurveyDAO();
			ISurveyStatsDAO surveyStatsDAO = DAOFactory.getDefaultFactory().getSurveyStatsDAO();
			Survey s = surveyDAO.getSurveyByUID(surveyUID);
			SurveyStats surveyStats = surveyStatsDAO.getStatsBySurvey(s);
			if(surveyStats != null) ret = surveyStats.toHTMLString();
			surveyDAO.close(); surveyStatsDAO.close();
		} catch (Exception e){
			logger.error("getOneSurveyResult() got exception: ", e);
			DBWriterListener.writeLogs("getOneSurveyResult() got exception: " +e.getMessage());
			ret = "";
		}
		return ret;
	}

	@SuppressWarnings("unchecked")
	public BufferedImage displayOneQuestionStats(long questionstatsId, String chartType, int width, int height) {
		try {
			IQuestionStatsDAO questionStatsDAO = DAOFactory.getDefaultFactory().getQuestionStatsDAO();
			QuestionStats stats = questionStatsDAO.getStatsById(questionstatsId);
			if(stats != null) {
				DefaultCategoryDataset dataset = new DefaultCategoryDataset();
				for(AnswerOptionStats aostats : new ArrayList<AnswerOptionStats>(stats.getAnswer_stats())) {
					dataset.addValue(aostats.getResponses().size(), aostats.getAnsweroption().getAnswer(), aostats.getAnsweroption().getValue());
				}
				JFreeChart chart = null;
				String cTitle = "Câu hỏi: " + stats.getQuestion().getQuestion();
				final Font font = new Font("Tahoma", Font.PLAIN, 11);
			    final Color color=new Color(0,0,0);
				
				if(CustChartFactory.BARSTACK_CHART.equals(chartType)) {
					chart = ChartFactory.createStackedBarChart(cTitle, "Lựa chọn", "Kết quả",  dataset, PlotOrientation.VERTICAL, true, true, true);					
					CategoryPlot plot = (CategoryPlot)chart.getPlot();
					plot.setAxisOffset(new RectangleInsets(5D, 5D, 5D, 5D));
					plot.setDomainGridlinePaint(Color.white);
					plot.setRangeGridlinePaint(Color.white);
					chart.setBackgroundPaint(Color.lightGray.brighter());
					plot.setBackgroundPaint(Color.lightGray);
				} else if(CustChartFactory.BAR_CHART.equals(chartType)) {
					chart = ChartFactory.createBarChart(cTitle, "Lựa chọn", "Kết quả", dataset, PlotOrientation.VERTICAL, true, true, true);
					CategoryPlot plot = (CategoryPlot)chart.getPlot();
					plot.setAxisOffset(new RectangleInsets(5D, 5D, 5D, 5D));
					plot.setDomainGridlinePaint(Color.white);
					plot.setRangeGridlinePaint(Color.white);
					chart.setBackgroundPaint(Color.lightGray.brighter());
					plot.setBackgroundPaint(Color.lightGray);
				} else if(CustChartFactory.PIE_CHART.equals(chartType)) {
					if(stats.getQuestion().getQuestion_type().equals(Common.QUESTION_TYPE_RATING_SCALE)
							|| stats.getQuestion().getQuestion_type().equals(Common.QUESTION_TYPE_ORDER_ITEM)) {
						chart = ChartFactory.createMultiplePieChart(cTitle, dataset, TableOrder.BY_ROW, true, true, true);
						MultiplePiePlot plot = (MultiplePiePlot)chart.getPlot();
						plot.setBackgroundPaint(Color.white);
					} else {
						DefaultPieDataset pieDataset = new DefaultPieDataset();
						for(AnswerOptionStats aostats : new ArrayList<AnswerOptionStats>(stats.getAnswer_stats())) {
							pieDataset.setValue(aostats.getAnsweroption().getAnswer(), aostats.getResponses().size());
						}
						chart = ChartFactory.createPieChart(cTitle, pieDataset, true, true, true);
						PiePlot plot = (PiePlot)chart.getPlot();
						plot.setNoDataMessage("Không có kết quả");
						plot.setCircular(true);
						plot.setBackgroundPaint(Color.white);
					}
				}
				
				questionStatsDAO.close();
				
				chart.getTitle().setFont(font);
				StandardChartTheme chartTheme = (StandardChartTheme)StandardChartTheme.createJFreeTheme();
				
		        chartTheme.setExtraLargeFont(font);
		        chartTheme.setLargeFont(font);
		        chartTheme.setRegularFont(font);
		        chartTheme.setSmallFont(font);

		        chartTheme.setAxisLabelPaint(color);
		        chartTheme.setLegendItemPaint(color);
		        chartTheme.setItemLabelPaint(color);
				chartTheme.apply(chart);
		        
				return chart.createBufferedImage(width, height, new ChartRenderingInfo());
			}

		} catch (Exception e){
			logger.error("displayOneQuestionStats() got exception: ", e);
			DBWriterListener.writeLogs("displayOneQuestionStats() got exception: " +e.getMessage());
		}
		return null;
	}


	@SuppressWarnings("unchecked")
	public String getAllSurveys() {
		String ret = "";
		try {
			if(userAuthenticated()) {
				User u = (User)getRequest().getSession().getAttribute(Common.HTTPSESSION_USER_ATTRIBUTION);
				IUserDAO userDAO = DAOFactory.getDefaultFactory().getUserDAO();
				u = userDAO.getUser(u.getId());
				List<Survey> list = new ArrayList<Survey>(u.getSurvey_polls());
				Collections.sort(list);
				String tmpRet = "";
				tmpRet += String.format("<table class='allusersurveys'>" +
						"<tr>" + 
						"<th width='100'><b>Trạng thái</b></th>" + 
						"<th><b>Tiêu đề khảo sát</b></th>" +
						"<th width='100'><b>Ngày tạo</b></th>" +
						"<th width='50'><b>Kết quả</b></th>" + 
						"<th width='300'><b>Công cụ</b></th>" +  
				"</tr>");
				int minLen = tmpRet.length();
				for (Survey s : list) {
					String createdTime = GlobalDateTimeFactory.getInstance().format(s.getCreated_timestamp(),"yyyy-MMM-dd");
					tmpRet += String.format("<tr>" +
							"<td class='newtable'><img alt='' src='img/preset/play.gif'>%s</td>" +
							"<td class='newtable'><a class='surveytitlelink' href='%s'>%s</a>" +
							"<td class='newtable'>%s</td>" +
							"<td class='newtable'>%d</td>" +
							"<td class='newtable'>" +
							"<input type='image' border='0' src='img/preset/buttons/results.jpg' onclick='displaysurveyresult(\"%s\")'/>" +
							"<input type='image' border='0' src='img/preset/buttons/edit.jpg' onclick='editsurvey(\"%s\", \"%s\")'/>" +
							"<input type='image' border='0' src='img/preset/buttons/launch.jpg' %s onclick='launchsurvey(\"%s\")' />" +
							"<input type='image' border='0' src='img/preset/buttons/upgrade.jpg' onclick='upgradesurvey(\"%s\")' />" +
							"<input type='image' border='0' src='img/preset/buttons/delete.jpg' onclick='deletesurvey(\"%s\")' />" +
							"</td></tr>", s.getDisplaysurveystatus(), s.getTypicalURL(), s.getTitle(), 
							createdTime, s.getResponsecount(), s.getUid(), s.getUid(), s.getType(), s.getDisplaysurveystatus().equals(Common.SURVEYPOLL_STATUS_EDITING)? "": "disabled='disabled'", s.getUid(), s.getUid(), s.getUid());
				}
				tmpRet += String.format("</table>");
				if(tmpRet.length() > minLen + 10) ret = tmpRet;
				userDAO.close();
			}
			return ret;
		} catch (Exception e) {
			logger.error("getAllSurveys() got exception: ", e);
			DBWriterListener.writeLogs("getAllSurveys() got exception: " +e.getMessage());
			return "";
		}
	}

	@SuppressWarnings("unchecked")
	public Map saveEdittingQuestion(String surveyUID, String questionUid, Map qObj, Map aObj) {
		Map<String, Object> ret = new HashMap<String, Object>();
		ret.put("success", "false");
		try {
			IQuestionDAO questionDAO = DAOFactory.getDefaultFactory().getQuestionDAO();
			Question q = questionDAO.getQuestionByUid(questionUid);
			addQuestion(questionDAO, q, qObj, aObj);
			questionDAO.close();
			ISurveyDAO surveyDAO = DAOFactory.getDefaultFactory().getSurveyDAO();
			Survey s = surveyDAO.getSurveyByUID(surveyUID);
			ret.put("msg", s.toHTMLString(true, "step2"));
			surveyDAO.close();
			return ret;
		} catch (Exception e){
			logger.error("saveEdittingQuestion() got exception: ", e);
			DBWriterListener.writeLogs("saveEdittingQuestion() got exception: " +e.getMessage());
			ret.put("msg", "Báo lỗi");
			return ret;
		}
	}

	@SuppressWarnings("static-access")
	public Map<String, String> startEdittingQuestion(String surveyUid, String questionUid) {
		Map<String, String> ret = new HashMap<String, String>();
		try {
			ISurveyDAO surveyDAO = DAOFactory.getDefaultFactory().getSurveyDAO();
			Survey s = surveyDAO.getSurveyByUID(surveyUid);
			surveyDAO.close();
			if(Common.SURVEYPOLL_STATUS_LAUNCHED.equals(s.getDisplaysurveystatus())) {
				ret.put("success", "false");
				ret.put("msg", ConfigStore.getConfig("Message.EditNotAllow"));
			} else {
				IQuestionDAO questionDAO = DAOFactory.getDefaultFactory().getQuestionDAO();
				Question q = questionDAO.getQuestionByUid(questionUid);
				Map<String, Object> prefixedValues = new HashMap<String, Object>();
				prefixedValues.put("question", q.getQuestion());
				prefixedValues.put("type", q.getQuestion_type());
				CustomizedInputTable cit = SimpleXMLFileParserFactory.getCustomizedInputTable(Common.SERVICE_QUESTION_INPUT_FILE);
				q.setAnswer_options(new HashSet<AnswerOption>()); // Delete all the answer options
				questionDAO.updateQuestion(q);
				questionDAO.close();
				ret.put("success", "true");
				ret.put("msg", s.getStepSettingHTMLString("step2") + cit.toHTMLString(prefixedValues));
			}
			return ret;
		} catch (Exception e) {
			logger.error("editSurveyQuestion() got exception: ", e);
			DBWriterListener.writeLogs("editSurveyQuestion() got exception: " +e.getMessage());
		}
		return ret;
	}

	public String editSurveyBasicInformation(String surveyUid) {
		try {
			Map<String, Object> values = new HashMap<String, Object>();
			ISurveyDAO surveyDAO = DAOFactory.getDefaultFactory().getSurveyDAO();
			Survey s = surveyDAO.getSurveyByUID(surveyUid);
			values.put("email", s.getEmail());
			values.put("title", s.getTitle());
			values.put("introduction", s.getIntroduction());
			values.put("thankyou", s.getThankyou());
			CustomizedInputTable cit = SimpleXMLFileParserFactory.getCustomizedInputTable(Common.SURVEY_BASIC_INPUT_FILE);
			String ret = s.getStepSettingHTMLString();
			surveyDAO.close();
			return cit.toHtmlWithDiv(values, ret);
		} catch (Exception e){
			logger.error("editSurveyBasicInformation() got exception: ", e);
			DBWriterListener.writeLogs("editSurveyBasicInformation() got exception: " +e.getMessage());
			return "";
		}
	}

	@SuppressWarnings("unchecked")
	public String createSurvey(Map map, boolean newInput) {
		try {
			ISurveyDAO surveyDAO = DAOFactory.getDefaultFactory().getSurveyDAO();
			if(newInput) {
				if(userAuthenticated()) {
					User u = (User)getRequest().getSession().getAttribute(Common.HTTPSESSION_USER_ATTRIBUTION);
					IUserDAO userDAO = DAOFactory.getDefaultFactory().getUserDAO();
					u = userDAO.getUser(u.getId());
					Survey survey = new Survey();
					survey.setUid(UUID.randomUUID().toString());
					survey.setCreated_timestamp(GlobalTime.currentTimeSeconds());
					survey.setTitle(Common.toString(map.get("title"), ""));
					survey.setLanguage(Common.toString(map.get("language"), "Vietnamese"));
					survey.setType(Common.toString(map.get("type"), Common.FREE_INTERNET_SURVEY));
					survey.setDisplaysurveystatus(Common.SURVEYPOLL_STATUS_EDITING);
					survey.setDisplayresultstatus(Common.SURVEY_RESULT_VALID_STATUS);
					survey.setDesign(Common.toString(map.get("surveydesign"), ""));
					survey.setEmail(Common.toString(map.get("email"), ConfigStore.getConfig("Application.Logger.Email")));
					survey.setEmailnotification(map.containsKey("email"));
					survey.setLogoimg(Common.toString(map.get("logoimg"), ""));
					survey.setIntroduction(Common.toString(map.get("introduction"), ""));
					survey.setThankyou(Common.toString(map.get("thankyou"), ""));
					survey.setResponsecount(0);
					survey.setPublicresult(Common.toInt(map.get("private"), 1) == 1);
					survey.setPrevent_duplicateresponse(Common.toInt(map.get("preventduplicate_response"), 1) == 1);
					survey.setLastupdated_timestamp(GlobalTime.currentTimeSeconds());

					ISurveyPageDAO surveyPageDAO = DAOFactory.getDefaultFactory().getSurveyPageDAO();
					SurveyPage surveyPage = new SurveyPage();
					surveyPage.setQuestions(null);
					surveyPage.setDesign(""); // TODO for now only
					surveyPage.setFooter("");
					surveyPage.setHeader("");
					surveyPage.setIdx(1);
					surveyPageDAO.insertSurveyPage(surveyPage);

					Set<SurveyPage> set = new HashSet<SurveyPage>(); set.add(surveyPage);
					survey.setPages(set);
					survey.setUser(u);
					survey = surveyDAO.insertSurvey(survey);

					u.getSurvey_polls().add(survey);
					u = userDAO.updateUser(u);
					getRequest().getSession().setAttribute(Common.HTTPSESSION_USER_ATTRIBUTION, u);

					surveyDAO.close(); userDAO.close(); surveyPageDAO.close();
					return survey.getUid();
				} else return "";
			} else {
				// TODO not implement yet
			}
			return "";
		} catch (Exception e){
			logger.debug("createSurvey() got exception: ", e);
			DBWriterListener.writeLogs("createSurvey() got exception: " +e.getMessage());
			return "";
		}
	}

	@SuppressWarnings("unchecked")
	public String saveSurveyBasicInput(Map map, String surveyUid) {
		try {
			ISurveyDAO surveyDAO = DAOFactory.getDefaultFactory().getSurveyDAO();
			Survey survey = surveyDAO.getSurveyByUID(surveyUid);
			survey.setTitle(Common.toString(map.get("title"), ""));
			survey.setLanguage(Common.toString(map.get("language"), "Vietnamese"));
			survey.setType(Common.toString(map.get("type"), Common.FREE_INTERNET_SURVEY));
			survey.setLogoimg(Common.toString(map.get("logoimg"), ""));
			survey.setDesign(Common.toString(map.get("surveydesign"), ""));
			survey.setEmail(Common.toString(map.get("email"), ConfigStore.getConfig("Application.Logger.Email")));
			survey.setEmailnotification(map.containsKey("email"));
			survey.setIntroduction(Common.toString(map.get("introduction"), ""));
			survey.setThankyou(Common.toString(map.get("thankyou"), ""));
			survey.setPublicresult(Common.toInt(map.get("private"), 1) == 1);
			survey.setPrevent_duplicateresponse(Common.toInt(map.get("preventduplicate_response"), 1) == 1);
			survey.setLastupdated_timestamp(GlobalTime.currentTimeSeconds());
			survey = surveyDAO.updateSurvey(survey);
			String ret = survey.toHTMLString(true, "step2");
			surveyDAO.close();
			return ret;
		} catch (Exception e){
			logger.debug("saveSurveyBasicInput() got exception: ", e);
			DBWriterListener.writeLogs("saveSurveyBasicInput() got exception: " +e.getMessage());
			return "";
		}
	}

	private static final String EXCEEDING_ALLOWABLE_QUESTIONS = ConfigStore.getConfig("Message.ExceedingQuestion");
	@SuppressWarnings("unchecked")
	public Map insertQuestionIntoSurvey(String surveyUID, long nextQuestionIdx, Map qObj, Map aObj) {
		Map<String, Object> ret = new HashMap<String, Object>();
		ret.put("success", "false");
		try {
			ISurveyDAO surveyDAO = DAOFactory.getDefaultFactory().getSurveyDAO();
			Survey survey = surveyDAO.getSurveyByUID(surveyUID);
			if(getNextQuestionIdx(getLastSurveyPage(survey)) < ServiceDefStore.getServiceTypeDef(survey.getType()).getMaxquestion()) {
				ret.put("msg", EXCEEDING_ALLOWABLE_QUESTIONS);
				surveyDAO.close();
				return ret;
			}
			SurveyPage currSP = null;
			for(SurveyPage sp : new ArrayList<SurveyPage>(survey.getPages())) {
				Iterator<Question> it = sp.getQuestions().iterator();
				while(it.hasNext()) {
					Question q = it.next();
					if(q.getIdx() >= nextQuestionIdx) {
						if(q.getIdx() == nextQuestionIdx) currSP = sp;
						q.setIdx(q.getIdx() +1);
					}
				}
			}

			addQuestion(qObj, aObj, nextQuestionIdx, currSP);
			survey = surveyDAO.updateSurvey(survey);
			ret.put("success", "true");
			ret.put("msg", survey.toHTMLString(true, "step2"));
			surveyDAO.close();
			return ret;
		} catch (Exception e){
			logger.error("insertQuestionIntoSurvey() got exception: ", e);
			DBWriterListener.writeLogs("insertQuestionIntoSurvey() got exception: " +e.getMessage());
			ret.put("msg", "Báo lỗi");
			return ret;
		}
	}

	@SuppressWarnings("unchecked")
	public Map addQuestionIntoSurvey(String surveyUID, Map qObj, Map aObj){
		Map<String, Object> ret = new HashMap<String, Object>();
		ret.put("success", "false");
		try {
			ISurveyDAO surveyDAO = DAOFactory.getDefaultFactory().getSurveyDAO();
			Survey survey = surveyDAO.getSurveyByUID(surveyUID);
			SurveyPage page = getLastSurveyPage(survey);
			long nextIdx = getNextQuestionIdx(page);
			if(ServiceDefStore.getServiceTypeDef(survey.getType()).getMaxquestion() < nextIdx) {
				ret.put("msg", EXCEEDING_ALLOWABLE_QUESTIONS);
				surveyDAO.close();
				return ret;
			}
			addQuestion(qObj, aObj, nextIdx , page); 
			survey = surveyDAO.updateSurvey(survey);
			ret.put("success", "true");
			ret.put("msg", survey.toHTMLString(true, "step2"));
			surveyDAO.close(); 
			return ret;
		} catch (Exception e){
			logger.error("addQuestionIntoSurvey() got exception: ",e);
			DBWriterListener.writeLogs("addQuestionIntoSurvey() got exception: " +e.getMessage());
			ret.put("msg", "Báo lỗi");
			return ret;
		}
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> deleteQuestion(String surveyUid, long surveyPageId, long questionId) {
		Map<String, String> ret = new HashMap<String, String>();
		ret.put("success", "false");
		try {
			ISurveyDAO surveyDAO = DAOFactory.getDefaultFactory().getSurveyDAO();
			Survey survey = surveyDAO.getSurveyByUID(surveyUid);
			if(Common.SURVEYPOLL_STATUS_LAUNCHED.equals(survey.getDisplaysurveystatus())) {
				ret.put("msg", ConfigStore.getConfig("Message.DeleteNotAllow"));
			} else {
				IQuestionDAO questionDAO = DAOFactory.getDefaultFactory().getQuestionDAO();
				Question deletingQuestion = questionDAO.getQuestionById(questionId);
				for(SurveyPage sp : new ArrayList<SurveyPage>(survey.getPages())) {
					Iterator<Question> it = sp.getQuestions().iterator();
					while(it.hasNext()) {
						Question q = it.next();
						if(q.getIdx() > deletingQuestion.getIdx()) {
							q.setIdx(q.getIdx() -1);
						}  else if(q.getId() == questionId) it.remove();
					}
				}
				survey = surveyDAO.updateSurvey(survey);
				questionDAO.deleteQuestion(deletingQuestion);
				ret.put("success", "true");
				ret.put("msg", survey.toHTMLString(true, "step2"));
				surveyDAO.close(); questionDAO.close(); 
			}
			return ret;
		} catch (Exception e){
			logger.error("deleteQuestion() got exception: ", e);
			ret.put("msg", "Báo lỗi");
			DBWriterListener.writeLogs("deleteQuestion() got exception: " +e.getMessage());
		}
		return ret;
	}

	public String deleteSurvey(String surveyUid) {
		try {
			if(userAuthenticated()) {
				User u = (User)getRequest().getSession().getAttribute(Common.HTTPSESSION_USER_ATTRIBUTION);
				IUserDAO userDAO = DAOFactory.getDefaultFactory().getUserDAO();
				ISurveyDAO surveyDAO = DAOFactory.getDefaultFactory().getSurveyDAO();
				
				u = userDAO.getUser(u.getId());
				Survey survey = surveyDAO.getSurveyByUID(surveyUid);

				// Delete in user
				u.getSurvey_polls().remove(survey);
				u = userDAO.updateUser(u);
				getRequest().getSession().setAttribute(Common.HTTPSESSION_USER_ATTRIBUTION, u);

				// Delete in survey stats
				ISurveyStatsDAO surveyStatsDAO = DAOFactory.getDefaultFactory().getSurveyStatsDAO();
				SurveyStats surveyStats = surveyStatsDAO.getStatsBySurvey(survey);
				if(surveyStats != null) surveyStatsDAO.deleteSurveyStats(surveyStats);

				surveyDAO.deleteSurvey(surveyUid);
				surveyDAO.close(); surveyStatsDAO.close(); userDAO.close();
				return getAllSurveys();
			}
			return "";
		} catch (Exception e){
			logger.error("deleteSurvey() got exception: ", e);
			DBWriterListener.writeLogs("deleteSurvey() got exception: " +e.getMessage());
			return "";
		}
	}

	@SuppressWarnings("static-access")
	public String launchOneSurvey(String uid) {
		try {
			User u = (User)getRequest().getSession().getAttribute(Common.HTTPSESSION_USER_ATTRIBUTION);
			ISurveyDAO surveyDAO = DAOFactory.getDefaultFactory().getSurveyDAO();
			Survey s = surveyDAO.getSurveyByUID(uid);
			String oldStatus = s.getDisplaysurveystatus();
			s.setDisplaysurveystatus(Common.SURVEYPOLL_STATUS_LAUNCHED);
			s.setLastupdated_timestamp(GlobalTime.currentTimeSeconds());
			surveyDAO.flush();
			surveyDAO.close();
			if(Common.SURVEYPOLL_STATUS_EDITING.equals(oldStatus)) {
				EmailTask task = new EmailTask();
				task.setSubject(CommonDWR.LAUNCHING_SURVEY_EMAIL_SUBJECT);
				task.setToAddresses(new String[] { (s.getEmail() == null || s.getEmail().equals(""))? ConfigStore.getConfig("Application.Admin.Email") : s.getEmail()});
				task.setBody(String.format(CommonDWR.LAUNCHING_SURVEY_EMAILBODY, u != null? u.getFullname():"", s.getUid()));
				EmailListener.addEmailTask(task);
				logger.error("launchOneSurvey(), finished scheduling an email!");
			}
			String ret = s.getStepSettingHTMLString("step3");
			ret += String.format("<table width='100%%' cellpadding='4' cellspacing='4' border='0'>" +
					"<tbody><tr><td>" +
					"<p class='title'>Khởi chạy khảo sát thành công</p>" +
					"<div class='indent-50 padded-50'>" +
						"<table class='norm' width='100%%' cellspacing='4' cellpadding='4' border='0'>" +
							"<tbody>" +
								"<tr>" +
									"<td valign='top'>" +
										"<span class='title'>1.</span>" +
									"</td>" +
									"<td valign='top'>Đây là địa chỉ link khảo sát duy nhất của bạn, vui lòng gửi link này đến những người điền khảo sát do bạn chọn<br>" +
										"<form id='unique_surveyurl' method='post' action=''>" +
											"<div>" +
												"<input id='surveyli' class='surveyli' type='text' onclick='document.getElementById(\"surveyli\").focus();document.getElementById(\"surveyli\").select();' size='120' value='%s' name='textarea'>" +
											"</div>" +
										"</form>" +
									"</td>" +
								"</tr>" +
								
								"<tr>"+
									"<td>&nbsp;</td>"+
									"<td>" +
										"<i><b>Mẹo nhỏ dành cho bạn</b>: sử dụng nút 'Like' ở <a href='index.jsp'>trang chính</a> để hiển thị link khảo sát này trên trang FaceBook cá nhân của bạn và sau đó tag các bạn bè của bạn vào<br></i>" +
									"</td>"+
								"</tr>"+
							"</tbody>" +
						"</table>", ConfigStore.getConfig("Application.Host.URL") + "/" + s.getTypicalURL());
			return ret;
		} catch (Exception e) {
			logger.error("launchOneSurvey() got exception: ", e);
			DBWriterListener.writeLogs("launchOneSurvey() got exception: " +e.getMessage());
			return "";
		}
	}

	public String displayOneSurvey(String uid) {
		try {
			ISurveyDAO surveyDAO = DAOFactory.getDefaultFactory().getSurveyDAO();
			Survey survey = surveyDAO.getSurveyByUID(uid);
			String ret = survey.toHTMLString(true, "step2");
			surveyDAO.close();
			return ret;
		} catch (Exception e){
			logger.error("displayOneSurvey() got exception: ", e);
			DBWriterListener.writeLogs("displayOneSurvey() got exception: " +e.getMessage());
			return "";
		}
	}

	public Map<String, Object> getSurveyUpgradeInputForm(String surveyUid){
		Map<String, Object> ret = new HashMap<String, Object>();
		ret.put("success", "false");
		try {
			ISurveyDAO surveyDAO = DAOFactory.getDefaultFactory().getSurveyDAO();
			Survey survey = surveyDAO.getSurveyByUID(surveyUid);
			surveyDAO.close();
			if(survey.getType().equals(Common.PREMIUM_INTERNET_SURVEY)) {
				ret.put("msg", ConfigStore.getConfig("Message.SurveyAlreadyUpgraded"));
				return ret;
			}
			Map<String, Object> values = new HashMap<String, Object>();
			values.put("title", survey.getTitle());
			CustomizedInputTable table = SimpleXMLFileParserFactory.getCustomizedInputTable(Common.SURVEY_UPGRADE_INPUT_FILE);
			ret.put("success", "true"); ret.put("msg", table.toHTMLString(values));
			return ret;
		} catch (Exception e){
			logger.error("getSurveyUpgradeInputForm() got exception: ", e);
			DBWriterListener.writeLogs("getSurveyUpgradeInputForm() got exception: " +e.getMessage());
			ret.put("msg", "Báo lỗi");
			return ret;
		}
	}
	
	@SuppressWarnings("unchecked")
	public boolean upgradeOneSurvey(Map map) {
		try {
			if(userAuthenticated()) {
				User user = (User)getRequest().getSession().getAttribute(Common.HTTPSESSION_USER_ATTRIBUTION);
				IPendingSurveyUpgradeDAO pendingSurveyUpgradeDAO = DAOFactory.getDefaultFactory().getPendingSurveyUpgradeDAO();
				ISurveyDAO surveyDAO = DAOFactory.getDefaultFactory().getSurveyDAO();
				Survey s = surveyDAO.getSurveyByUID(map.get("surveyUID").toString());
				surveyDAO.close();
				PendingSurveyUpgrade psu = new PendingSurveyUpgrade();
				psu.setRefnum(map.get("refnum").toString());
				psu.setStatus(Common.UPGRADE_STATUS_UNDERPROCESSING);
				psu.setSurvey(s);
				psu.setTimestamp(GlobalTime.currentTimeSeconds());
				boolean ret = pendingSurveyUpgradeDAO.insertPendingSurveyUpgrade(psu);
				pendingSurveyUpgradeDAO.close();
				
				// Sending and email of confirmation
				EmailTask task = new EmailTask();
				task.setToAddresses(new String[]{ s.getEmail(), user.getUsername_emailaddress(), ConfigStore.getConfig("Application.Admin.Email")});
				task.setSubject(CommonDWR.SURVEY_UPGRADE_EMAIL_SUBJECT);
				task.setBody(String.format(CommonDWR.SURVEY_UPGRADE_EMAILBODY, user.getFullname()));
				EmailListener.addEmailTask(task);
				
				return ret;
			}
			return false;
		} catch (Exception e){
			logger.error("upgradeOneSurvey() got exception: ", e);
			DBWriterListener.writeLogs("upgradeOneSurvey() got exception: " +e.getMessage());
			return false;
		}
	}
	
	@SuppressWarnings("unchecked")
	private Question addQuestion(IQuestionDAO questionDAO, Question q, Map qObj, Map aObj) {
		try {
			String questionType = Common.toString(qObj.get("type"), "");
			q.setQuestion(Common.toString(qObj.get("question"), ""));
	
			q.setQuestion_type(questionType);
			q.setAnswer_compulsory(Common.toInt(aObj.get("answer_compulsory"), 0) == 1);
			q.setAppend_default_option(Common.toInt(aObj.get("append_default_option"), 0) == 1);
			q.setDisplayansweroptionstyle(Common.toString(aObj.get("displayansweroptionstyle"), ""));

			IAnswerOptionDAO answeroptionDAO = DAOFactory.getDefaultFactory().getAnswerOptionDAO();
			Set<AnswerOption> answers = new HashSet<AnswerOption>();
			int idx = 1;
			if(questionType.equals(Common.QUESTION_TYPE_MULTIPLE_CHOICES) 
					|| questionType.equals(Common.QUESTION_TYPE_MORE_THAN_ONE)
					|| questionType.equals(Common.QUESTION_TYPE_TRUE_FALSE)) {
				String[] answerOptionArr = questionType.equals(Common.QUESTION_TYPE_TRUE_FALSE)?
						new String[]{"TRUE", "FALSE"} : Common.toString(aObj.get("answer_options"), "").split("\\r|\\n");
						// Those questions which have the answer options listed separatedly by a line!
						for(String oneoption : answerOptionArr){
							AnswerOption ao = new AnswerOption();
							ao.setAnswer(oneoption);
							ao.setIdx(idx++);
							ao.setValue("");
							ao.setAnswer_type(Common.toString(ConfigStore.getConfig(Common.COMPONENT_QTYPE_AO_MAPPING+questionType), ""));
							answeroptionDAO.insertOrUpdateAnswerOption(ao);
							answers.add(ao);
						}
						// Check whether there is a default answer
						if(q.isAppend_default_option()) {
							AnswerOption ao = new AnswerOption();
							ao.setAnswer(Common.ANSWEROPTION_DEFAULT);
							ao.setIdx(idx++);
							ao.setValue("");
							ao.setAnswer_type(Common.toString(ConfigStore.getConfig(Common.COMPONENT_QTYPE_AO_MAPPING+questionType), Common.TYPE_TEXTBOX));
							answeroptionDAO.insertOrUpdateAnswerOption(ao);
							answers.add(ao);
						}
			} else if(questionType.equals(Common.QUESTION_TYPE_OPEN_ENDED)) {
				AnswerOption ao = new AnswerOption();
				ao.setAnswer("");
				ao.setValue("");
				ao.setIdx(idx++);
				ao.setAnswer_type(Common.toString(ConfigStore.getConfig(Common.COMPONENT_QTYPE_AO_MAPPING+questionType), Common.TYPE_TEXTBOX));
				answeroptionDAO.insertOrUpdateAnswerOption(ao);
				answers.add(ao);	
			} else if(questionType.equals(Common.QUESTION_TYPE_RATING_SCALE)){
				String rowheadings = Common.toString(aObj.get("row_headings"), "");
				String columnheadings = Common.toString(aObj.get("column_headings"), "");
				// Those questions which have the answer options listed separately by a line!
				for(String oneRow : rowheadings.split("\\r|\\n")) {
					for(String oneColumn : columnheadings.split("\\r|\\n")) {
						AnswerOption ao = new AnswerOption();
						ao.setAnswer(oneRow);
						ao.setValue(oneColumn);
						ao.setIdx(idx++);
						ao.setAnswer_type(Common.toString(ConfigStore.getConfig(Common.COMPONENT_QTYPE_AO_MAPPING+questionType), ""));
						answeroptionDAO.insertOrUpdateAnswerOption(ao);
						answers.add(ao);
					}
				}
			} else if(questionType.equals(Common.QUESTION_TYPE_ORDER_ITEM)) {
				String[] answerOptions = Common.toString(aObj.get("answer_options"), "").split("\\r|\\n");
				for(String op : answerOptions) {
					for(int i = 1; i <= answerOptions.length; i ++) {
						AnswerOption ao = new AnswerOption();
						ao.setAnswer(op);
						ao.setValue(String.format("%d", i));
						ao.setIdx(idx++);
						ao.setAnswer_type(Common.toString(ConfigStore.getConfig(Common.COMPONENT_QTYPE_AO_MAPPING+questionType), ""));
						answeroptionDAO.insertOrUpdateAnswerOption(ao);
						answers.add(ao);
					}
				}
			}

			q.setAnswer_options(answers);
			questionDAO.updateQuestion(q);
			answeroptionDAO.close();
			return q;
		} catch (Exception e){
			logger.error("addQuestion() got exception: ", e);
			DBWriterListener.writeLogs("addQuestion() got exception: " +e.getMessage());
			return q;
		}
	}

	@SuppressWarnings({ "unchecked", "unchecked" })
	private Question addQuestion(Map qObj, Map aObj, long qIndex, SurveyPage page) throws Exception {
		Question q = new Question();
		q.setUid(UUID.randomUUID().toString());
		q.setIdx(qIndex);
		q.setQuestion(Common.toString(qObj.get("question"), ""));
		String questionType = Common.toString(qObj.get("type"), "");
			
		q.setQuestion_type(questionType);
		q.setAnswer_compulsory(Common.toInt(aObj.get("answer_compulsory"), 0) == 1);
		q.setAppend_default_option(Common.toInt(aObj.get("append_default_option"), 0) == 1);
		q.setDisplayansweroptionstyle(Common.toString(aObj.get("displayansweroptionstyle"), ""));

		IAnswerOptionDAO answeroptionDAO = DAOFactory.getDefaultFactory().getAnswerOptionDAO();
		Set<AnswerOption> answers = new HashSet<AnswerOption>();
		int idx = 1;
		if(questionType.equals(Common.QUESTION_TYPE_MULTIPLE_CHOICES) 
				|| questionType.equals(Common.QUESTION_TYPE_MORE_THAN_ONE)
				|| questionType.equals(Common.QUESTION_TYPE_TRUE_FALSE)) {
			String[] answerOptionArr = questionType.equals(Common.QUESTION_TYPE_TRUE_FALSE)?
					new String[]{"TRUE", "FALSE"} : Common.toString(aObj.get("answer_options"), "").split("\\r|\\n");
					// Those questions which have the answer options listed separately by a line!
					for(String oneoption : answerOptionArr){
						AnswerOption ao = new AnswerOption();
						ao.setAnswer(oneoption);
						ao.setIdx(idx++);
						ao.setValue("");
						ao.setAnswer_type(Common.toString(ConfigStore.getConfig(Common.COMPONENT_QTYPE_AO_MAPPING+questionType), ""));
						answeroptionDAO.insertOrUpdateAnswerOption(ao);
						answers.add(ao);
					}
					// Check whether there is a default answer
					if(q.isAppend_default_option()) {
						AnswerOption ao = new AnswerOption();
						ao.setAnswer(Common.ANSWEROPTION_DEFAULT);
						ao.setIdx(idx++);
						ao.setValue("");
						ao.setAnswer_type(Common.toString(ConfigStore.getConfig(Common.COMPONENT_QTYPE_AO_MAPPING+questionType), Common.TYPE_TEXTBOX));
						answeroptionDAO.insertOrUpdateAnswerOption(ao);
						answers.add(ao);
					}
		} else if(questionType.equals(Common.QUESTION_TYPE_OPEN_ENDED)) {
			AnswerOption ao = new AnswerOption();
			ao.setAnswer("");
			ao.setValue("");
			ao.setIdx(idx++);
			ao.setAnswer_type(Common.toString(ConfigStore.getConfig(Common.COMPONENT_QTYPE_AO_MAPPING+questionType), Common.TYPE_TEXTBOX));
			answeroptionDAO.insertOrUpdateAnswerOption(ao);
			answers.add(ao);	
		} else if(questionType.equals(Common.QUESTION_TYPE_RATING_SCALE)){
			String rowheadings = Common.toString(aObj.get("row_headings"), "");
			String columnheadings = Common.toString(aObj.get("column_headings"), "");
			// Those questions which have the answer options listed separately by a line!
			for(String oneRow : rowheadings.split("\\r|\\n")) {
				for(String oneColumn : columnheadings.split("\\r|\\n")) {
					AnswerOption ao = new AnswerOption();
					ao.setAnswer(oneRow);
					ao.setValue(oneColumn);
					ao.setIdx(idx++);
					ao.setAnswer_type(Common.toString(ConfigStore.getConfig(Common.COMPONENT_QTYPE_AO_MAPPING+questionType), ""));
					answeroptionDAO.insertOrUpdateAnswerOption(ao);
					answers.add(ao);
				}
			}
		} else if(questionType.equals(Common.QUESTION_TYPE_ORDER_ITEM)) {
			String[] answerOptions = Common.toString(aObj.get("answer_options"), "").split("\\r|\\n");
			for(String op : answerOptions) {
				for(int i = 1; i <= answerOptions.length; i ++) {
					AnswerOption ao = new AnswerOption();
					ao.setAnswer(op);
					ao.setValue(String.format("%d", i));
					ao.setIdx(idx++);
					ao.setAnswer_type(Common.toString(ConfigStore.getConfig(Common.COMPONENT_QTYPE_AO_MAPPING+questionType), ""));
					answeroptionDAO.insertOrUpdateAnswerOption(ao);
					answers.add(ao);
				}
			}
		}

		q.setAnswer_options(answers);
		IQuestionDAO questionDAO = DAOFactory.getDefaultFactory().getQuestionDAO();
		q = questionDAO.insertQuestion(q);
		page.getQuestions().add(q);
		
		answeroptionDAO.close();
		questionDAO.close();
		return q;
	}

	@SuppressWarnings("unchecked")
	private SurveyPage getLastSurveyPage(Survey s) {
		Iterator<SurveyPage> it = s.getPages().iterator();
		SurveyPage ret = it.next();
		while(it.hasNext()) {
			SurveyPage temp = it.next();
			ret = temp.getIdx() > ret.getIdx() ? temp : ret;
		}
		return ret;
	}

	@SuppressWarnings("unchecked")
	private long getNextQuestionIdx(SurveyPage page) {
		long ret = 0;
		for(Iterator<Question> it = page.getQuestions().iterator(); it.hasNext();) {
			ret = Math.max(it.next().getIdx(), ret);
		}
		return ret + 1;
	}

}