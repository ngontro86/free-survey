
var submitted = false;
var nextCall = null;

$(document).ready(function() {
    autoRefresh('bigbanner_pos1', 'ANY', 1, 30000, true);
});


function checkMandatorySurveyResponseFields(name, compulsoryQuestionIdxs) {
    var fieldArr = compulsoryQuestionIdxs.split(',');
    for(var i = 0; i < fieldArr.length; i++) {
        fieldID = fieldArr[i];
        if(fieldID != null && fieldID.length > 1) {
            var obj = getFormValueObject("answeroption_"+fieldID);
            var filled = false;
            for(var key in obj) {
               if(obj[key] != null && obj[key].length > 0) filled = true;
            }
            if(filled == false) return false;
        }
    }
    return true;
}


function submitsurveyresponse(surveyUID, compulsoryQuestionDivIds, allQuestionDivIds) {
    $('#submit_surveyresponse').attr("disabled", "true");
    if(checkMandatorySurveyResponseFields(surveyUID, compulsoryQuestionDivIds) == false) { 
        alert("Vui lòng trả lời đầy đủ các câu hỏi bắt buộc"); 
        $('#submit_surveyresponse').attr("disabled", "false"); return; 
    }
    var allResponses = new Object();
    var allQuestionDivArr = allQuestionDivIds.split(',');
    for(var i = 0; i < allQuestionDivArr.length; i++) {
        var questionDivId = allQuestionDivArr[i];
        if(questionDivId != null && questionDivId.length > 0) {
            allResponses[questionDivId] = getFormValueObject("answeroption_"+questionDivId);
        }
    }
    var respBack = function(data) {
        $("#spinningwheel").html(''); $('#submit_surveyresponse').attr("disabled", "false");
        if(data.success == 'true') {
            submitted = true; stopUpdateAds();
            var htmlString = "<div class='main'>";
                htmlString += "<div class='mainContent'>";
                    htmlString += "<div class='content-wrapper-c'>";
                        htmlString += "<div class='home-landing-details'>";
                            htmlString += "<h1 class='thank-you'>" + data.message + "</h1>";
                            htmlString += "<h1> Bạn có thể tạo khảo sát MIỄN PHÍ ngay tại đây.</h1></div>";
                        htmlString += "<div class='home-landing-inner-container'>";
                            htmlString += "<div class='clearfix'>";
                                htmlString += "<div class='cell'><ul class='list-3'><li> Tạo câu hỏi -> gửi khảo sát -> nhận kết quả -> phân tích kết quả</li><li>Công cụ khảo sát trực tuyến hàng đầu hiện nay</li></ul></div>";
                                htmlString += "<div class='btnSpace'><a href='index.jsp'><span>Đăng ký tài khoản MIỄN PHÍ</span></div>";
                                htmlString += "</div></div>";
            htmlString += "</div></div>";
            $('#survey').hide();
            $('#thankyou').html(htmlString);
            $('#thankyou').show();
        } else {
            alert("Vui lòng kiểm tra lại câu trả lời");
        }
    }
    $("#spinningwheel").html('<img src="img/small_loader.gif" alt="Wait" />');
    SurveyResponseManagement.processOneSurveyResponse(surveyUID, allResponses, respBack);
}

