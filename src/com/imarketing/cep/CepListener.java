package com.imarketing.cep;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.imarketing.cep.engine.CepEngine;
import com.imarketing.cep.io.EventProcessor;
import com.imarketing.cep.io.ObjectReceiver;
import com.imarketing.cep.io.SocketListener;
import com.imarketing.cep.io.SocketSender;
import com.imarketing.cep.io.SubscribeProcessor;
import com.imarketing.cep.util.ObjectEvent;
import com.imarketing.cep.util.ProcSubscribe;
import com.imarketing.common.Common;
import com.imarketing.conf.ConfigStore;


public class CepListener implements ServletContextListener {

	private Logger logger = Logger.getLogger(CepListener.class);
	
	private static CepEngine engine = null;
	
	SocketListener<Object, ObjectEvent> objectListener;
	
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		if(ConfigStore.getConfig("Cep.Activated").equals("TRUE")) {
			logger.info("############## Starting cep engine ################");
			if(engine == null) {engine = new CepEngine();}
			SubscribeProcessor subscribeProcessor = new SubscribeProcessor(engine); startThread(subscribeProcessor);
			EventProcessor eventProcessor = new EventProcessor(engine); startThread(eventProcessor);
			
			ObjectReceiver objectReceiver = new ObjectReceiver();
			objectReceiver.registerProcessor(ProcSubscribe.class, subscribeProcessor);
			objectReceiver.setDefaultProcessor(eventProcessor);
			
			objectListener = new SocketListener<Object, ObjectEvent>(ConfigStore.getConfig("Cep.AuthRef"), Common.toInt(ConfigStore.getConfig("Cep.ListeningPort"), 9999), objectReceiver);
			
			SocketSender<ObjectEvent> objectSender = new SocketSender<ObjectEvent>(objectListener);
			objectReceiver.setOutgoingHandler(objectSender);
			startThread(objectListener);
		}
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		logger.info("CepListener: context destroyed, release the socket now");
		objectListener.quit();
	}
	
	public static CepEngine getCep() { return engine; }
	
	private void startThread(Runnable runnable) { new Thread(runnable).start(); }
}
