package com.imarketing.db;

import java.io.Serializable;
import java.util.Set;

@SuppressWarnings("serial")
public class AccessibleMenu implements Serializable, Comparable<AccessibleMenu>{
	private Long id;
	private String label;
	private String imgsrc;
	private String link;
	
	@SuppressWarnings("unchecked")
	private Set submenus;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	
	public String getImgsrc() {
		return imgsrc;
	}
	public void setImgsrc(String imgsrc) {
		this.imgsrc = imgsrc;
	}
	@SuppressWarnings("unchecked")
	public Set getSubmenus() {
		return submenus;
	}
	@SuppressWarnings("unchecked")
	public void setSubmenus(Set submenus) {
		this.submenus = submenus;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final AccessibleMenu other = (AccessibleMenu) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(AccessibleMenu arg0) {
		if(arg0 == null) return 1;
		if(this.getId().longValue() > arg0.getId().longValue()) return 1;
		if(this.getId().longValue() < arg0.getId().longValue()) return -1;
		return 0;
	}
	
}
