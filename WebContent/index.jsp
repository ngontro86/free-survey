<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.imarketing.client.MenuGeneratorClient"%>
<%@page import="com.imarketing.client.SurveyGeneratorClient"%>
<%@page import="com.imarketing.common.Common"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<%@include file="includes/google_analytics.inc"%>

<title>Chào mừng bạn đến với KhaosatOnline</title>

<script type='text/javascript' src='dwr/engine.js'></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script type='text/javascript' src='dwr/interface/UserManagement.js'></script>
<script type='text/javascript' src='dwr/interface/SurveyManagement.js'></script>
<script type='text/javascript' src='dwr/interface/SurveyResponseManagement.js'></script>
<script type='text/javascript' src='dwr/interface/AdsManagement.js'></script>


<script type='text/javascript' src='js/utils/richtext/wysiwyg.js'></script>
<script type='text/javascript' src='js/utils/jquery.js'></script>
<script type='text/javascript' src='js/utils/msgbox/jquery.dragndrop.min.js'></script>
<script type='text/javascript' src='js/utils/msgbox/jquery.msgbox.js'></script>
<script type='text/javascript' src='js/utils/jquery.jqDock.min.js'></script>
<script type='text/javascript' src='js/utils/draggable.jquery.ui.js'></script>
<script type="text/javascript" src="js/utils/jquery-easing-1.3.pack.js"></script>
<script type="text/javascript" src="js/utils/jquery-easing-compatibility.1.2.pack.js"></script>
<script type="text/javascript" src="js/utils/coda-slider.1.1.1.pack.js"></script>
<script type="text/javascript" src="js/utils/zopim.js"></script>

<script type='text/javascript' src='js/common/cookies.js'></script>
<script type='text/javascript' src='js/common/common.js'></script>
<script type='text/javascript' src='js/common/ads.js'></script>
<script type='text/javascript' src='js/index.js'></script>

<link rel="stylesheet" href="css/common.css" type="text/css" />
<link rel="stylesheet" href="css/index.css" type="text/css" />
<link rel="stylesheet" href="css/menu.css" type="text/css" />
<link rel="stylesheet" href="css/slider.css" type="text/css" />
<link rel=stylesheet href="css/jquery.msgbox.css" type=text/css />
<link rel=stylesheet href="css/genericSearchPagination.css" type=text/css />

<script type="javascript">
<% 
String header = "";
MenuGeneratorClient menuGeneratorClient = new MenuGeneratorClient(request, response);
session.setAttribute(Common.HTTPSESSION_CURRENT_MENU_ID, new Long(Common.DEFAULT_PAGE_WITHOUT_AUTHENTICATION));
String menuHtml = menuGeneratorClient.getMenuTabHTML();
%>
</script>

</head>

<body>
<table class="mainBodyTable" style="vertical-align:top">
  <tr>
    <td width="15%" rowspan="6" bgcolor="#333333"><div class="leftads" id="leftads"></div></td>
    <td height="143" rowspan="2" align="center" bgcolor="#D93911"><div class="toplogo" id="toplogo"> <h1 class="style6">KhaosatOnline</h1></div></td>
    <td height="135" bgcolor="#EAEAEA"><div class="topbanner" id="topbanner"></div></td>
    <td width="15%" rowspan="6"><div class="rightads" id="rightads"></div></td>
  </tr>
  <tr>
    <td height="30" bgcolor="#D93911"><div class="menutab" id="menutab"><ul><%=menuHtml%></ul></div></td>
  </tr>
  <tr style="vertical-align:top">
    <td height="120" bgcolor="#D93911"><h3 class="style2"><strong>Ba bước để tham gia</strong>:</h3>
        <p class="style2">1. Đăng ký thành viên</p>
        <p class="style2">2. Tạo khảo sát MIỄN PHÍ</p>
        <p class="style2">3. Gửi khảo sát đến cho bạn bè</p>
    </td>
     <td width="51%" rowspan="3" bgcolor="#EAEAEA" style="vertical-align:top">
        <div id="availsurveys" style="display:none;vertical-align:top"></div>
        <div id="registration" style="display:none">
            <%
             String registrationFormHML = menuGeneratorClient.getHTMLForm(Common.USER_REGISTRATION_INPUT_FILE);
             out.println(registrationFormHML);
            %>
        </div>
        <div id="signin" style="display:none">
            <%
              String loginFormHML = menuGeneratorClient.getHTMLForm(Common.USER_LOGIN_INPUT_FILE);
              out.println(loginFormHML);
            %>
        </div>
        <div id='spinningwheel'></div>
     </td>    
  </tr>
  <tr>
    <td bgcolor="#D93911" valign="top" align="center">
      <h2 class="style4">Đăng nhập</h2>
      <p class="style4">(<a href="#" onclick='return displayMenu("Đăng ký");'>chưa là thành viên?</a>)</p>
      <div class='signinform' id='AnotherLogin'>
	      <table width="100%" height="128" border="0">
	        <tr>
	          <td width="40%"><span class="style2">Email:</span></td>
	          <td width="60%"><input name="email_address" type="text" id="email_address" size="20" /></td>
	        </tr>
	        <tr>
	          <td><span class="style2">Mật khẩu:</span></td>
	          <td><input name="pswd" type="password" id="pswd" size="20" />&nbsp;</td>
	        </tr>
			<tr>
	          <td align="left"><input name="rememberme" type="checkbox"><span class="style2">Nhớ?</span> </td>
	          <td alight="right"><input name="Đăng nhập" type="button" onClick="submitbttnonclick('AnotherLogin', 'email_address,pswd')" value="Đăng nhập" /></td>
	        </tr>
	      </table>
      </div>
      <p class="style3">&nbsp;</p>
      <p class="style3">&nbsp;</p>
      <p class="style3">&nbsp;</p>
    </td>
  </tr>
  
  <tr>
    <td height="228" colspan="2" bgcolor="#000000"><div class="footer" id="footer"><%@ include file="includes/footer.inc" %></div></td>
  </tr>
</table>
</body>
</html>
