package com.imarketing.db.dao;

import java.util.List;

import com.imarketing.db.Poll;

public interface IPollDAO extends ICommonDAO {
	
	public boolean insertPoll(Poll p) throws Exception;
	
	public boolean updatePoll(Poll p) throws Exception;
	
	public Poll getPollById(long id) throws Exception;
	
	public List<Poll> getValidPolls(int limit, long startingId) throws Exception;
	
}
