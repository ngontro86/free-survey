package com.imarketing.db.dao.mysql;

import java.util.List;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.imarketing.common.Common;
import com.imarketing.db.Poll;
import com.imarketing.db.dao.IPollDAO;

public class PollMySQLDAO extends CommonMySQLDAO implements IPollDAO {

	@Override
	public Poll getPollById(long id) throws Exception {
		beginTransaction();
		return (Poll)session.get(Poll.class, new Long(id));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Poll> getValidPolls(int limit, long startingId)
			throws Exception {
		beginTransaction();
		return session.createCriteria(Poll.class)
				.add(Restrictions.ne("status", Common.SURVEYPOLL_STATUS_EXPIRED))
				.add(Restrictions.gt("id", new Long(startingId)))
				.addOrder(Order.desc("id"))
				.setMaxResults(limit)
				.list();
	}

	@Override
	public boolean insertPoll(Poll p) throws Exception {
		beginTransaction();
		session.save(p);
		flush();
		return true;
	}

	@Override
	public boolean updatePoll(Poll p) throws Exception {
		beginTransaction();
		session.saveOrUpdate(p);
		flush();
		return true;
	}

}
