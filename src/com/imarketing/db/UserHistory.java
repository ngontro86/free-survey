package com.imarketing.db;

import java.io.Serializable;

@SuppressWarnings({ "serial", "unchecked" })
public class UserHistory implements Serializable, Comparable{
	private long id;
	
	private String notes;
	
	private String location_ip;
	
	private long login_timestamp;
	private long logout_timestamp; 
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public long getLogin_timestamp() {
		return login_timestamp;
	}

	public void setLogin_timestamp(long login_timestamp) {
		this.login_timestamp = login_timestamp;
	}

	public long getLogout_timestamp() {
		return logout_timestamp;
	}

	public void setLogout_timestamp(long logout_timestamp) {
		this.logout_timestamp = logout_timestamp;
	}

	public String getLocation_ip() {
		return location_ip;
	}

	public void setLocation_ip(String location_ip) {
		this.location_ip = location_ip;
	}

	@Override
	public int compareTo(Object o) {
		if(o instanceof UserHistory){
			UserHistory uh = (UserHistory)o;
			if(uh.getId() == getId()) return 0;
			if(getId() > uh.getId()) return 1;
			return -1;
		}
		return 1;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final UserHistory other = (UserHistory) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}
