package com.imarketing.sessionlistener;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Logger;

import com.imarketing.common.Common;
import com.imarketing.db.User;
import com.imarketing.db.UserHistory;
import com.imarketing.db.dao.DAOFactory;
import com.imarketing.db.dao.IUserDAO;
import com.imarketing.db.dao.IUserHistoryDAO;
import com.imarketing.dbwriter.DBWriterListener;

public class TimeoutListener implements HttpSessionListener {
	
	private Logger logger = Logger.getLogger(TimeoutListener.class);
	
	private ConcurrentHashMap<String, HttpSession> activeAuthenticatedSessions = new ConcurrentHashMap<String, HttpSession>();
	
	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		try {
			HttpSession session = arg0.getSession();
			if(session.getAttribute(Common.HTTPSESSION_USER_ATTRIBUTION) instanceof User) {
				User user = (User)session.getAttribute(Common.HTTPSESSION_USER_ATTRIBUTION);
				if(activeAuthenticatedSessions.contains(user.getUsername_emailaddress())) {
					logger.error("Have to invalidate the old session");
					HttpSession oldSession = activeAuthenticatedSessions.get(user.getUsername_emailaddress());
					oldSession.removeAttribute(Common.HTTPSESSION_USER_ATTRIBUTION);
					oldSession.removeAttribute(Common.HTTPSESSION_USERHISTORY);
					oldSession.invalidate();
				}
				activeAuthenticatedSessions.put(user.getUsername_emailaddress(), session);
			}
		} catch (Exception e){
			logger.error("sessionCreated() got exception: ", e);
			DBWriterListener.writeLogs("TimeoutListener: sessionCreated() got exception: " +e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void sessionDestroyed(HttpSessionEvent arg0) {
		try {
			HttpSession session = arg0.getSession();
			if(session.getAttribute(Common.HTTPSESSION_USER_ATTRIBUTION) instanceof User) {
				User user = (User)session.getAttribute(Common.HTTPSESSION_USER_ATTRIBUTION);
				
				IUserDAO userDAO = DAOFactory.getDefaultFactory().getUserDAO();
				user = userDAO.getUser(user.getId());
				IUserHistoryDAO userHistoryDAO = DAOFactory.getDefaultFactory().getUserHistoryDAO();
				UserHistory userHistory = (UserHistory)session.getAttribute(Common.HTTPSESSION_USERHISTORY);
				userHistory.setNotes("Timeout");
				userHistory = userHistoryDAO.insertOrUpdateUserHistory(userHistory);
				((Set<UserHistory>)user.getHistories()).add(userHistory);
				userDAO.updateUser(user);
				
				userHistoryDAO.close(); userDAO.close();
				
				session.removeAttribute(Common.HTTPSESSION_USER_ATTRIBUTION);
				session.removeAttribute(Common.HTTPSESSION_USERHISTORY);
				session.invalidate();
				
				HttpSession oldSession = activeAuthenticatedSessions.get(user.getUsername_emailaddress());
				if(oldSession != null) {
					oldSession.removeAttribute(Common.HTTPSESSION_USER_ATTRIBUTION);
					oldSession.removeAttribute(Common.HTTPSESSION_USERHISTORY);
					oldSession.invalidate();
					activeAuthenticatedSessions.remove(user.getUsername_emailaddress());
				}
			}
		} catch (Exception e){
			logger.error("sessionDestroyed() got exception: ", e);
			DBWriterListener.writeLogs("TimeoutListener: sessionDestroyed() got exception: " +e.getMessage());
		}
	}

}
