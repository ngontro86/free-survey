package com.imarketing.db;


@SuppressWarnings("unchecked")
public class AnswerOption implements Comparable {
	private long id;
	
	private long idx;
	private String answer;
	private String value; // likely is empty
	private String answer_type;
	
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getAnswer_type() {
		return answer_type;
	}
	public void setAnswer_type(String answer_type) {
		this.answer_type = answer_type;
	}
	
	public long getIdx() {
		return idx;
	}
	public void setIdx(long idx) {
		this.idx = idx;
	}
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public int compareTo(Object o) {
		if(o instanceof AnswerOption){
			AnswerOption ao = (AnswerOption)o;
			if(ao.getId() == getId()) return 0;
			if(getId() > ao.getId()) return 1;
			return -1;
		}
		return 1;
	}
	
}
